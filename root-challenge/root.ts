#!/usr/bin/env node
import { RootReporter } from "./root_reporter";

export async function main() {
    const args = process.argv.slice(2);
    if (args.length === 0) {
        console.log(`
No file specified
Usage: <file>

Instructions: 
File must be within current working directory.
May be specified as such (i.e., ./filename) or by filename alone
        `);
    } else {
        await new RootReporter(args[0]).runAll();
    }
}

main();
