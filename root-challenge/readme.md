## Instructions

Built with Nodejs 14.9

For your convenience, a self-contained executable is provided as `root` built via https://www.npmjs.com/package/@vercel/ncc

Alternatively, the built javascript is in `dist/index.js`. 

To build from scratch:
```bash
npm ci
npm run build
```

A launch.json task is also available to debug with VSCode.

To execute tests, `npm run test`

## Design philosophy
While this could be built to be more customizable, such as by passing in a callback to customize the logic and leaving the general-purpose reporter handling the file-reading & output (a pattern I've used), I erred on the side of minimalism for now.

I assumed that I could also register a driver with a Trip command as the specification was unclear and it minimized work without affecting output.

Real-world applications could have more sanity check validations, such as overlapping trips.

I used minimal dependencies to stay consistent with the simple design, but in a real app I would of course be using a real test runner and more dependencies.

## Follow-up
