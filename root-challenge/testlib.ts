import * as assert from "assert";

export async function test(desc, fn) {
    try {
        await fn();
        console.log(`Success! ${desc}`);
    } catch (e) {
        if (e instanceof assert.AssertionError) {
            console.log(`Failure! ${desc}`);
            console.error(e);
        } else {
            console.log("Unknown error:");
            console.error(e);
        }
    }
}
