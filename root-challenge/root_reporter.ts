import * as fs from "fs";
import * as readline from "readline";

interface HourMinute {
    hours: number;
    minutes: number;
}
interface Trip {
    start: HourMinute;
    end: HourMinute;
    distance: number;
}

interface Report {
    totalDistance: number;
    totalMinutes?: number;
    totalHours?: number;
    avgSpeedMph?: number;
    totalHighwayDistance?: number;
    highwayMilesPercent?: number;
}
interface Driver {
    name: string;
    trips?: Array<Trip>;
    report?: Report;
}

export class RootReporter {
    private drivers: Map<string, Driver>;

    public constructor(private filename: string, private test = false) {
        this.drivers = new Map();
    }

    private calculateAvgSpeedPerHour(
        current: Trip,
        totalMinutes: number
    ): number {
        const avgSpeedPerMinute = current.distance / totalMinutes;
        return avgSpeedPerMinute * 60;
    }

    private isOutlierTrip(avgSpeedPerHour: number): boolean {
        return avgSpeedPerHour < 5 || avgSpeedPerHour > 100;
    }

    private prepareReport(): void {
        for (const [_, driver] of this.drivers) {
            let report = driver.trips.reduce(
                (memo: Report, current: Trip) => {
                    let totalHighwayDistance = 0;
                    // convert hours to minutes
                    const hoursAsMinutes =
                        current.end.hours * 60 - current.start.hours * 60;
                    const minutes = current.end.minutes - current.start.minutes;

                    const totalMinutes = hoursAsMinutes + minutes;

                    const avgSpeedPerHour = this.calculateAvgSpeedPerHour(
                        current,
                        totalMinutes
                    );

                    if (avgSpeedPerHour >= 50) {
                        totalHighwayDistance = current.distance;
                    }

                    if (this.isOutlierTrip(avgSpeedPerHour)) {
                        return {
                            totalDistance: memo.totalDistance,
                            totalMinutes: memo.totalMinutes,
                        };
                    }

                    return {
                        totalDistance: memo.totalDistance + current.distance,
                        totalMinutes: memo.totalMinutes + totalMinutes,
                        totalHighwayDistance:
                            memo.totalHighwayDistance + totalHighwayDistance,
                    };
                },
                <Report>{
                    totalDistance: 0,
                    totalMinutes: 0,
                    totalHighwayDistance: 0,
                }
            );

            driver.report = report;
            report.avgSpeedMph =
                (report.totalDistance / report.totalMinutes) * 60;
            report.totalDistance = report.totalDistance;
            report.highwayMilesPercent = parseFloat(
                (
                    (report.totalHighwayDistance / report.totalDistance) *
                    100
                ).toFixed(2)
            );
        }

        this.drivers = new Map(
            [...this.drivers].sort((a, b): number => {
                return b[1].report.totalDistance - a[1].report.totalDistance;
            })
        );
    }

    private printReport(): string | void {
        let output = "";
        for (const [name, driver] of this.drivers.entries()) {
            const report = driver.report;
            output += `${name}: ${Math.round(report.totalDistance)} miles`;
            if (report.totalDistance > 0) {
                output += ` @ ${Math.round(
                    report.avgSpeedMph
                )} mph & %highway: ${report.highwayMilesPercent}%\n`;
            } else {
                output += "\n";
            }
        }

        output = output.slice(0, -1); // rm trailing \n

        if (this.test) return output;
        else console.log(output);
    }

    private async loadFile(): Promise<void> {
        if (!this.filename.startsWith("./")) {
            this.filename = `./${this.filename}`;
        }
        const rs = fs.createReadStream(this.filename);
        const rl = readline.createInterface({ input: rs });

        // process stream in case the file is very large
        for await (const line of <any>rl) {
            const data = line.split(" ");
            const command = data[0];
            const name = data[1];
            let driver: Driver = this.drivers.get(name) ?? null;
            if (!driver) {
                // driver not registered, but we can register it
                // if registering from trip, might want to error or warn if that's not valid; assume valid for now
                driver = {
                    name,
                    trips: [],
                    report: {
                        totalDistance: 0,
                    },
                };
                this.drivers.set(name, driver);
            }

            if (command === "Trip") {
                let [_, __, start, end, distance] = data;

                const [hourS, minuteS] = start
                    .split(":")
                    .map((s) => parseInt(s, 10));
                const [hourE, minuteE] = end
                    .split(":")
                    .map((s) => parseInt(s, 10));

                driver.trips.push({
                    start: {
                        hours: hourS,
                        minutes: minuteS,
                    },
                    end: {
                        hours: hourE,
                        minutes: minuteE,
                    },
                    distance: parseFloat(distance),
                });
            } else if (command !== "Driver") {
                throw new Error("unknown command");
            }
        }
    }

    public async runAll(): Promise<void | string> {
        await this.loadFile();
        this.prepareReport();
        const output = this.printReport();
        if (this.test) return output;
    }
}
