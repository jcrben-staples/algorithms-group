import * as assert from "assert";

import { RootReporter } from "./root_reporter";
import { test } from "./testlib";

test("output should match the spec", async () => {
    const output = await new RootReporter("./root_data.txt", true).runAll();
    // console.log("output", output);
    const expectedOutput = `Lauren: 42 miles @ 34 mph & %highway: 0%
Dan: 39 miles @ 47 mph & %highway: 55.75%
Kumi: 0 miles`;
    assert.strictEqual(output, expectedOutput);
});

test("outliers are dropped", async () => {
    // Lucas has 100+ outlier; Sharon has < 5 outlier
    const output = await new RootReporter(
        "./root_data_expanded.txt",
        true
    ).runAll();
    const expected = `George: 70 miles @ 11 mph & %highway: 0%
Lauren: 42 miles @ 34 mph & %highway: 0%
Dan: 39 miles @ 47 mph & %highway: 55.75%
Kumi: 0 miles
Lucas: 0 miles
Sharon: 0 miles`;
    assert.strictEqual(output, expected);
});

test("highway distance calculated correctly", async () => {
    // Correct highway distance = distance / highway distance = 21.8+17.3 / (21.8+17.3)
    const expected = `Dan: 39 miles @ 47 mph & %highway: 55.75%`;
    const output = await new RootReporter(
        "./root_data_highway.txt",
        true
    ).runAll();
    assert.strictEqual(output, expected);
});
