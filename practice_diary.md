
## resources
https://seanprashad.com/leetcode-patterns/!
* found via [How to use Leetcode in 2020](https://www.youtube.com/watch?v=6jf6SK9qWBc)

https://leetcodetherapy.com/
* found via https://www.reddit.com/r/leetcode/comments/nv9p2j/help_me_in_being_successful_i_cant_do_even_easy/

https://algodaily.com/lessons/how-to-get-better-at-coding-interviews
* https://algodaily.com/lessons/a-gentle-refresher-into-arrays-and-strings


### union find
[Data Structures Easy to Advanced Course - Full Tutorial from a Google Engineer](https://www.youtube.com/watch?v=RBSGKlAvoiM)
* [Disjoint Set Union (Union Find) — The same blood type](https://medium.com/@RamkrishnaKulka/disjoint-set-union-union-find-the-same-blood-type-e67c51b1d2)
  * "rank of the set means the total number of elements that belong to the set" - does not seem correct
  * "In path compression, every node in the find path will be directly attached to the representative(leader) node"
* https://en.wikipedia.org/wiki/Disjoint-set_data_structure
  * "To merge trees with roots x and y, first compare their ranks. If the ranks are different, then the larger rank tree becomes the parent, and the ranks of x and y do not change. If the ranks are the same, then either one can become the parent, but the new parent's rank is incremented by one."
    * https://algorithms.tutorialhorizon.com/disjoint-set-union-find-algorithm-union-by-rank-and-path-compression/
    * "Ranks are used instead of height or depth because path compression will change the trees’ heights over time."
* Kruskal's algo per Youtube video above: sort the adjacency list by weight to get the "miminum part", then skip edges which area already in a group


## log
* 2021-08-10: did some easy ones; https://leetcode.com/problems/backspace-string-compare/ is theoretically easy but struggled w/ O(1) space a lot
...skipped
* 2021-08-7: been getting some w/o cheating esp BFS & easy DFS - backtracking combinations / perms still tricky; ended day w/ codesignal and did OK
...some work on the weekends August 3 (BFS) and August 7 (backtracking, binary search, DFS)
* 2021-07-12: after trading, did a few of the starting leetcode problems

Amazon most asked \
![](amazon-most-asked.png)
