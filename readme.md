* Currently solving inside bradfield-algorithms

## approaches
* Two pointers
* Hashmap
* Dynamic programming
* BFS
* DFS

## dynamic programming
three-dimensional covered in the advanced https://www.topcoder.com/thrive/articles/Dynamic%20Programming:%20From%20Novice%20to%20Advanced

## code of progress
1. Possible values:
    1. DONE - no need to look
    2. REDO
    3. PARTIAL
    4. TODO
    5. TOOHARD - build up to this

## resources
neetcode on youtube is the best by far

tried a few chatrooms, they're all pretty much dead; /r/leetcode is where ppl are talking

also see https://disboard.org/servers/tag/computer-science

https://liyin2015.medium.com/
* https://github.com/liyin2015/Hands-on-Algorithmic-Problem-Solving
* discord chat https://discord.gg/ZXnSag7fMP - about 600 ppl

Let's talk algorithms https://discord.gg/e22dgAkNJW
* old & semi-active, but no moderation
