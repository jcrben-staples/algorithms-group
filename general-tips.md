Places to look:

1. practice_diary.md
   1. bradfield-algorithms


Random notes:
* https://leetcode.com/discuss/interview-experience/1210957/Amazon-SDE-I-OA-%2B-tips
  * write out optimal in written portion
* neetcode
* Jackson Gabbard - https://www.youtube.com/watch?v=olK6SWl8UrM
* ??

Dynamic programming:
* https://www.reddit.com/r/leetcode/comments/o0kwg3/is_top_down_recursion_with_dp_memoization/h1vpc7j/
* per https://www.reddit.com/r/leetcode/comments/o0kwg3/comment/h1vpc7j https://web.archive.org/web/20211126220903/https://codeforces.com/blog/entry/20308 can be used blindly
