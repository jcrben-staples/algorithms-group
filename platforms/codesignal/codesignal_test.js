function alloc(v, memory, i, X) {
    let startChecking = false;
    let l = 0;
    let r = memory.length - 1;
    countX = 0;
    let start = 0;
    while (l <= r) {
        // for (let i = 0 ; i < memory.length; i++) {
        let last;
        if (memory[i] === 0) {
            startChecking = true;
            start = i;
            countX += 1;
            memory[i] = 1;
            j = i + 1;
            while (memory[j] === 0 && countX < X) {
                memory[j] = 1;
                j += 1;
                countX += 1;
            }

            if (countX === X) return start;
            else {
                last = j - 1;
                while (j >= start) {
                    memory[j - 1] = 1;
                    j -= 1;
                }
                // return -1
            }
        }

        if (last) {
            l = last;
        } else {
            l += 1;
        }

        // l += 1
        // }
    }
    // should not hit
    return -1;
}

function erase(v, preallocated, memory, blocks, index) {
    if (preallocated[index] === 1) {
        memory[index] = 0;
        return -1;
    } else {
        let blockLength = blocks[index];
        if (blockLength) {
            for (let i = index; i < index + blocksLength; i++) {
                memory[i] = 0;
            }
            return blockLength;
        }
    }
}

function memoryAllocator2(memory, queries) {
    let preallocated = memory.slice();
    // let blocks =
    let result = [];
    // start to length
    let blocks = {};
    for (let [i, v] of queries.entries()) {
        let X, index;
        if (v[0] === 0) {
            let X = v[1];
        } else {
            index = v[1];
        }

        // alloc
        if (v[0] === 0) {
            let rv = alloc(v, memory, i, X);
            blocks[rv] = X;
            result.push(rv);
            continue;
        }

        // erase
        if (v[1] === 1) {
            let rv = erase(v, preallocated, memory, blocks, index);
            result.push(rv);
        }
    }

    return result;
}

/*
memoryAllocator2
(Task 3 of 4)
Submitted
0:00:00
+
0:00:44
Codewriting

You are given an array of integers memory consisting of 0s and 1s - whether the corresponding memory unit is free or not. memory[i] = 0 means that the ith memory unit is free, and memory[i] = 1 means it's occupied.

Your task is to perform two types of queries:

alloc X: Find the left-most memory block of X consecutive free memory units and mark these units as occupied (ie: find the left-most contiguous subarray of 0s, and replace them all with 1s).
If there are no blocks with X consecutive free units, return -1; otherwise return the index of the first position of the allocated block segment.
erase index: If there exists an allocated memory block starting at position index, free all its memory units. If the memory cell at position index was occupied before the very first operation, free this cell only.
Return the length of the deleted memory block. If there is no such allocated block starting at the position index, return -1.
The queries are given in the following format:

queries is an array of 2-elements arrays;
if queries[i][0] = 0 then this is an alloc type query, where X = queries[i][1];
if queries[i][0] = 1 then this is an erase type query, where index = queries[i][1].
Return an array containing the results of all the queries.

Example

For memory = [0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0] and queries = [[0, 2], [0, 1], [0, 1], [1, 0], [1, 1], [1, 3], [0, 4]], the output should be memoryAllocator2(memory, queries) = [2, 0, 4, 1, 1, -1, -1].

example

[0, 2] corresponds to alloc 2, which allocates a memory block from units 2 to 3. After this operation memory = [0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0]. Return the memory block starting index - 2.
[0, 1] corresponds to alloc 1, which allocates a memory block from units 0 to 0. After this operation memory = [1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0].
[0, 1] corresponds to alloc 1, which allocates a memory block from units 4 to 4. After this operation memory = [1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0].
[1, 0] corresponds to erase 0. The 2nd allocated memory block [0, 0] starts at this position and has length of 1. After freeing the memory, memory = [0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0]. Return the length of the removed memory block - 1.
[1, 1] corresponds to erase 1. This memory cell was occupied before the start of the program, so, only this memory cell should be cleared. After freeing the memory cell, memory = [0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0]. Return the length of the removed memory block - 1.
[1, 3] corresponds to erase 3. There is no allocated memory block starting at index 3, so there is nothing to erase, -1 is returned.
[0, 4] corresponds to alloc 4. There is no memory block of length 4 in which all blocks are free, so return -1.
Input/Output

[execution time limit] 4 seconds (js)

[input] array.integer memory

An array of 0s and 1s representing bits of memory.

Guaranteed constraints:
1 ≤ memory.length ≤ 300.

[input] array.array.integer queries

An array of 2-element arrays representing queries of the type alloc or erase.

Guaranteed constraints:
2 ≤ queries.length ≤ 300,
queries[i].length = 2,
0 ≤ queries[i][0] ≤ 1,
If queries[i][0] = 0, then 1 ≤ queries[i][1] ≤ memory.length,
If queries[i][0] = 1, then 0 ≤ queries[i][1] ≤ memory.length - 1.

[output] array.integer

Return an array in which the ith element is equal to the answer of the ith query.

[JavaScript] Syntax Tips

// Prints help message to the console
// Returns a string
function helloWorld(name) {
    console.log("This prints to the console when you Run Tests");
    return "Hello, " + name;
}

JavaScript
Node.js v14.16.1
13141516171819202122232425262728293031323312343511109
function alloc(v, memory, i, X) {
    let startChecking = false
    let l = 0
    let r = memory.length -1;
    countX = 0
    let start = 0
    while (l <= r) {
            // for (let i = 0 ; i < memory.length; i++) {
        let last
        console.log('memory', memory)
…    }
    
    return result
}


TESTS
CUSTOM TESTS
Tests passed: 0/8.
Test 1
Input:
memory: [0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0]
queries:
[[0,2], 
 [0,1], 
 [0,1], 
 [1,0], 
 [1,1], 
 [1,3], 
 [0,4]]
Output:
[-1, -1, -1, -1, -1]
Expected Output:
[2, 0, 4, 1, 1, -1, -1]
*/
