"use strict";
/* 
return country from country code
s
*/

const fs = require("fs");
const https = require("https");

process.stdin.resume();
process.stdin.setEncoding("utf-8");

let inputString = "";
let currentLine = 0;

process.stdin.on("data", function (inputStdin) {
    inputString += inputStdin;
});

process.stdin.on("end", function () {
    inputString = inputString.split("\n");

    main();
});

function readLine() {
    return inputString[currentLine++];
}

async function req(p) {
    return new Promise((resolve, reject) => {
        https.get(
            `https://jsonmock.hackerrank.com/api/countries?page=${p}`,
            (res) => {
                var body = [];
                res.on("data", function (chunk) {
                    body.push(chunk);
                });
                res.on("end", function () {
                    try {
                        body = JSON.parse(Buffer.concat(body).toString());
                    } catch (e) {
                        reject(e);
                    }
                    resolve(body);
                });
            }
        );
    });

    // return p
}

async function getCountryName(code) {
    let result = null;
    let p = 1;
    while (result === null) {
        let body = [];
        let r = await req(p);
        // console.log('r', r)
        // console.log('body', body)
        // })
        // let r = fetch()
        if (r.data.length === 0) {
            break;
        }
        let rv = r.data.find((val) => val.alpha2Code === code);
        // console.log('rv', rv)

        if (rv != null) {
            result = rv.name;
            break;
        }

        p += 1;
    }

    return result;

    // write your code here
    // API endpoint: https://jsonmock.hackerrank.com/api/countries?page=<PAGE_NUMBER>
}
// async function main() {
