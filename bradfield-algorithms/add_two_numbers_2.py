"""
Testing myself: 
* 2021-10-03: better start but still got stuck quick
    * problems: got the node = ListNode(remain) then curr = node part wrong
* 2021-09-29: basically baffled as to how to start, totally whiffed the carry, reviewed my answer below after writing a bit of code
"""
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def addTwoNumbers(self, l1, l2):
        dummy = ListNode(0)
        curr = dummy
        carry = 0

        while l1 or l2:
            d1 = l1.val
            d2 = l2.val

            d3 = d1 + d2 + carry

            remain = d3 % 10
            carry = d3 // 10
            curr.val = remain
            curr.next = ListNode(0)
            curr = curr.next

            l1 = l1.next
            l2 = l2.next

        return dummy


# [2,4,3]
# [5,6,4]
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


l1 = ListNode(3)
l1.next = ListNode(4)
l1.next.next = ListNode(2)

l2 = ListNode(4)
l2.next = ListNode(6)
l2.next.next = ListNode(5)
r = Solution().addTwoNumbers(l1, l2)
print("r", r.val)


"""

You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

Example:

Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
Output: 7 -> 0 -> 8
Explanation: 342 + 465 = 807.
"""


def printList(_list):
    node = _list
    while node:
        print("list.val", node.val)
        node = node.next


def createList(number):
    chars = list(str(number)).reverse()

    for c in chars:
        print("c", c)


# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """


s = Solution()
l1 = ListNode(5)
l1.next = ListNode(3)
l1.next.next = ListNode(1)

# printList(l1)
createList(123)

# l2 =
s.addTwoNumbers()


def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
    dummy = ListNode()
    cur = dummy

    carry = 0
    while l1 or l2 or carry:
        val1 = l1.val if l1 else 0
        val2 = l2.val if l2 else 0

        sum = val1 + val2 + carry
        carry = sum // 10

        first_digit = sum % 10

        node = ListNode(first_digit)
        cur.next = node
        cur = node
        l1 = l1.next if l1 else None
        l2 = l2.next if l2 else None

    return dummy.next
