# https://leetcode.com/problems/sudoku-solver/
class Solution:
    def solveSudoku(self, board: List[List[str]]) -> None:
        """
        Do not return anything, modify board in-place instead.
        """

'''
Resources:

https://github.com/norvig/pytudes/blob/master/py/sudoku.py
'''