# suffix tree
https://en.wikipedia.org/wiki/Online_algorithm | Online algorithm - Wikipedia
https://www.youtube.com/watch?v=aPRqocoBsFQ | Suffix Tree using Ukkonen's algorithm - YouTube
http://brenden.github.io/ukkonen-animation/ | Visualization of Ukkonen's Algorithm
https://stackoverflow.com/questions/9452701/ukkonens-suffix-tree-algorithm-in-plain-english?rq=1 | string - Ukkonen's suffix tree algorithm in plain English - Stack Overflow
https://en.wikipedia.org/wiki/Suffix_tree | Suffix tree - Wikipedia
https://humanreadablemag.com/issues/0/articles/the-wonders-of-the-suffix-tree-through-the-lens-of-ukkonen%E2%80%99s-algorithm/ | The Wonders of the Suffix Tree through the Lens of Ukkonen’s Algorithm - Human Readable Magazine
https://marknelson.us/posts/1996/08/01/suffix-trees.html | Fast String Searching With Suffix Trees | Mark Nelson