# 2021-08-10: got it but had the dic[num] assignment too early at first
class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        dic = {}
        for i, num in enumerate(nums):
            complement = target - num

            r = dic.get(complement, False)

            if r is not False and i != r:
                return [r, i]

            dic[num] = i

        # return False
