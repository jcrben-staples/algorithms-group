/* 
Imagine we have an image. We'll represent this image as a simple 2D array where every pixel is a 1 or a 0. The image you get is known to have a single rectangle of 0s on a background of 1s.

Write a function that takes in the image and returns one of the following representations of the rectangle of 0's: top-left coordinate and bottom-right coordinate OR top-left coordinate, width, and height.

image1 = [
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 0, 0, 0, 1],
  [1, 1, 1, 0, 0, 0, 1],
  [1, 1, 1, 1, 1, 1, 1],
]

Sample output variations (only one is necessary):

findRectangle(image1) =>
  row: 2, column: 3, width: 3, height: 2
  2,3 3,5 -- row,column of the top-left and bottom-right corners

Other test cases:

image2 = [
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 0],
]

findRectangle(image2) =>
  row: 4, column: 6, width: 1, height: 1
  4,6 4,6

image3 = [
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 0, 0],
  [1, 1, 1, 1, 1, 0, 0],
]

findRectangle(image3) =>
  row: 3, column: 5, width: 2, height: 2
  3,5 4,6
  
image4 = [
  [0, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
]

findRectangle(image4) =>
  row: 0, column: 0, width: 1, height: 1
  0,0 0,0

image5 = [
  [0],
]

findRectangle(image5) =>
  row: 0, column: 0, width: 1, height: 1
  0,0 0,0

n: number of rows in the input image
m: number of columns in the input image



*/

const image1 = [
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 0, 0, 0, 1],
  [1, 1, 1, 0, 0, 0, 1],
  [1, 1, 1, 1, 1, 1, 1],
];

const image2 = [
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 0],
];

const image3 = [
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 0, 0],
  [1, 1, 1, 1, 1, 0, 0],
];

const image4 = [
  [0, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
];

const image5 = [
  [0],
];

function findRectangle(image) {
  const rows = image.length -1 
  const cols = image[0].length - 1
  const visited = image.map((row) => {
    return row.map(item => {
      return false
    })
  })
  
  
  const directions = [
    [0, 1], // right
    [0, -1], // left
    [1, 0], // down
    [-1, 0] // up
  ]
//   let q = [0,0]
  let q = [];
//   console.log('visited', visited[0][0])
//   for (let r = 0; r < rows; r++) {
//     for (let c = 0; c < cols; c++) {
      q.unshift([r, c])
      let curr = q.pop()
//       console.log('curr', curr)
      const row = curr[0]
      const col = curr[1]
      visited[row][col] = true;

      for (const dir of directions) {
        const newRow = curr[0]+dir[0]
        const newCol = curr[1]+dir[1]
        if (newRow < 0 || newCol < 0 || newRow > rows || newCol > cols) {
          continue
        }
        if (!visited[newRow][newCol]) {
          q.unshift([curr[0]+dir[0],curr[1]+dir[1]])
        }
      }
      
      while (q.length > 0) {
        console.log('popping', q.length)
        curr = q.pop()
        const r = curr[0]
        const c = curr[1]
        if (curr[r][c] === 0) {
          // startIndex
        }
        visited[r][c] = true;
       for (const dir of directions) {
//          console.log('curr', curr)
            const newRow = curr[0]+dir[0]
            const newCol = curr[1]+dir[1]
             if (newRow < 0 || newCol < 0 || newRow > rows || newCol > cols) {
              continue
            }
//             console.log('visited', visited)
//          console.log('newRow', newRow, 'newCol', newCol)
            
//          console.log('newRow', newRow)
//          console.log('newCol', newCol)
            
            if (!visited[newRow][newCol]) {
              q.unshift([newRow,newCol])
            }
//          console.log('visited', visited)
        }
      }
      
//     }
//   }
      
      
//   }
//       q.unshift(image[r][directions[0][1]])
//       q.unshift((image[r][directions[0][0]]))
//       q.unshift((image[r][directions[1][0]]))
//       q.unshift((image[r][directions[1][1]]))
//       q.unshift((image[r][directions[2][0]]))
//       q.unshift((image[r][directions[2][1]]))
//       q.unshift((image[r][directions[3][0]]))
//       q.unshift((image[r][directions[3][1]]))
      

//       //       if (curr[r][c] === 0) {
        
// //       }
//   }
}

findRectangle(image1)


