from typing import List

# TODO: understand simple solution https://leetcode.com/problems/subsets-ii/discuss/30305/Simple-python-solution-(DFS).
# NOTE: incrementing start as i+1 rather than start + 1 was key mistake
class Solution:
    def subsetsWithDup(self, nums: List[int]) -> List[List[int]]:
        # dups = set()
        r = []

        # nums.sort()

        def dfs(sofar, start):
            r.append(sofar.copy())

            # for i in range(start, len(nums)):
            i = start
            while i < len(nums):
                # if nums[i] in dups:
                #     continue
                # how is this not working? seems identical to https://leetcode.com/problems/subsets-ii/discuss/30156/Subset-I-II-and-Perm-I-II-Difference-Explained
                if i != start and nums[i] == nums[i - 1]:
                    i += 1
                    continue
                # dups.add(nums[i])
                sofar.append(nums[i])
                # dfs(sofar + [nums[i]], start + 1)
                dfs(sofar, i + 1)
                sofar.pop()
                i += 1
                # dups.remove(nums[i])

        dfs([], 0)

        return r


print(Solution().subsetsWithDup([1, 2, 2]))


class Solution(object):
    def subsetsWithDup(self, nums):
        ret = []
        self.dfs(sorted(nums), [], ret)
        return ret

    def dfs(self, nums, path, ret):
        ret.append(path)
        for i in range(len(nums)):
            if i > 0 and nums[i] == nums[i - 1]:
                continue
            self.dfs(nums[i + 1 :], path + [nums[i]], ret)


print(Solution().subsetsWithDup([1, 2, 2]))
