from typing import List


class Solution:
    def numIslands(self, grid: List[List[str]]) -> int:
        rows = max(grid)
        cols = max(grid[0])

        visited = [[False for _ in range(cols) for _ in range(rows)]]
        directions = [[0, 1], [0, -1][-1, 0], [1, 0]]

        for r in range(rows):
            for c in range(cols):
                if visited[r][c]:
                    continue
                if grid[r][c] == 0:
                    continue

                q = [(r, c)]
                while q:
                    row, col = q.pop()
