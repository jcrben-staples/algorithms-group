# the leaves subtration is the easiest and most elegant solution https://leetcode.com/problems/minimum-height-trees/discuss/76055/Share-some-thoughts
# from https://leetcode.com/problems/minimum-height-trees/discuss/659405/Python-Dynamic-Programming-solution-O(n)-time-explained
from collections import defaultdict
import json
from typing import List


class Node:
    def __init__(self, val: int, children: List["Node"] = None):
        self.val = val
        self.children = children if children is not None else []
        self.visited = False


def tree_height_map(root: Node, dist=0, height_matrix=None):
    # print(f'Working on {root.val}')
    if not root.children:
        # no children, return 1
        return dist
    else:
        unvisited_children = [node for node in root.children if not node.visited]
        if not unvisited_children:
            # raise ZeroDivisionError
            return dist
        root.visited = True
        # pairs = []
        heights = []
        for node in unvisited_children:
            dist_from_root = dist
            height = tree_height_map(node, dist + 1, height_matrix=height_matrix)
            heights.append(height)
            height_matrix[root.val][node.val] = height - dist_from_root
            # pairs.append({'node': node.val, 'height': height, 'dist_from_root': dist_from_root})
        # max_pair = max(pairs, key=lambda x: x['height'])
        # height_matrix[root.val] = max_pair
        root.visited = False
        return max(heights)


class Solution:
    def findMinHeightTrees(self, n: int, edges: List[List[int]]) -> List[int]:
        if not edges:
            return list(range(n))
        if n <= 2:
            return list(range(n))
        # construct a linked-list like graph in O(n) time and O(n)
        nodemap = {}
        for edge in edges:
            nodes = []
            for val in edge:
                node = nodemap.get(val)
                if not node:
                    node = Node(val=val)
                    nodemap[val] = node
                nodes.append(node)
            # undirected graph, they are parent/children of each other
            nodes[0].children.append(nodes[1])
            nodes[1].children.append(nodes[0])
        # for each node in nodemap, find the height of the tree
        height_matrix = defaultdict(lambda: defaultdict(int))
        # select a node that splits the graph, i.e. not a leaf node
        rootval = None
        for val, node in nodemap.items():
            if len(node.children) > 1:
                rootval = val
                break
        print(f"Selected {rootval} as the root")
        root = nodemap[rootval]
        root_height = tree_height_map(root, dist=0, height_matrix=height_matrix)
        node_heights = {root.val: root_height}
        nodes = [(root, child) for child in root.children]
        while nodes:
            parent, node = nodes.pop()
            # what is the height of the biggest other adjacent node to the parent?
            max_child_height = 0
            for childval, h in height_matrix[parent.val].items():
                if childval != node.val:
                    if h > max_child_height:
                        max_child_height = h
            node_height = max(
                [max_child_height + 1, height_matrix[parent.val][node.val] - 1]
            )
            height_matrix[node.val][parent.val] = max_child_height + 1
            node_heights[node.val] = node_height
            for child in node.children:
                if child.val != parent.val:
                    nodes.append((node, child))
        best_nodes = []
        min_height = None
        for (nodeval, height) in node_heights.items():
            if not min_height:
                min_height = height
                best_nodes.append(nodeval)
            else:
                if height < min_height:
                    min_height = height
                    best_nodes = [nodeval]
                elif height == min_height:
                    best_nodes.append(nodeval)
        print(json.dumps(node_heights, indent=1))
        return best_nodes


# print(
#     "Solution().findMinHeightTrees",
#     Solution().findMinHeightTrees(5, [[0, 3], [1, 3], [2, 3], [3, 4], [4, 5]]),
# )
print(
    "Solution().findMinHeightTrees",
    Solution().findMinHeightTrees(
        5, [[0, 3], [1, 3], [2, 3], [3, 4], [4, 5], [5, 6], [6, 7]]
    ),
)
