class RingBufferQueue(object):
    """
    still find this confusing, decent article:
    https://medium.com/better-programming/now-buffering-7a7d384faab5
    * https://github.com/Coswold/CS_1.3/blob/master/circular_buffer.py

    Finish the functions below such that this queue is backed by a Ring Buffer.
    Recall that a ring buffer uses an array and two pointers to keep track of
    where to read, and where to write.
    
    Be careful! If the read pointer were to overtake the write pointer, it
    would return incorrect data! If the write pointer were to overtake the read
    pointer, it would overwrite data that hasn't been read yet!

    In many contexts, you would avoid this issue by stalling when one pointer
    would overwrite the other. Since doing so only makes sense in a multithreaded
    environment, you may prefer to just resize the underlying ring buffer at
    these times, instead.
    """

    def __init__(self):
        self._capacity = 200
        self._items = [None] * self._capacity
        self._write_idx = 0
        self._read_idx = 0
        self._item_count = 0

    def enqueue(self, item):
        if self._item_count == self._capacity:
            # If the buffer is full, rotate items so that the next items to be
            # read are at the beginning, then double the capacity. To rotate,
            # we reverse the whole list in place, then reverse both components.
            # See Programming Pearls for an explanation.
            items = self._items
            items.reverse()
            n = self._capacity - self._read_idx
            for i in range(n):
                items[i], items[n - i - 1] = items[n - i - 1], items[i]
            for i in range(self._read_idx // 2):
                items[n + i], items[-i - 1] = items[-i - 1], items[n + i]
            items.extend(None for _ in range(self._capacity))

            self._items = items
            self._write_idx = self._capacity
            self._read_idx = 0
            self._capacity *= 2

        self._items[self._write_idx] = item
        self._write_idx = (self._write_idx + 1) % self._capacity
        self._item_count += 1

    def dequeue(self):
        ret_itm = self._items[self._read_idx]
        self._read_idx = (self._read_idx + 1) % self._capacity
        self._item_count -= 1

        return ret_itm

    def size(self):
        return self._item_count

    def is_empty(self):
        return self._item_count == 0
