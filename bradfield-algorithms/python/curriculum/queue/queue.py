class PythonListQueue(object):
    """
    A queue based on the built in Python list type.
    """
    def __init__(self):
        self._items = []

    def enqueue(self, item):
        self._items.append(item)

    def dequeue(self):
        return self._items.pop(0)

    def size(self):
        return len(self._items)

    def is_empty(self):
        return len(self._items) == 0


class LinkedListNode(object):
    """
    A doubly linked list node, support for the LinkedListQueue. You should not need
    to change this code, but you will want to use it in the LinkedListQueue
    """
    def __init__(self, value, prevNode, nextNode):
        self.value = value
        self.prev = prevNode
        self.next = nextNode


class LinkedListQueue(object):
    """
    Finish the functions below to create a queue based on a linked list. Because
    a queue must either:

        * enqueue to the head and dequeue from the tail; or
        * enqueue to the tail and dequeue from the head.

    You should use a doubly linked list to ensure O(1) time enqueue and dequeue.
    """
    def __init__(self):
        self.tail = None
        self.head = None
        self.total = 0

    # enqueue to tail / rear
    def enqueue(self, item):

        node = LinkedListNode(item, None, self.tail)

        # initially, node can be tail & head (TODO: review answers / edge cases)
        if self.tail == None:
            self.head = node
        else:
            node.next = self.tail
            self.tail.prev = node

        self.tail = node
        self.total += 1
        
        return self

    # dequeue from head / front
    def dequeue(self):
        if self.head == None:
            return

        result = self.head.value
        self.head = self.head.prev
        self.total -= 1
        return result

    def size(self):
        return self.total

    def is_empty(self):
        return self.total == 0


class RingBufferQueue(object):
    """
    NOTE: wasn't sure about this problem; tried research:
    Google "what is a ring buffer"
        -> https://en.wikipedia.org/wiki/Circular_buffer#How_it_works
            * seems like a linked list due to circularity?
            * but below says it uses array
        -> https://www.embedded.com/electronics-blogs/embedded-round-table/4419407/The-ring-buffer

    Finish the functions below such that this queue is backed by a Ring Buffer.
    Recall that a ring buffer uses an array and two pointers to keep track of
    where to read, and where to write.
    
    Be careful! If the read pointer were to overtake the write pointer, it
    would return incorrect data! If the write pointer were to overtake the read
    pointer, it would overwrite data that hasn't been read yet!

    In many contexts, you would avoid this issue by stalling when one pointer
    would overwrite the other. Since doing so only makes sense in a multithreaded
    environment, you may prefer to just resize the underlying ring buffer at
    these times, instead.
    """
    def __init__(self):
        self._items = []
        self.bufferWriteLocation = 0
        self.bufferReadLocation = 0
        self.count = 0

    def enqueue(self, item):
        if self.bufferWriteLocation >= self.bufferReadLocation:
            return
        else:
            self.bufferWriteLocation += 1
            self._items.insert(self.bufferWriteLocation, item)

        return self

    def dequeue(self):
        # dequeue is reading; ensure write is ahead
        if self.bufferWriteLocation <= self.bufferReadLocation:
            return
        else:
            result = self._items.pop(self.bufferReadLocation)
            self.bufferReadLocation -= 1
        
        return result
    
    def size(self):
        return self.count

    def is_empty(self):
        return self.count == 0


QUEUE_CLASSES = (
    # PythonListQueue,
    # LinkedListQueue,
    RingBufferQueue,
)

