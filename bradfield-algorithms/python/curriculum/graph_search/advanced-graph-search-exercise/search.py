from constants import *

# left, right, up, down
offsets = [[0, -1], [0, 1], [-1, 0], [1, 0]]
boundaries = []


def get_neighbors(loc, grid, explored):
    neighbors = []
    # possibleNeighbors = [
    #     [loc[0] + x, loc[1] + y] if loc[0] + x >= 0 or loc[1] + y >= 0
    #     for x, y in offsets
    # ]
    possibleNeighbors = [([loc[0] + x, loc[1] + y]) for x, y in offsets]

    for x, y in possibleNeighbors:
        if (
            (x >= boundaries[0] and x <= boundaries[1])
            and (y >= boundaries[0] and y <= boundaries[1])
            and ((x, y) not in explored)
            and (grid[x][y] != M)
        ):
            neighbors.append(((x, y), loc))

    return neighbors


def dfs(grid, start, goal):
    global boundaries
    boundaries = [0, len(grid[0]) - 1]
    explored = {}

    def traverse(curr, parent):
        explored[curr] = parent
        neighbors = get_neighbors(curr, grid, explored)

        if (len(neighbors)) == 0:
            return

        for neighbor, _ in neighbors:
            traverse(neighbor, curr)

    traverse((0, 0), None)

    return explored


def bfs(grid, start, goal):
    global boundaries
    boundaries = [0, len(grid[0]) - 1]
    queue = [((0, 0), None)]

    explored = {}

    while len(queue) != 0:
        next, parent = queue.pop(0)
        explored[next] = parent
        neighbors = get_neighbors(next, grid, explored)
        queue = [*queue, *neighbors]

    return explored


def ucs(grid, start, goal):
    pass
    # TODO: factor out duplication (decorator?)...
    # global boundaries
    # boundaries = [0, len(grid[0]) - 1]


print(
    "result bfs:",
    bfs([[S, N, N, N], [N, M, M, M], [N, R, R, R], [N, N, N, G]], (0, 0), (3, 3)),
)
# grid = Grid([[S, N, N, N], [N, M, M, M], [N, R, R, R], [N, N, N, G]])
# grid.dfs()
# print(grid.grid)

