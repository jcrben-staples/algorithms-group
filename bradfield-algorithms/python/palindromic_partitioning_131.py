from typing import List

# use https://leetcode.com/problems/palindrome-partitioning/discuss/41973/Python-recursiveiterative-backtracking-solution as a model
class Solution:
    def partition(self, s):
        res = []
        self.dfs(s, [], res)
        return res

    def dfs(self, s, path, res):
        if not s:
            res.append(path)
            return
        for i in range(1, len(s) + 1):
            if self.isPal(s[:i]):
                self.dfs(s[i:], path + [s[:i]], res)

    def isPal(self, s):
        return s == s[::-1]


print(Solution().partition("aab"))

# MISTAKE
# totally ineffective solution modeled after https://leetcode.com/problems/palindromic-substrings/submissions/ and https://www.youtube.com/watch?v=4RACzI5-du8
class Solution:
    def partition(self, s: str) -> List[List[str]]:
        ra = []
        count = 0

        for i in range(len(s)):
            r = i
            l = i
            while r < len(s) and l >= 0 and s[l] == s[r]:
                count += 1
                # if s[r] != s[l]:
                #     break
                # else:
                rv = [s[r : l + 1]]
                ra.append(rv)
                r += 1
                l -= 1

            l = i
            r = i + 1
            while r < len(s) and l >= 0 and s[l] == s[r]:
                count += 1
                # if s[r] == s[l]:
                #     # break
                # # else:
                if r > l:
                    rv = [s[l : r + 1]]
                else:
                    rv = [s[r : l + 1]]
                ra.append(rv)

                r += 1
                l -= 1

        # print('r',r)
        # return ra
        return ra


# print(Solution().partition("aab"))
