# naive
# 2021-09-29: failed, had the bucket instinct but ended up looking at the discussion to get it:
# bucket = [False]* (max(nums))

# for i, v in enumerate(nums):
#     if v >= len(bucket):
#         continue
#     bucket[v] = 1

# for i, val in enumerate(bucket):
#     if val == False:
#         return i

# return len(nums)

# better: SUM from https://leetcode.com/problems/missing-number/discuss/69786/3-different-ideas%3A-XOR-SUM-Binary-Search.-Java-code
class Solution:
    def missingNumber(self, nums: List[int]) -> int:
        maxN = max(nums)
        arrs = [None] * (maxN + 1)
        miss = None

        for n in nums:
            arrs[n] = True

        for i, n in enumerate(arrs):
            if n == None:
                miss = i

        return miss
