class Solution:
    def setZeroes(self, matrix: List[List[int]]) -> None:
        """
        Do not return anything, modify matrix in-place instead.
        """
        rows = len(matrix)
        cols = len(matrix[0])

        # matrix = [
        #     [1,2,3]
        #     [1,2,3]
        #     [1,2,3]
        # ]

        zeroesR = [1 for _ in range(rows)]
        zeroesC = [1 for _ in range(cols)]

        for r in range(rows):
            for c in range(cols):
                if matrix[r][c] == 0:
                    zeroesR[r] = 0
                    zeroesC[c] = 0

        for i, r in enumerate(zeroesR):
            if r == 0:
                for j in range(cols):
                    matrix[i][j] = 0

        for i, c in enumerate(zeroesC):
            if c == 0:
                for j in range(rows):
                    matrix[j][i] = 0

