from typing import Optional

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


# 2021-08-07: coded w/o hints but fails a bit
# ended up using https://leetcode.com/problems/diameter-of-binary-tree/discuss/101145/Simple-Python
# imagine that you are just a little branch in a bigger system of stars - clearly the root does not have to be part of path
class Solution:
    def diameterOfBinaryTree(self, root: Optional[TreeNode]) -> int:
        maxim = 0
        # get depth of both
        def dfs(node, i):
            nonlocal maxim
            if not node:
                return i - 1
            if node:
                r1 = dfs(node.left, i + 1)
                r2 = dfs(node.right, i + 1)
                maxim = max(maxim, left + right)
                return r1 if r1 > r2 else r2

        left = dfs(root.left, 1)
        right = dfs(root.right, 1)

        return maxim
