from typing import List

# my suboptimal bucket solution
class Solution:
    def findDisappearedNumbers(self, nums: List[int]) -> List[int]:
        buckets = [x for x in range(0, len(nums) + 1, 1)]

        for i in nums:
            buckets[i] = False

        result = [x for x in buckets if x != False]

        return result


# still have trouble intuiting this
# observe that there must be duplicate numbers...
class Solution(object):
    def findDisappearedNumbers(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        # For each number i in nums,
        # we mark the number that i points as negative.
        # Then we filter the list, get all the indexes
        # who points to a positive number
        print("len nums", len(nums))
        for i in range(len(nums)):
            index = abs(nums[i]) - 1
            nums[index] = -abs(nums[index])

        return [i + 1 for i in range(len(nums)) if nums[i] > 0]


r = Solution().findDisappearedNumbers(
    # [5, 4, 3, 2, 7, 8, 2, 3, 1]
    [3, 1, 2, 10]
)  # [4,3,2,7,8,2,3,1] -> [5,6]
print("r", r)
