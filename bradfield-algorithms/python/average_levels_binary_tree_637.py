from typing import List

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


# https://leetcode.com/problems/average-of-levels-in-binary-tree/submissions/
class Solution:
    def averageOfLevels(self, root: TreeNode) -> List[float]:
        # if root is None:
        #     return None

        print("root", root)
        # r = [root.val
        r = []
        queue = [root]
        # queue.extend([root.left, root.right])

        while queue:
            # nodes = [queue.pop(0), queue.pop(0)]
            sum = 0
            s = len(queue)
            for _ in range(s):
                curr = queue.pop(0)
                if curr:
                    if curr.left is not None:
                        queue.append(curr.left)

                    if curr.right is not None:
                        queue.append(curr.right)

                # val1, val2 = 0, 0
                if curr is not None:
                    sum += curr.val
                #     val1 = nodes[0].val

                # if nodes[1] is not None:
                #     val2 = nodes[1].val

            avg = (sum) / s
            r.append(avg)

        return r


# tree = TreeNode(3)
# tree.left = TreeNode(9)
# tree.right = TreeNode(20)
# tree.right.left = TreeNode(15)
# tree.right.right = TreeNode(7)
# r = Solution().averageOfLevels(
#     tree
#     # [3, 9, 20, None, 15, 7],
# )
# print("r", r)

tree = TreeNode(1)
r = Solution().averageOfLevels(
    tree
    # [3, 9, 20, None, 15, 7],
)
print("r", r)
