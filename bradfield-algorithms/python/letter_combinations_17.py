from functools import reduce
from typing import List

# based upon neetcode https://www.youtube.com/watch?v=0snEunUacZY
class Solution:
    def letterCombinations(self, digits: str) -> List[str]:
        if not digits:
            return []

        map_nums_letters = {
            "2": "abc",
            "3": "def",
            "4": "ghi",
            "5": "jkl",
            "6": "mno",
            "7": "pqrs",
            "8": "tuv",
            "9": "wxyz",
        }
        r = []

        def dfs(path, idx):
            if len(path) == len(digits):
                r.append(reduce(lambda a, b: str(a) + str(b), path))
                return
            curr = digits[idx]
            letters = map_nums_letters[curr]
            for i, v in enumerate(letters):
                dfs(path + [letters[i]], idx + 1)

        dfs([], 0)

        return r


print(Solution().letterCombinations("2"))


# first pass w/o much help
class Solution:
    def letterCombinations(self, digits: str) -> List[str]:
        map_nums_letters = {
            "2": "abc",
            "3": "def",
            "4": "ghi",
            "5": "jkl",
            "6": "mno",
            "7": "pqrs",
            "8": "tuv",
            "9": "wxyz",
        }
        r = []

        path = []

        def dfs(path, avail):
            if len(path) == len(digits):
                r.append(reduce(lambda a, b: str(a) + str(b), path))
                return

            for i, v in enumerate(avail):
                dfs(path + [avail[i]], avail[i + 1 :])

        for i in range(len(digits) - 1):
            curr = digits[i]
            letters = map_nums_letters[curr]
            next_digit = digits[i + 1]
            next_letters = map_nums_letters[str(next_digit)]

            for i, v in enumerate(letters):
                dfs(path + [letters[i]], next_letters)

        if len(digits) == 1:
            curr = digits[0]
            letters = map_nums_letters[curr]
            for i, v in enumerate(letters):
                dfs(path + [letters[i]], letters[i + 1 :])

        return r


# print(Solution().letterCombinations("23"))
# print(Solution().letterCombinations("2"))
