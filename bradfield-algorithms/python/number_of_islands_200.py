from typing import List

# union find example code https://leetcode.com/problems/number-of-islands/discuss/121174/Python-Union-Find-solution-with-both-path-compression-and-union-by-rank
# also https://leetcode.com/problems/number-of-islands/discuss/121174/Python-Union-Find-solution-with-both-path-compression-and-union-by-rank/454229
# in combination w/ stackoverflow
# next up use union find to solve https://stackoverflow.com/questions/55284043/algorithm-use-union-find-to-count-number-of-islands
class Solution:
    def numIslands(self, grid: List[List[str]]) -> int:
        rows = len(grid)
        cols = len(grid[0])

        islands = 0

        visited = [[False for _ in range(cols)] for _ in range(rows)]
        directions = [
            [0, 1],  # down
            [0, -1],  # up
            [-1, 0],  # left
            [1, 0]  # right
            # [-1,-1]
        ]

        for r in range(rows):   
            for c in range(cols):
                if visited[r][c]:
                    continue
                if grid[r][c] == 0:
                    continue

                q = [(r, c)]
                while q:
                    row, col = q.pop(0)
                    if visited[row][col] == True:
                        continue

                    if grid[row][col] == "0":
                        continue

                    if grid[row][col] == "1":
                        for d in directions:
                            newRow = row + d[0]
                            newCol = col + d[1]
                            if (
                                newRow < 0
                                or newCol < 0
                                or newRow >= rows
                                or newCol >= cols
                            ):
                                continue

                            q.append((newRow, newCol))
                            visited[row][col] = True

                islands += 1

        return islands


print(
    "numIslands",
    Solution().numIslands(
        [
            ["1", "1", "1", "1", "0"],
            ["1", "1", "0", "1", "0"],
            ["1", "1", "0", "0", "0"],
            ["0", "0", "0", "0", "0"],
        ]
    ),
)
