# 2021-08-01: could not really do
# below from https://leetcode.com/problems/permutations-ii/discuss/18649/Python-easy-to-understand-backtracking-solution.
# duplicate followed by duplicate isn't necessary since the first time we
# will eventually use the second instance (??)
def permuteUnique(self, nums):
    res = []
    nums.sort()
    self.dfs(nums, [], res)
    return res


def dfs(self, nums, path, res):
    if not nums:
        res.append(path)
    for i in xrange(len(nums)):
        if i > 0 and nums[i] == nums[i - 1]:
            continue
        self.dfs(nums[:i] + nums[i + 1 :], path + [nums[i]], res)
