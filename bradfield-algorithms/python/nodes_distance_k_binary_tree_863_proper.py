# from typing import List


# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None


# # total mess solution
# # passes most of the tests but not
# # [0,1,6,2,3,null,null,7,4,null,null,null,null,null,5]
# # 1
# # 3
# #
# # solved above and ran into error at [0,1,5,3,2,15,6,4,9,8,null,null,null,11,null,null,7,18,17,13,null,12,14,10,null,null,null,19,null,null,null,null,null,null,null,null,16]
# # 16
# #  6
# class Solution:
#     def distanceK(self, root: TreeNode, target: TreeNode, k: int) -> List[int]:
#         def dfs(node, parent, direction):
#             if node is None:
#                 return None

#             node.parent = parent
#             if node.parent is not False:
#                 node.parent.isParent = True
#             node.direction = direction
#             # print('node.val', node.val)
#             if node.val == target.val:
#                 # print('returning node', node)
#                 return node

#             target1 = dfs(node.left, node, "left")
#             target2 = dfs(node.right, node, "right")

#             if target1:
#                 node.direction = "left"
#                 return target1
#             if target2:
#                 node.direction = "right"
#                 return target2

#             return None

#         # root.isParent = True
#         target = dfs(root, False, None)

#         q = [target]
#         prev = None
#         while q:
#             l = len(q)
#             for _ in range(l):
#                 curr = q.pop(0)
#                 if curr is None:
#                     continue

#                 if curr.left is not None and curr.right is not prev:
#                     q.append(curr.left)

#                 if curr.right is not None and curr.right is not prev:
#                     q.append(curr.right)

#                 prev = curr

#             pass
