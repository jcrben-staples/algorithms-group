# 2021-10-03: got much closer after trying from scratch and failing
# 2021-09-29: baffled, did not continue
class Solution:
    def maxArea(self, height: List[int]) -> int:
        # https://leetcode.com/problems/container-with-most-water/discuss/6100/Simple-and-clear-proofexplanation
        i, j = 0, len(height) - 1
        water = 0
        while i < j:
            water = max(water, (j - i) * min(height[i], height[j]))
            if height[i] < height[j]:
                i += 1
            else:
                j -= 1
        return water
