from typing import List

# TODO: study https://leetcode.com/problems/binary-search/discuss/423162/Binary-Search-101
# missed mid = (l + r) // 2 https://leetcode.com/problems/binary-search/discuss/148840/Python-typical-solutions-beat-100
class Solution:
    def search(self, nums: List[int], target: int) -> int:
        mid = len(nums) // 2

        l = 0
        r = len(nums)

        while l < len(nums) and r >= 0:
            if r - l <= 1:
                if nums[mid] == target:
                    return mid
                else:
                    return -1
            if nums[mid] == target:
                return mid
            elif target > nums[mid]:
                l = mid
                diff = r - l
                mid = (diff // 2) + mid
            else:
                r = mid
                diff = r - l + 1
                mid = (diff // 2) - mid

        return -1


# print(Solution().search([-1, 0, 3, 5, 9, 12], 9))
# print(Solution().search([-5], 5))
print(Solution().search([2, 5], 2))

# 2021-08-07: failed, mistakes include:
# forgot to update mid
# realized that I needed realmid
# realmid passes first case but fails later
# need two pointers approach
class Solution:
    def search(self, nums: List[int], target: int) -> int:
        mid = len(nums) // 2
        realmid = mid

        # l = 0
        # r = len(nums)
        while len(nums) > 1:
            if target == nums[mid]:
                return realmid
            if target > nums[mid]:
                # right half
                nums = nums[mid:]
                mid = len(nums) // 2
                realmid += mid
            else:
                nums = nums[0:mid]
                mid = len(nums) // 2
                realmid -= mid

        return nums[0]


# print(Solution().search([-1, 0, 3, 5, 9, 12], 9))
