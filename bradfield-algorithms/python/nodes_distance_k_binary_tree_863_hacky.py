from typing import List

# TODO: learn https://leetcode.com/problems/all-nodes-distance-k-in-binary-tree/discuss/143729/Python-DFS-and-BFS
# neat solution - just use DFS and a hashmap to keep track of distances

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


# total mess solution
# passes most of the tests but not
# [0,1,6,2,3,null,null,7,4,null,null,null,null,null,5]
# 1
# 3
#
# solved above and ran into error at [0,1,5,3,2,15,6,4,9,8,null,null,null,11,null,null,7,18,17,13,null,12,14,10,null,null,null,19,null,null,null,null,null,null,null,null,16]
# 16
#  6
class Solution:
    def distanceK(self, root: TreeNode, target: TreeNode, k: int) -> List[int]:
        # print('target', target)
        # find target node using dfs
        def dfs(node, parent, direction):
            if node is None:
                return None

            node.parent = parent
            if node.parent is not False:
                node.parent.isParent = True
            node.direction = direction
            # print('node.val', node.val)
            if node.val == target.val:
                # print('returning node', node)
                return node

            target1 = dfs(node.left, node, "left")
            target2 = dfs(node.right, node, "right")

            if target1:
                node.direction = "left"
                return target1
            if target2:
                node.direction = "right"
                return target2

            return None

        root.isParent = True
        target = dfs(root, False, None)
        # print('target', target)

        # if target:
        # return target.val

        q = [target]
        incr = 0
        # levels = {
        #     0: [target]
        # }
        r = []

        while q:
            if incr == k:
                return [node.val for node in q if node is not None]

            l = len(q)
            for _ in range(l):
                curr = q.pop(0)
                if curr is None:
                    continue

                if getattr(curr, "isParent", False) is not False:
                    if curr.direction == "left":
                        if curr.right:
                            curr.right.parent = False
                            curr.right.isParent = False
                        q.append(curr.right)
                        if curr.parent is not False:
                            q.append(curr.parent)
                        continue
                    elif curr.direction == "right":
                        if curr.left:
                            curr.left.parent = False
                            curr.left.isParent = False
                        q.append(curr.left)
                        if curr.parent is not False:
                            q.append(curr.parent)
                        continue

                if getattr(curr, "parent", False) is not False:
                    # print(curr.val)
                    # curr.parent.isParent = True
                    # curr.parent.priorDirection = 'left'
                    q.append(curr.parent)

                if curr.left is not None:
                    q.append(curr.left)

                if curr.right is not None:
                    q.append(curr.right)

            incr += 1

        return []


# tree = TreeNode(3)
# tree.left = TreeNode(9)
# tree.right = TreeNode(20)
# tree.right.left = TreeNode(15)
# tree.right.right = TreeNode(7)
# [3,5,1,6,2,0,8,null,null,7,4]

# tree = TreeNode(3)
# tree.left = TreeNode(5)
# tree.left.left = TreeNode(6)
# tree.left.right = TreeNode(2)
# tree.left.right.left = TreeNode(7)
# tree.left.right.right = TreeNode(4)

# tree.right = TreeNode(1)
# tree.right.left = TreeNode(0)
# tree.right.right = TreeNode(8)
# print(Solution().distanceK(tree, TreeNode(5), 2))
# fmt: off
# arr = [0,1,5,3,2,15,6,4,9,8,None,None,None,11,None,None,7,18,17,13,None,12,14,10,None,None,None,19,None,None,None,None,None,None,None,None,16]
# 16
# 6
arr = [0,6,1,None,None,None,2,7,3,None,8,4,9,None,None,None,5]
# fmt: on

# arr = [0, 1, 6, 2, 3, None, None, 7, 4, None, None, None, None, None, 5]
# arr = [3, 5, 1, 6, 2, 0, 8, None, None, 7, 4]
# root = None
hash = {}
# https://stackoverflow.com/questions/37941318/how-to-build-an-incomplete-binary-tree-from-array-representation
# use a queue http://analgorithmaday.blogspot.com/2013/05/create-binary-tree-using-array.html
# for i, val in enumerate(arr, start=1):
#     if i < len(arr):
#         node = TreeNode(arr[i])
#         hash[i] = node
#         if root is None:
#             root = node
#         if len(arr) > (2 * i + 1):
#             node.left = hash[2 * i + 1] or TreeNode(arr[2 * i + 1])
#         if len(arr) > (2 * i + 2):
#             node.right = TreeNode(arr[2 * (i + 1)])
root = TreeNode(arr[0])
q = [root]

i = 0
while q and i < len(arr):
    curr = q.pop(0)
    left = None
    right = None

    if len(arr) > (2 * i + 1):
        left = TreeNode(arr[2 * i + 1]) if arr[2 * i + 1] is not None else None
        curr.left = left
        if left is not None:
            q.append(left)
    if len(arr) > (2 * (i + 1)):
        right = TreeNode(arr[2 * (i + 1)]) if arr[2 * (i + 1)] is not None else None
        curr.right = right
        if right is not None:
            q.append(right)
        # [2 * (i + 1)]
    i += 1

print(Solution().distanceK(root, TreeNode(7), 5))
# print("node", root)
