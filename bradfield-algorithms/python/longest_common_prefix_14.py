class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        r = ""
        n = len(strs)
        m = min([len(i) for i in strs])

        for i in range(0, m):
            l = strs[0][i]
            match = True
            for j in range(1, len(strs)):
                if strs[j][i] == l:
                    continue
                else:
                    match = False
                    return r

            if match:
                r += l

        return r
        # if strs[j][i] == l

