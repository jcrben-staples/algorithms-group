from functools import reduce
from typing import List

# 2021-08-02: did it close to by myself
# missed that I needed to pass avail[i:] rather than avail
# wrong w/o: [[2, 2, 3], [2, 3, 2], [3, 2, 2], [7]]
# right with: [[2, 2, 3], [7]]
class Solution:
    def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:
        r = []

        def dfs(path, avail):
            sumSoFar = reduce(lambda a, b: a + b, path) if len(path) > 0 else 0
            if sumSoFar > target:
                return

            if sumSoFar == target:
                r.append(path.copy())
                return

            for i in range(len(avail)):
                curr = avail[i]
                path.append(curr)
                dfs(path, avail[i:])
                path.pop()

        dfs([], candidates)
        return r


# print(Solution().combinationSum([1, 2, 3], 7))
print(Solution().combinationSum([2, 3, 6, 7], 7))
