from typing import List


# solved without help;
# also did version 1 (102) easily https://leetcode.com/problems/binary-tree-level-order-traversal
# also did 103 zigzag easily - just use mod % 2 == 0 to flip and insert rather than append
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def levelOrderBottom(self, root: TreeNode) -> List[List[int]]:
        if root is None:
            return []

        q = [root]
        level = 0
        second_q = {level: [root]}
        r = []
        while q:
            l = len(q)

            for _ in range(l):
                curr = q.pop(0)
                if curr is None:
                    continue

                if curr.left is None and curr.right is None:
                    continue
                if second_q.get(level + 1, False) is False:
                    # note: got keyerror 1 for failing to initialize this before accessing
                    second_q[level + 1] = []

                if curr.left is not None:
                    q.append(curr.left)
                    second_q[level + 1].append(curr.left)

                if curr.right is not None:
                    q.append(curr.right)
                    second_q[level + 1].append(curr.right)

            level += 1

        for i in range(len(second_q), 0, -1):
            curr_level = second_q[i - 1]
            level_r = []

            for node in curr_level:
                if node is not None:
                    level_r.append(node.val)
            r.append(level_r)

        return r


tree = TreeNode(3)
tree.left = TreeNode(9)
tree.right = TreeNode(20)
tree.right.left = TreeNode(15)
tree.right.right = TreeNode(7)
print(Solution().levelOrderBottom(tree))
