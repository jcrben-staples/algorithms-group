"""
# Definition for a Node.
"""


class Node:
    def __init__(self, val=0, neighbors=None):
        self.val = val
        self.neighbors = neighbors if neighbors is not None else []


# input
adjList = [[2, 4], [1, 3], [2, 4], [1, 3]]

# TODO: make this recursive

# dummy node
hash = {}
for i, x in enumerate(adjList, start=1):
    n = hash[i] if hash.get(i, False) else Node(i)
    hash[i] = n
    for j in x:
        # print("x", x)
        # a neighbor
        curr = hash[j] if hash.get(j, False) else Node(j)
        n.neighbors.append(curr)
        # if curr.neighbors

dummy = Node(None)
dummy.neighbors = [hash.get(1)]
# n.neighbors =


class Solution:
    def cloneGraph(self, node: "Node") -> "Node":
        print("node", node.neighbors)
        print("n1", node.neighbors[1].val)

        # recursively print graph
        def dfs(node):
            print("dfs-val", node.val)
            print("dfs-neighbors", node.val)

        pass


Solution().cloneGraph(hash.get(1))

# proper solution from # [Clone Graph - Depth First Search - Leetcode 133](https://www.youtube.com/watch?v=mQeF6bN8hMk)
# class Solution:
#     def cloneGraph(self, node: 'Node') -> 'Node':
#         oldToNew = {}

#         def dfs(node):
#             if node in oldToNew:
#                 return oldToNew[node]

#             copy = Node(node.val)
#             oldToNew[node] = copy
#             for x in node.neighbors:
#                 # if x is not None:
#                 copy.neighbors.append(dfs(x))
#             return copy

#         return dfs(node)

