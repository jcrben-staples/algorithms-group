# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

# 2021-08-07: successful first try w/o help
class Solution:
    def isSameTree(self, p: Optional[TreeNode], q: Optional[TreeNode]) -> bool:
        def dfs(node1, node2):
            if not node1 and not node2:
                return True
            if not node1 or not node2:
                return False

            if node1.val != node2.val:
                return False

            r1 = dfs(node1.left, node2.left)
            r2 = dfs(node1.right, node2.right)

            return r1 == True and r2 == True

        return False if not dfs(p, q) else True
