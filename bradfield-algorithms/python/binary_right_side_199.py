# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
# 2021-07-31: completed w/o help or hints
class Solution:
    def rightSideView(self, root: TreeNode) -> List[int]:
        # for each level, add the rightmost value to the result
        if root is None:
            return []

        q = [root]

        r = []

        while q:
            if len(q) > 1:
                r.append(q[len(q) - 1].val)
            else:
                r.append(q[0].val)
            for _ in range(len(q)):

                curr = q.pop(0)
                if curr is None:
                    continue

                if curr.left is not None:
                    q.append(curr.left)

                if curr.right is not None:
                    q.append(curr.right)

        return r
