# SEE ALSO: topological_sort.py
# class Node:
#     def __init__(self, children):
#         self.children = children

#     def traverse(self):
#         if (!len(children))
#         for child in children:
#             child.traverse


class Solution:
    def canFinish(self, numCourses: int, prerequisites: List[List[int]]) -> bool:
        edge_mapping: Dict[str, List[int]] = {str(i): [] for i in range(numCourses)}

        parent: Dict[str, Union[None, str]] = {}

        for x in prerequisites:
            edge_mapping[str(x[0])].append(x[1])

        visited: Dict[str, Union[bool, List[int], Any]] = {}
        for node in edge_mapping.items():
            key = node[0]

            children = node[1]

            parent[key] = None

            # ignore per per https://github.com/python/typeshed/issues/2383
            stack: List[int] = [int(key)]

            visiting: Dict[str, bool] = {}
            while stack:
                current = str(stack.pop())
                children = edge_mapping[current]
                visiting[current] = True

                for i in children:
                    if visited.get(str(i), False):
                        continue

                    if visiting.get(str(i), False):
                        return False

                    stack += [i]

                if not children or (
                    len(
                        list(filter(lambda x: visited.get(str(x), False), children))
                    )  # type: ignore
                    == len(children)
                ):
                    visiting.pop(current, None)
                    visited[current] = True

        return True
