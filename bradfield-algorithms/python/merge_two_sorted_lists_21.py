from typing import Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


# https://www.youtube.com/watch?v=XIdigk956u0
#
class Solution:
    def mergeTwoLists(
        self, l1: Optional[ListNode], l2: Optional[ListNode]
    ) -> Optional[ListNode]:
        dummy = ListNode()  # dummy node
        tail = dummy

        while l1 and l2:
            if l1.val < l2.val:
                tail.next = l1
                l1 = l1.next
            else:
                tail.next = l2
                l2 = l2.next

        if l1:
            tail.next = l1
        elif l2:
            tail.next = l2

        return dummy.next


# 2021-08-10: failed but got sorta close
# mistakes:
# and versus or for comparison
# modified existing list instead of the new one properly
class Solution:
    def mergeTwoLists(
        self, l1: Optional[ListNode], l2: Optional[ListNode]
    ) -> Optional[ListNode]:
        r = ListNode()  # dummy node
        r.next = l1 if l1.val < l2.val else l2

        curr = r.next
        while l1 and l2:

            if l1.val < l2.val:
                l1.next = l2
                l1 = l1.next
                # l2 = l2.next
                curr = l2
            else:
                l2.next = l1
                l2 = l2.next
                # l1 = l1.next
                curr = l1

        if l1:
            curr.next = l1
            # l2 = l2.next
            # continue
        else:
            curr.next = l2
            # l1 = l1.next
            # continue

        return r.next
