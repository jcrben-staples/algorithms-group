from typing import Union, Optional, Any


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


def reverseList(head: ListNode) -> ListNode:
    # if not head:
    #     return None

    # newHead = head

    if head.next:
        reverseList(head.next)
        head.prev = head.next
        head.next.next = head
    # head.next = None

    return None


a = ListNode(1)
b = ListNode(2, a)
c = ListNode(3, b)
d = ListNode(3, c)
a.next = None

# print("c", c)

reverseList(d)

d = a.next
print("d", d)
