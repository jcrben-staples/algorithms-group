# 2021-09-29: starting off,  baffled a bit...
# starting off ok I think...
# got stuck - was jumping ahead to restart at p2 on fail and forgot to remove from the set...
# played around a bit after the hint and got it
class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        if len(s) == 0:
            return 0

        r = 1
        p1 = 0
        p2 = 1

        curr = r
        curr_chars = set(s[p1])

        while p2 < len(s):
            if s[p2] not in curr_chars:
                curr_chars.add(s[p2])
                p2 += 1
                curr += 1
            else:
                r = max(curr, r)
                while s[p2] in curr_chars:
                    curr_chars.remove(s[p1])
                    p1 += 1
                    curr -= 1
                # r = max(r, curr)
                # curr = 1
                # p1 = p2
                # p2 = p1+1
                # curr_chars = set(s[p1]) if p1 < len(s) else set()

        return max(r, curr)


r = Solution().lengthOfLongestSubstring("abcabcbb")
print("r", r)

# 2021-07-21: after long effort, solved by looking at sliding window answer... https://leetcode.com/submissions/detail/521602743/
# if len(s) == 0 or len(s) == 1:
#     return len(s)

# l = 0
# r = 1

# longest = 0
# result = ""

# chars = set()
# chars.add(s[0])

# while l < r and r < len(s):
#     c = s[r]

#     if c in chars:
#         while c in chars and l <= r:
#             p = s[l]
#             chars.remove(p)
#             l += 1

#         # chars.add(c)
#         # r += 1

#         # longest = max(longest, len(s[l + 1 : r + 1]))
#         # result = s[l + 1 : r + 1]
#         # l += 1
#     # else:
#     #     chars.remove(p)
#     #     chars.add(c)

#     chars.add(c)
#     longest = max(longest, len(s[l : r + 1]))
#     r += 1

# return max(longest, len(s[l + 1 : r + 1]))

# brute force - this doesn't even work
# class Solution:
#     def lengthOfLongestSubstring(self, s: str) -> int:
#         if s == " ":
#             return 1
#         if len(s) == 1:
#             return 1
#         result = ""
#         longest = 0
#         dp = []

#         def dfs(ss, avail, j):
#             nonlocal result, longest
#             ls = len(ss)
#             if ls > 1:
#                 for i in range(len(ss) - 1):
#                     if ss[i] == ss[ls - 1]:
#                         if len(ss[0:-1]) > longest:
#                             longest = len(ss[0:-1])
#                             result = ss[0:-1]
#                             return
#                         return
#                 # if ls > 1 and ss[ls - 1] == ss[ls - 2]:

#                 #     if len(ss) - 1 > len(result):
#                 #         result = ss[0:-1]
#                 #     return

#                 # while (j < len(avail)):
#                 # for i in range(len(avail)):
#                 # dp[i+1] =
#                 # dp[i] =
#             if len(avail) > 0:
#                 dfs(ss + avail[0], avail[1:], j + 1)
#             # elif len(avail) == 1:
#             #     dfs(ss + avail[0], avail[j:], j)
#             # dfs(ss + avail[i + 1 : i + 2], avail[i + 2 :])

#         for i in range(len(s)):
#             dfs(s[i], s[i + 1 :], 1)
#         return len(result)


# # r = Solution().lengthOfLongestSubstring("abcabcbb")
# # r = Solution().lengthOfLongestSubstring("bbbbb")
# # r = Solution().lengthOfLongestSubstring("pwwkew")
# r = Solution().lengthOfLongestSubstring("au")
# print("r", r)
