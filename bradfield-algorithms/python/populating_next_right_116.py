# Definition for a Node.
class TreeNode:
    def __init__(
        self,
        val: int = 0,
        left: "Node" = None,
        right: "Node" = None,
        next: "Node" = None,
    ):
        self.val = val
        self.left = left
        self.right = right
        self.next = next


# 2021-07-31: completed w/o help but uses O(n) space
class Solution:
    def connect(self, root):
        # proper solution copied from https://leetcode.com/problems/populating-next-right-pointers-in-each-node/discuss/37472/A-simple-accepted-solution
        it = root
        curr = None

        while it and it.left:
            curr = it

            while curr:
                curr.left.next = curr.right
                if curr.next:
                    curr.right.next = curr.next.left
                curr = curr.next

            it = it.left

        return root

        # my solution
        # q = [root]

        # while q:
        #     l = len(q)

        #     for i in range(l):
        #         curr = q.pop(0)
        #         nextRight = q[0] if (i < (l - 1) and len(q) > 0) else None
        #         if curr is None:
        #             continue

        #         curr.next = nextRight

        #         if curr.left is not None:
        #             q.append(curr.left)

        #         if curr.right is not None:
        #             q.append(curr.right)

        # return root


tree = TreeNode(1)
tree.left = TreeNode(2)
tree.right = TreeNode(3)
tree.left.left = TreeNode(4)
tree.left.right = TreeNode(5)
tree.right.left = TreeNode(6)
tree.right.right = TreeNode(7)
print(Solution().connect(tree))
