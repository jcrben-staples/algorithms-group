from typing import List

# FUN THOUGHT EXPERIMENT:
# Imagine instead you are walking thru doors into a strange new plane
# at each plane, there is a pot of gold
# you want to reach X amount of gold

# amazing diagram https://leetcode.com/problems/combination-sum-ii/discuss/16861/Java-solution-using-dfs-easy-understand/977097
# major mistake: forgot to sort
# also pass in i+1 as the new idx, not idx+1
class Solution:
    def combinationSum2(self, candidates: List[int], target: int) -> List[List[int]]:
        r = []
        candidates.sort()

        def dfs(idx, path, targetAfterSubtracting, avail):
            # targetAfterSubtracting -= path[len(path)-1] if len(path) > 0 else 0
            if (targetAfterSubtracting) == 0:
                r.append(path.copy())
                return
            if (targetAfterSubtracting) < 0:
                return
            for i in range(idx, len(avail)):
                if i > idx and avail[i - 1] == avail[i]:
                    continue
                path += [avail[i]]
                # (path + [avail[i]]).copy()
                dfs(i + 1, path, targetAfterSubtracting - avail[i], avail)
                path.pop()

        dfs(0, [], target, candidates)

        return r


print(Solution().combinationSum2([10, 1, 2, 7, 6, 1, 5], 8))
