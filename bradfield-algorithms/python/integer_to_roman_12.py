# Amazon Coding Interview Question - Integer to Roman (LeetCode) https://www.youtube.com/watch?v=yzB4M-UXqgI
class Solution:
    def intToRoman(self, num) -> str:
        numerals = {
            "M": 1000,
            "CM": 900,
            "D": 500,
            "CD": 400,
            "C": 100,
            "XC": 90,
            "L": 50,
            "XL": 40,
            "X": 10,
            "IX": 9,
            "V": 5,
            "IV": 4,
            "I": 1,
        }

        r = ""

        # numbers = [1000, 500, 100, 50, 10, 5, 1]

        # divisor = int // 10
        # remainer = int % 10

        # divide by largest numeral
        # remember this https://stackoverflow.com/questions/36244380/enumerate-for-dictionary-in-python
        for numeral, value in numerals.items():
            # print("value", numeral)
            repeats = num // value
            if repeats == 0:
                continue

            for _ in range(repeats):
                r += numeral

            num %= value
            if num == 0:
                return r

        return r


print("intToRoman", Solution().intToRoman(2300))
