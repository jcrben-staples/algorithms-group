# Success! without checking notes or prior solutions
class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        # sliding window...

        if len(s) == 0 or len(s) == 1:
            return len(s)

        l = 0
        r = 1

        longest = 0
        result = ""

        chars = set()
        chars.add(s[0])

        while l < r and r < len(s):
            c = s[r]

            if c in chars:
                while c in chars and l <= r:
                    p = s[l]
                    chars.remove(p)
                    l += 1

                # chars.add(c)
                # r += 1

                # longest = max(longest, len(s[l + 1 : r + 1]))
                # result = s[l + 1 : r + 1]
                # l += 1
            # else:
            #     chars.remove(p)
            #     chars.add(c)

            chars.add(c)
            longest = max(longest, len(s[l : r + 1]))
            r += 1

        return max(longest, len(s[l + 1 : r + 1]))


# r = Solution().lengthOfLongestSubstring("abcabcbb")
r = Solution().lengthOfLongestSubstring("pwwkew")
