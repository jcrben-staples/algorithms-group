from typing import List

# https://leetcode.com/problems/3sum/
# https://www.youtube.com/watch?v=jXZDUdHRbhY
# brute force - time limit exceeded
class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        # target = 0
        r = {}
        # r = []
        nums.sort()
        # numbers_dict = {}
        for i in range(len(nums)):
            # numbers_dict[i] += 1
            for j in range(i + 1, len(nums)):
                # partial_sum = nums[i] + nums[j]
                for k in range(j + 1, len(nums)):
                    if nums[i] + nums[j] + nums[k] == 0:
                        key = str(nums[i]) + str(nums[j]) + str(nums[k])
                        r[key] = [nums[i], nums[j], nums[k]]
                        # r.append([nums[i], nums[j], nums[k]])

        # return r
        return r.values()


# TODO: binary search # https://leetcode.com/problems/3sum/discuss/281302/JavaScript-with-lots-of-explanatory-comments!

# hash table w/ complement
# class Solution:
#     def threeSum(self, nums: List[int]) -> List[List[int]]:
#         pass


tests = (
    [[-1, 0, 1, 2, -1, -4], 0, [[-1, -1, 2], [-1, 0, 1]]],  # first
    [[-1, 0, 1, 2, -1, -4], 0, [[-1, -1, 2], [-1, 0, 1]]],
)

for input, target, expected in tests:
    print(f"{expected == list(Solution().threeSum(input))}")
    #  and got {Solution().threeSum(input)}"
# from https://leetcode.com/problems/3sum/discuss/396477/javascript-n3-times-out-311313-test-cases.-Send-Halp.
# var threeSum = function(nums) {
#     if (nums.length < 3) {
#         return [];
#     }
#     let result = {}
#     nums.sort(function(a,b){return a-b})
#     for (let i = 0; i < nums.length; i ++) {
#         for (let j = i + 1; j < nums.length; j++) {
#             for(let k = j+1; k < nums.length; k++) {
#                 if (nums[i] + nums[j] + nums[k] === 0) {
#                     let key = '' + nums[i] + nums[j] + nums[k]
#                     if (!result[key]) {
#                       result[key] = [nums[i], nums[j], nums[k]]
#                     }
#                 }
#             }
#         }
#     }
#     return Object.values(result)
# };
