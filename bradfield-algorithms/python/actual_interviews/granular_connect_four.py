"""
for this simple game, use integers to represent players
"""

from functools import reduce


class ConnectFour:
    # array indices
    column_border = 6
    row_border = 5

    # left, left upper,  right upper, right, up, down
    _directions = [[0, -1], [-1, -1], [1, 1], [0, 1], [-1, 0], [1, 0]]
    _grid = [
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
    ]

    def place(self, column, player):
        print("self._grid", self._grid)
        for i, row in reversed(list(enumerate(self._grid))):
            if self._grid[i][column] == 0:
                self._grid[i][column] = player
                return

    def _traverse_direction(self, direction, player, loc=[0, 0], total=0):

        if self._grid[loc[0]][loc[1]] == player:
            total += 1

        # base case - win
        if total == 4:
            return True

        # base case - hit another player
        # if self._grid[loc[0][loc[1]]] != 0 and self._grid[loc[0][loc[1]]] != player:
        #     return false

        possibleNeighbors = [([loc[0] + x, loc[1] + y]) for x, y in self._directions]
        for x, y in possibleNeighbors:
            if (x >= 0 and x <= self.column_border) and (
                y >= 0 and y <= self.row_border
            ):
                return self._traverse_direction(direction, player, [x, y], total)

        return False

    def _is_win_simple(self, player):
        """
        incomplete - idea is to
        1. look for the win in each row while looking for the win in each column
        2. 

        """
        # look for win in each row
        for row in self._grid:
            total_row = 0
            total_column = 0
            for x in row:
                if x == player:
                    total_row += 1

            # vals = reduce(lambda a: if a+1 if b == player else a+0, row)

    def is_win(self, player):
        origin = [0, 0]
        players = [1, 2]

        for player in players:
            self._is_win_simple(player)

    def is_win_recursive(self):
        """
        not sure if this ultimately works
        """
        origin = [0, 0]
        players = [1, 2]

        for player in players:
            for d in self._directions:
                if self._traverse_direction(d, player):
                    return True

        return False


game = ConnectFour()
game.place(0, 1)
game.place(0, 1)
game.place(0, 1)
# game.place(0, 1)
game.place(0, 2)
print("game", game._grid)

# game.is_win(1)
print("game.is_win_recursive()", game.is_win_recursive())
