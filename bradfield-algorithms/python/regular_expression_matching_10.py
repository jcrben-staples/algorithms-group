# brute force naive attempt - illustrates how crude / tough it is
# basically shows how hard backtracking w/o using stacks or recursion is...
# https://leetcode.com/problems/regular-expression-matching/discuss/439697/C%2B%2B-back-to-front-blocked-by-test-case managed to dit
# see neetcode: Regular Expression Matching - Dynamic Programming Top-Down Memoization - Leetcode 10 https://www.youtube.com/watch?app=desktop&v=HAA8mgxlov8&list=PLot-Xpze53lcvx_tjrr_m2lgD2NsRHlNO&index=16
class Solution:
    def isMatch(self, s: str, p: str) -> bool:
        #
        lS, rS = 0, len(s)
        lP, rP = 0, len(p)
        flag = False

        if p == ".*":
            return True

        while (lS < rS) and (lP < rP):
            curr_c = s[lS]
            curr_p = p[lP]
            if lP + 1 < rP:
                curr_is_star = True if p[lP + 1] == "*" else False
            else:
                curr_is_star = False
            # is_match = s[lP]

            if not curr_is_star:
                if (curr_p == curr_c) or curr_p == ".":
                    lS += 1
                    lP += 1
                else:
                    return False
            else:  # is star
                if not (curr_p == curr_c or curr_p == "."):
                    lP += 2
                    # lS += 1
                    continue
                else:
                    flag = True
                    # TODO: need to handle this recursively or w/ while loop
                    while curr_p == curr_c or curr_p == ".":
                        lS += 1
                        if lS < len(s):
                            curr_c = s[lS]
                        else:
                            break

                    if lP == rP - 1:
                        return True  # wrong, fails for "ab", ".*c"
                    while lP < rP:
                        # if p[]
                        if len(p) > lP + 1:
                            if p[lP + 1] == "*":
                                lP += 2
                            else:
                                return False
                                # break
                        else:
                            # check last digit
                            curr_p = p[lP]
                            if curr_p == curr_c or curr_p == ".":
                                return True
                            else:
                                return False
                            # break
                            # lS -= 1
                            # break
                        # curr_p = p[lP]
                    lP += 2

                # while:
                # while curr_c == curr_p or curr_p == '.':

        while lP < rP:
            if len(p) > lP + 1:
                if p[lP + 1] == "*":
                    lP += 2
                else:
                    return False
                    # break
            else:
                break

        if lP >= rP and lS < rS and not flag:
            return False
        if lP < rP and lS >= rS and not flag:
            return False
        elif lP < rP and lS >= rS - 1:
            return False
        # lP < rP and
        elif lS < rS:
            return False
        # elif lS < (rS - 1):
        #     return False

        return True


# r = Solution().isMatch("aa", "a")  # false
# r = Solution().isMatch("aab", "c*a*b")  # true
# r = Solution().isMatch("ab", ".*")  # true
r = Solution().isMatch("mississippi", "mis*is*ip*.")  # true
# r = Solution().isMatch("aa", "a*")  # true
# r = Solution().isMatch("aaa", "aaaa")  # false
# r = Solution().isMatch("aaa", "a*a")  # true
# r = Solution().isMatch("aa", "a")  # false
# r = Solution().isMatch("a", "ab*")  # false
# r = Solution().isMatch("bbbba", ".*a*a")  # true
# r = Solution().isMatch("ab", ".*c")  # false
# r = Solution().isMatch("aab", "c*a*b")  # true
# r = Solution().isMatch("aa", "a*")  # true
print("r", r)
# optimal solution: https://leetcode.com/problems/regular-expression-matching/discuss/5651/Easy-DP-Java-Solution-with-detailed-Explanation/238767
# // M[i][j] represents if the 1st i characters in s can match the 1st j characters in p
# M[i][0] = false as
