# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def minDepth(self, root: TreeNode) -> int:
        if root is None:
            return 0
        queue = [root]
        r = len(queue)

        while len(queue) > 0:
            l = len(queue)
            for _ in range(l):
                curr = queue.pop(0)
                if curr is None:
                    continue
                if curr.left is None and curr.right is None:
                    return r
                else:
                    queue.append(curr.left)
                    queue.append(curr.right)

            r += 1

        return r

