# 2021-09-30: can't remember at all how I did this...
# got the sense of it
# class Solution:
#     def convert(self, s: str, numRows: int) -> str:
#         currRow = 0
#         outputPerRow = [[] for col in range(1) for row in range(numRows)]
#         currDir = 'down'
#         i = 0

#         while i < len(s):
#             while currRow < numRows and currDir == 'down':
#                 outputPerRow[currRow].append(s[i])
#                 currRow += 1

#                 if currRow > numRows:
#                     currDir == 'up'


# 2021-07-12: did via memory + debugging after looking at the problem day before
# took like nearly an hour, made a bunch of mistakes
class Solution:
    def convert(self, s: str, numRows: int) -> str:
        if numRows == 1:
            return s

        output = ["" for i in range(numRows)]

        # for i in numRows:
        # outp

        j = 0
        i = 0
        reverse = False
        while i < len(s):
            if j < numRows and not reverse:
                output[j] += s[i]
                j += 1
                i += 1
            elif j > 0 and reverse:
                output[j] += s[i]
                j -= 1
                i += 1
            elif j == numRows or j == 0:
                reverse = False if reverse else True
                if j == 0:
                    j = 0
                    # output[j] += s[i]
                elif j == numRows:
                    j = numRows - 2
                    # output[j] += s[i]
                continue

        return "".join(output)


r = Solution().convert("PAYPALISHIRING", 3)
print("r", r)
