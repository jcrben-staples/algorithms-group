from typing import List

# watch Tushar Roy https://www.youtube.com/watch?v=nYFd7VHKyWQ from https://leetcode.com/problems/permutations-ii/discuss/18594/Really-easy-Java-solution-much-easier-than-the-solutions-with-very-high-vote
# 2021-08-01: completed w/o too much trouble, altho forgot a bit about the specifics of slicing
# see also anagrams from javascript
class Solution:
    def permute(self, nums: List[int]) -> List[List[int]]:
        r = []

        def dfs(sofar, avail):
            if len(sofar) == len(nums):
                r.append(sofar)
                return

            for i in range(len(avail)):
                # second =
                dfs(
                    sofar + [avail[i]],
                    avail[0:i] + (avail[i + 1 :] if i < len(nums) else []),
                )

        dfs([], nums)

        return r


print(Solution().permute([1, 2, 3]))
