from typing import Optional

# 2021-08-07: first pass did not accommodate negative numbers...
# key was not to not bail early since we can always go backwards...

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def hasPathSum(self, root: Optional[TreeNode], targetSum: int) -> bool:
        def dfs(node, t):
            if not node:
                return False

            t += node.val
            if t == targetSum and (not node.left and not node.right):
                return True
            if t > targetSum:
                return False

            if dfs(node.left, t) or dfs(node.right, t):
                return True

        return dfs(root, 0)


tree = TreeNode(-2)
tree.left = TreeNode(None)
tree.right = TreeNode(-3)
print(Solution().hasPathSum(tree, -5))

# -5
