# https://leetcode.com/problems/longest-substring-without-repeating-characters/

# below solution from neetcoee and written by me
class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        chars_included = set()
        result = 0

        l = 0
        # chars_included.add(s[l])

        for r in range(len(s)):
            # r += 1
            r_char = s[r]
            # duplicate...
            while r_char in chars_included:
                # chars_included.remove(r_char)
                chars_included.remove(s[l])
                l += 1
                # l_char = s[l]
                # chars_included.add(l_char)
                # move l forward...?

            chars_included.add(r_char)
            result = max(result, r - l + 1)

        return result


r = Solution().lengthOfLongestSubstring("abcabcbb")
print("r", r)


# below solution copied from someone else
class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        chars = [0] * 128

        left = right = 0

        res = 0
        while right < len(s):
            r = s[right]
            chars[ord(r)] += 1

            while chars[ord(r)] > 1:
                l = s[left]
                chars[ord(l)] -= 1
                left += 1

            res = max(res, right - left + 1)

            right += 1
        return res


class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        chars_included = set()
        result = 0

        l = 0
        # chars_included.add(s[l])

        for r in range(len(s)):
            # r += 1
            r_char = s[r]
            # duplicate...
            while r_char in chars_included:
                # chars_included.remove(r_char)
                chars_included.remove(s[l])
                l += 1
                # l_char = s[l]
                # chars_included.add(l_char)
                # move l forward...?

            chars_included.add(r_char)
            result = max(result, r - l + 1)

        return result


r = Solution().lengthOfLongestSubstring("cbbd")
print("r", r)

