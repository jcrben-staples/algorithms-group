import os
import re
import shutil

target = "/Users/bencreasy/sync/health/ibd"
prefix = "gastro_"


def rename_folders():
    files = os.listdir(target)
    print('files', files)

    for f in files:
        match = re.search(f"^{prefix}(.*)", f)
        if match is not None:
            match = match.group(1)
            print("match", match)

            if match != "":
                shutil.move(f"{target}/{f}", f"{target}/{match}")


rename_folders()
