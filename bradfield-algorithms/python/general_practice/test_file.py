print("globals", globals())

# "__file__ is the pathname of the file from which the module was loaded, if it was loaded from a file" - confusingly worded since it could be interpreted to mean importing file
# also see https://stackoverflow.com/questions/4934806/how-can-i-find-scripts-directory-with-python#comment33438371_9350788 where there is an detour into running in an "interpreter"
print("__file__", __file__)


def fn1():
    print("hi")

