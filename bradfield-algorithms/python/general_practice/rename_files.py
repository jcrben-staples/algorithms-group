"""
small task was more time consuming than I expected!


"""


import os
import shutil
import re
import test_file

print("globals", globals())
test_file.fn1()


def extract_files_no_extension():
    # per https://stackoverflow.com/a/6416333/4200039
    script_dir = os.path.dirname(os.path.realpath(__file__))
    dest = f"{script_dir}/rename"
    files = os.listdir(dest)

    files_no_ext = []
    for x in files:
        # shutil.copy(x, f"{x}-copy")
        # https://docs.python.org/3/library/re.html#re.search
        # search is the one to use - re.match should be deprecated? https://stackoverflow.com/a/37363575/4200039
        f_no_ext = re.search(".*(?=\.)", x).group()

        if f_no_ext != "":
            os.rename(f"{dest}/{x}", f"{dest}/{f_no_ext}-cloud.svg")


extract_files_no_extension()
