# could not do it, watched  https://www.youtube.com/watch?v=fMSJSS7eO1w

# see also codesignal_rotate_matrix.py

# works
class Solution:
    def rotate(self, matrix: List[List[int]]) -> None:
        """
        Do not return anything, modify matrix in-place instead.
        """
        l, r = 0, len(matrix) - 1

        while l < r:
            for i in range(r - l):
                top, bottom = l, r

                topLeft = matrix[top][l + i]
                # must be moved to topRight
                topRight = matrix[top + i][r]
                bottomRight = matrix[bottom][r - i]
                bottomLeft = matrix[bottom - i][l]

                # topLeft -> bottomLeft
                matrix[top][l + i] = bottomLeft

                # topRight-> topLeft
                matrix[top + i][r] = topLeft

                # bottomRight -> bottomLeft
                matrix[bottom][r - i] = topRight

                # bottomLeft -> bottomRight
                matrix[bottom - i][l] = bottomRight

                # bottomLeft -> bottomRight
                # move topRight to bottomRight
                # bottomRight = matrix[bottom + i][r]
                # matrix[bottom + i][r] = topRight
                # bottomLeft = matrix[bottom + i][]
            r -= 1
            l += 1


# attempt #1
class Solution:
    def rotate(self, matrix: List[List[int]]) -> None:
        """
        Do not return anything, modify matrix in-place instead.
        """
        n = len(matrix)
        # rows = len(matrix)
        # cols = len(matrix[0])
        row = True
        for i in range(len(n)):
            for j in range(len(n)):
                if row:
                    tmp1 = matrix[i][j]
                    matrix[i][j] = matrix[n - i][j]  # replace tmp
                    tmp2 = matrix[j][n - i]
                    matrix[j][n - i] = tmp1
                    matrix[j + 1][n - i] = tmp2
                    # matrix[n][i] = tmp1
