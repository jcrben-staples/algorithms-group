from typing import List


class Solution:
    def letterCasePermutation(self, s: str) -> List[str]:
        chars = list(s)
        r = []

        def dfs(i, new_chars):
            if i >= len(new_chars):
                r.append(new_chars.copy())
                return

            # two options: capitalize or not
            if new_chars[i].isdigit() is True:
                dfs(i + 1, new_chars)
                return
                # new_chars[i] = new_chars[i].upper()
                # r.append(new_chars)
            # else:
            # continue
            # pass

            new_chars[i] = new_chars[i].lower()
            dfs(i + 1, new_chars)

            new_chars[i] = new_chars[i].upper()
            dfs(i + 1, new_chars)

        dfs(0, chars)
        return ["".join(chars) for chars in r]


print(Solution().letterCasePermutation("a1b2"))
