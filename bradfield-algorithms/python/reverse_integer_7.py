# 2021-09-30: couldn't figure it out, had to review the solution which recommends modulus
# originally failed before adding the number checks
# official solution recommends using a stack
class Solution:
    def reverse(self, x: int) -> int:
        s = str(x)
        negative = False
        if s[0] == "-":
            negative = True
            s = s[1:]

        rS = []

        for i in range(len(s), 0, -1):
            rS.append(s[i - 1])

        if negative:
            rS.insert(0, "-")

        r = int("".join(rS))

        if (r > 2 ** 31) or (r < -(2 ** 31)):
            return 0
        else:
            return r
