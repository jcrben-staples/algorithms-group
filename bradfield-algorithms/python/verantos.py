"""
Interview Question - from nicholas.james@verantos.com

1. Suppose that you are given information about a city’s streets. More specifically, which
streets each given street has access to (it is safe to assume that there are no one way
streets). From past company research a model has been created that can predict how
long it will take to travel between adjacent intersections. This model takes as inputs <the
current time, intersection A, intersection B> and returns the estimated travel time in
seconds. Using this information create a method that can accomplish the following:
a. Find a route from a given starting intersection to a destination intersection.
b. Find the route that will get you there at the earliest estimated time when also
given a travel start time.
You can assume the following constraints:
● The total travel time (in seconds) between any two intersections can fit into a
64-bit integer.
● There are at most 100 distinct streets in this city.
"""

import random


def travelTimes():
    return random.random()


# question: find route to street from existing streets
def findRoute(input, destination):
    r = []

    def dfs(street, path, timeSum):
        nonlocal r
        if street == destination:
            r.append([path.copy(), timeSum])
            return

        edges = input[street]
        if len(edges) == 0:
            return

        for i in edges:
            dfs(i, path + [i], timeSum + travelTimes())

    # for i, v in enumerate(input):
    #     edges = input[v]
    #     print("edges", edges)
    #     print("i", i, "v", v)

    dfs(1, [1], 0)

    shortestTime = 2 ** 64
    shortestPath = []

    for v in r:
        path = v[0]
        time = v[1]
        if time < shortestTime:
            shortestTime = time
            shortestPath = path

    return shortestPath

    # pass
    # pass


print(travelTimes())


print(findRoute({1: [2, 3], 2: [3], 3: [4], 4: [5], 5: [6]}, 5))
# [[1, 2], [2, 3][3, 4], [5, 6]]
