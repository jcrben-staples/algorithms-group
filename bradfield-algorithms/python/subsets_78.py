from typing import List

# 2021-07-31: struggled a bunch, could not do it
# key is to shrink the nums, can either do that w/ slicing nums as you pass it in
# or via a nested for loop
class Solution:
    def subsets(self, nums: List[int]) -> List[List[int]]:
        r = []

        def dfs(sofar, start):
            r.append(sofar.copy())
            # if len(avail) == 0:
            #     r.append(sofar)
            #     return

            for i in range(start, len(nums)):
                # avail[i:]
                sofar.append(nums[i])
                dfs(sofar, i + 1)
                sofar.pop()

        dfs([], 0)

        return r


print(Solution().subsets([1, 2, 3]))
