# Python program to print prime factors
"""
TODO: compare to the sieve of Eratosthenes
from https://www.geeksforgeeks.org/print-all-prime-factors-of-a-given-number/ 

motivated by reading up on Euclid's algorithm (https://www.geeksforgeeks.org/c-program-find-gcd-hcf-two-numbers/)
"""
import math
 
# A function to print all prime factors of 
# a given number n
def primeFactors(n):
     
    # Print the number of two's that divide n
    while n % 2 == 0:
        print(2)
        n = n / 2
        print(n )
         
    # n must be odd at this point
    # so a skip of 2 ( i = i + 2) can be used
    for i in range(3,int(math.sqrt(n))+1,2):
        print('int(math.sqrt(n))+1', int(math.sqrt(n))+1)
         
        # while i divides n , print i ad divide n
        while n % i== 0:
            print(i),
            n = n / i
             
    # Condition if n is a prime
    # number greater than 2
    if n > 2:
        print(n)

primeFactors(2835)