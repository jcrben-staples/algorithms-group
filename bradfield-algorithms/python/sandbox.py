class Solution:
    def twoSum(self, nums, target: int):
        hash = {}

        for i in range(len(nums)):
            if hash.get(nums[i], False) is not False:
                return [hash.get(nums[i]), i]

            hash[nums[i]] = i
            complement = target - nums[i]
            complement_in_hash = hash.get(complement, False)

            if complement_in_hash is not False and i != complement_in_hash:
                return [i, complement_in_hash]


c = Solution().twoSum([3, 3], 6)
print("c", c)

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        dummy = ListNode()
        cur = dummy

        carry = 0
        while l1 or l2 or carry:
            val1 = l1.val if l1 else 0
            val2 = l2.val if l2 else 0

            sum = val1 + val2 + carry
            carry = sum // 10

            first_digit = sum % 10

            node = ListNode(first_digit)
            cur.next = node
            cur = node

        return dummy.next
