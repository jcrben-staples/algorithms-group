from typing import List

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def averageOfLevels(self, root: TreeNode) -> List[float]:
        # if root is None:
        #     return None

        print("root", root)
        r = [root.val]
        queue = []
        queue.extend([root.left, root.right])

        while queue:
            nodes = [queue.pop(0), queue.pop(0)]
            for n in nodes:
                if n:
                    queue.extend([n.left, n.right])

            val1, val2 = 0, 0
            if nodes[0] is not None:
                val1 = nodes[0].val

            if nodes[1] is not None:
                val2 = nodes[1].val
            avg = (val1 + val2) / 2
            r.append(avg)

        return r


tree = TreeNode(3)  # 1
tree.left = TreeNode(9)  # 2
tree.right = TreeNode(20)  # 2
tree.right.left = TreeNode(15)  # 3
tree.right.right = TreeNode(7)  # 3
r = Solution().averageOfLevels(
    tree
    # [3, 9, 20, None, 15, 7],
)
print("r", r)
