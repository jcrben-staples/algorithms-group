# 2021-09-30:
# two questions:
#
# first one - resolve counts of domains
# e.g. counts:
# [
# '900, google.com'
# 30    mail.google.com'
# 10 workplace.mail.google.com
# domains would be com, google.com, mail.google.com, etc
# I screwed up by 
# (1) making dumb mistakes, used period instead of comma on reduce for example
# (2) I first collected all the domains, then added the clicks, when I could've done it all in the first step
#
# second question: findContiguousHistory because user1 and user2
# didn't have enough time, figured 2 pointer approach

```js
/*

We have some clickstream data that we gathered on our client's website. Using cookies, we collected snippets of users' anonymized URL histories while they browsed the site. The histories are in chronological order, and no URL was visited more than once per person.

Write a function that takes two users' browsing histories as input and returns the longest contiguous sequence of URLs that appears in both.

Sample input:

user0 = ["/start", "/green", "/blue", "/pink", "/register", "/orange", "/one/two"]
user1 = ["/start", "/pink", "/register", "/orange", "/red", "a"]
user2 = ["a", "/one", "/two"]
user3 = ["/pink", "/orange", "/yellow", "/plum", "/blue", "/tan", "/red", "/amber", "/HotRodPink", "/CornflowerBlue", "/LightGoldenRodYellow", "/BritishRacingGreen"]
user4 = ["/pink", "/orange", "/amber", "/BritishRacingGreen", "/plum", "/blue", "/tan", "/red", "/lavender", "/HotRodPink", "/CornflowerBlue", "/LightGoldenRodYellow"]
user5 = ["a"]
user6 = ["/pink","/orange","/six","/plum","/seven","/tan","/red", "/amber"]

Sample output:

findContiguousHistory(user0, user1) => ["/pink", "/register", "/orange"]
findContiguousHistory(user0, user2) => [] (empty)
findContiguousHistory(user0, user0) => ["/start", "/green", "/blue", "/pink", "/register", "/orange", "/one/two"]
findContiguousHistory(user2, user1) => ["a"] 
findContiguousHistory(user5, user2) => ["a"]
findContiguousHistory(user3, user4) => ["/plum", "/blue", "/tan", "/red"]
findContiguousHistory(user4, user3) => ["/plum", "/blue", "/tan", "/red"]
findContiguousHistory(user3, user6) => ["/tan", "/red", "/amber"]

n: length of the first user's browsing history
m: length of the second user's browsing history

*/

"use strict";

const user0 = ["/start", "/green", "/blue", "/pink", "/register", "/orange", "/one/two"];
const user1 = ["/start", "/pink", "/register", "/orange", "/red", "a"];
const user2 = ["a", "/one", "/two"];
const user3 = ["/pink", "/orange", "/yellow", "/plum", "/blue", "/tan", "/red", "/amber", "/HotRodPink", "/CornflowerBlue", "/LightGoldenRodYellow", "/BritishRacingGreen"];
const user4 = ["/pink", "/orange", "/amber", "/BritishRacingGreen", "/plum", "/blue", "/tan", "/red", "/lavender", "/HotRodPink", "/CornflowerBlue", "/LightGoldenRodYellow"];
const user5 = ["a"];
const user6 = ["/pink","/orange","/six","/plum","/seven","/tan","/red", "/amber"];

function findContiguousHistory(user0, user1) {
  let u1i1 = 0
  let u2i1 = 0
  let u1i2 = 1
  let u2i2 = 1
  
  let final = 0
  let interimResult = 0
  
  while (u1i2 < user0.length &&  u2i1 < user1.length) {
      while (user0[u1i1] != user1[u2i1]) {
        u1i1 += 1
        u2i1 += 1
      }

      u2i1 = u1i1+1
      u1i2 = u2i2+1

      while (user0[u1i2] === user1[u2i2]) {
        interimResult += 1
        u2i1 = u1i1+1
        u1i2 = u2i2+1

        if (user0[u1i2] !== user1[u2i2) {
          result = Math.max(result, interimResult)
        interimResult = 0
      }
  }
  
  return result;
  
}

function calculateClicksByDomain() {
  const domains = counts.reduce((memo, curr) => {
    const d = curr.split(',')[1]
    const clicks = d[0]
    const x = d.split('.')
    const domains = [];
    domains.push(x[x.length-1])
    for (let i = x.length - 1; i >= 0; i--) {
      domains.push(x.slice(i, x.length))
    }
    for (const domain of domains) {
      memo[domain] = 0;
    }
    return memo
  }, {})
  
//   counts.forEach((i, c) => {
  for (let curr of counts) {
    console.log('curr', curr)
    const d = curr.split(',')
    const domain = d[1]
    const clicks = parseInt(d[0], 10)
    const x = domain.split('.')
    const ldomains = []
//     ldomains.push([x[x.length-1], clicks])
    for (let i = x.length - 1; i >= 0; i--) {
      ldomains.push([x.slice(i, x.length), clicks])
    }
    console.log('ldomains', ldomains)
  
    for (const [domain, _clicks] of ldomains) {
      console.log('domain', domain, '_click', _clicks)
      domains[domain] += _clicks;
      console.log('domains[com]', domains['com'])
    }
//     console.log('clicks', clicks)
//     domains[domain] += clicks
//     return memo
  }
  
  return domains
}

//   const domains = calculateClicksByDomain(counts)
//   console.log('domains', domains)
```
