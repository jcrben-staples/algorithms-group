from typing import List

# 2021-08-07: could not get it, copied from https://leetcode.com/problems/peak-index-in-a-mountain-array/discuss/139840/JavaPython-3-O(n)-and-O(log(n))-codes
class Solution:
    def peakIndexInMountainArray(self, arr: List[int]) -> int:
        l = 0
        r = len(arr) - 1

        # for i in range(len(arr)):
        while l < r:
            mid = l + ((r - l) // 2)

            if arr[mid - 1] > arr[mid]:
                r = mid
            elif arr[mid + 1] > arr[mid]:
                l = mid + 1
            else:
                return mid

        return -1
