See ~/sync/career/algorithms/trees.md

HackerEarth - [Graph Theory Part II](https://www.hackerearth.com/practice/notes/graph-theory-part-ii/)

## types of dfs
Per https://www.geeksforgeeks.org/tree-traversals-inorder-preorder-and-postorder/amp/:
* (a) Inorder (Left, Root, Right) : 4 2 5 1 3
* (b) Preorder (Root, Left, Right) : 1 2 4 5 3
* (c) Postorder (Left, Right, Root) : 4 5 2 3 1