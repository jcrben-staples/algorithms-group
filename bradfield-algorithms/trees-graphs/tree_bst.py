# inspired by https://hackernoon.com/technical-interviewing-for-people-who-suck-at-algorithms-96178d389a83


class TreeBST:
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None

    def addChild(self, newVal):
        if newVal < self.val:
            if self.left:
                self.left.addChild(self.val)
            else:
                self.left = TreeBST(self.val)
        elif newVal > self.val:
            self.right = TreeBST(self.val)
        else:
            raise Exception("duplicates not allowed")


# from https://hackernoon.com/technical-interviewing-for-people-who-suck-at-algorithms-96178d389a83
def find_second_largest(root):
    child = root.right or root.left
    if child.left or child.right:
        find_second_largest(child)
    else:
        return child.val


if __name__ == "__main__":
    bst = TreeBST(5)
    bst.addChild(10)
    bst.addChild(3)

    r = find_second_largest(bst)
    print("r", r)
