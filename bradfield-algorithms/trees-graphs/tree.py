"""
start with a basic tree
"""

# class Node:
#     def __init__(self, val):
#         self.val = val


class Tree:
    def __init__(self, children, val):
        self.children = children or []
        self.val = val

    # this is weird - it hits the root multiple times!
    # if this was a binary tree, it would be:
    # (1) left, root, right
    def traverseDFSInOrder(self):
        print("traversing", self.children)
        # preorder

        for item in self.children:
            item.traverseDFSInOrder()
            print("item.val", item.val)

        print("self.val", self.val)

        # postorder
        # print("self.val", self.val)

    def traverseBFS(self):
        queue = []

        queue.extend(children)

        if len(queue) == 0:
            return

        while len(queue) > 0:
            item = queue.pop(0)
            print("val", item.val)
            queue.extend(item.children)

    def traverseDFSPreOrder(self):
        queue = []

        queue.extend(children)

        if len(queue) == 0:
            return

        while len(queue) > 0:
            item = queue.pop(0)
            print("val", item.val)
            queue.extend(item.children)

            # item.traverseBFS()
            # queue.extend(children)


children1 = [Tree([], 3), Tree([], 4)]
children = [Tree(children1, 1), Tree([], 2)]
t = Tree(children, 5)
t.traverseDFSInOrder()

# x = [1,2,3,4, 5]
# midpoint = len(x) // 2
# print('midpoint', midpoint)

# class Forest:

