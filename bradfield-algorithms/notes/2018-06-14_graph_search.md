# graphs
## class notes
* divide and conquer review
    * implement pow() function
        * we can eliminate the other trees since they are the same
        * 2^8 is a good example
        * 2^8 = 2^4 * 2^4 and so on...
* The Water Jug Riddle (AKA The Die Hard Riddle)
    * graph search problem
    * nodes are the state: [<3-gallon jug>, <5-gallon jug>]
    * state changes are how we can change these jugs
* breadth-first search and depth-first search


## a-star A*
See python/curriculum/graph_search/

* [Introduction to A* (1997)](https://news.ycombinator.com/item?id=17776521)
* [A* algorithm tutorial](http://heyes-jones.com/astar.php)

## resources
[Algorithms Course - Graph Theory Tutorial from a Google Engineer](https://www.youtube.com/watch?v=09_LlHjoEiY)