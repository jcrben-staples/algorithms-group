2018-06-07:
takewaway: practice matrix diagram in balanced parens problem

prev character (going back) | current character
operator                    | number

* complexity analysis:
    * matrix multiplication - n^3
    * join:
        * nested loop join
        * hashjoin
    * btree or hashing in databases
        * low height - 4 or 5 levels deep
    * fizzbuzz
        * calculate line numbers with a mathematical formula
            * division plus division
    * resizable arrays
        * O(n) 
            * has to be contiguous
        * man calloc (C standard lib)
        * man malloc
        * https://github.com/python/cpython/blob/8f4042964d5b0ddf5cdf87862db962ba64e3f64a/Objects/listobject.c#L50-L63
        * UInt32 in JavaSript or Python has Array
    * stacks
        * IDENTIFY REVERSAL PROPERTY
        * balanced parens, browser forward / back, undo stack
        * problem: unix file path
            * file/../bar -> bar
                * push the previous directory into stack
            * 1 + (2 - (1 + 1))
                * CREATE DIAGRAM
    * queue
        * Djisktra's search is a breadth-first search with a priority queue (min heap)
        * A* is effectively what AI used
            * same as Djistraka's is a priority queue with a guess
        * ring buffer (buffer synonym of queue) is faster than linked list
            * OS uses semaphores
            * pipes run in parallel
            * cat foo | wc | grep ... streams
            * stream is a synonym for queue
            * buffer is a synonym for queue
            * the output ("wc" and "grep") have ring buffers which sometimes block
    * arrays in C
        * 4 bytes
        * in Python, pointer to anything uses PyObject