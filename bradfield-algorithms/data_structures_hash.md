TODO: grab photos from whiteboarding

Steps:
1. turn string into integer between 0 and a max (?)
2. Convert said integer into the proper "bucket" index
    1. typically take the mod against a prime https://cs.stackexchange.com/questions/11029/why-is-it-best-to-use-a-prime-number-as-a-mod-in-a-hashing-function

## Hash tables
These are kind of like phone books, where the page number is the hash index. In example, there's a total of 8 indexes so there's a lot of name collision
When the quantity of items stored is 3/4 the storage size, double size; when you hit 1/4 you halve the storage size
Hashing function = huge number modulo size of bucket will be between 1 and the size of the bucket

## real-world
https://stackoverflow.com/questions/8997894/what-hash-algorithm-does-pythons-dictionary-mapping-use#