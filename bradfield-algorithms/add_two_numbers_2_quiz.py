# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def addTwoNumbers(self, l1, l2):
        dummy = ListNode(0)
        curr = dummy
        carry = 0

        while l1 or l2:
            d1 = l1.val
            d2 = l2.val

            d3 = d1 + d2 + carry

            remain = d3 % 10
            carry = d3 // 10
            # curr.val = remain
            node = ListNode(remain)
            curr.next = node
            curr = node

            l1 = l1.next
            l2 = l2.next

        return dummy


# [2,4,3]
# [5,6,4]
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


l1 = ListNode(3)
l1.next = ListNode(4)
l1.next.next = ListNode(2)

l2 = ListNode(4)
l2.next = ListNode(6)
l2.next.next = ListNode(5)
r = Solution().addTwoNumbers(l1, l2)
print("r", r.val)
