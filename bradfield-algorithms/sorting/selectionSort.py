"""
see readme for how it works: "select an item from unsorted and compare to sorted"
"""

def insertionSortMyAttempt(arr):
    for i in range(1, len(arr), 1):
        j = i
        val = arr[i]
        while j > 0 and arr[j-1] > val:
            temp = arr[j-1]
            arr[j-1] = val
            arr[i] = temp
            j -= 1
    return arr

# from https://web.archive.org/web/20180519111014/https://www.cs.cmu.edu/~adamchik/15-121/lectures/Sorting%20Algorithms/sorting.html
def insertionSortAnswer(arr):
    for i in range(1, len(arr), 1):
        j = i
        val = arr[i]
        while j > 0 and arr[j-1] > val:
            # temp = arr[j-1]
            # arr[j-1] = val
            # arr[i] = temp
            j -= 1
        arr[i] = arr[j-1]
    return arr

# returns [2, 4, 5, 6, 10]
print(insertionSort([4,2,5,6,10]))