## list

Hello World - no
Two Fer - no
isLeapYear - yes
reverseString - prior knowledge 
    * good practice, in place
rnaTranscription - simple
Shift Cipher - seems too easy
Pangram - done
Bob - not very algorithmic
Gigasecond - mostly a standard library thing due to monthly calendar messiness
    * newDate(new Date(<some date>).getTime() + 10 ** 12)
        * works before 1970 due to negatives (learned something)

Space Age in JavaScript - no
Binary in JavaScript - done