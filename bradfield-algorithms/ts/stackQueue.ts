// @ts-ignore
const assert = require("assert");

interface IQueue {
    constructor: Function;
    dequeue(param: number): number;
    enqueue(param: number): IQueue;
}

class Queue implements IQueue {
    list: (number)[];
    constructor(param?: number) {
        this.list = [];
        return this;
    }

    enqueue(param: number) {
        this.list.unshift(param);
        return this;
    }

    dequeue() {
        return this.list.pop();
    }

    size() {
        return this.list.length;
    }

    peek() {
        return this.list.slice(-1).pop();
    }

    empty() {
        return this.list.length === 0;
    }
}

class Stack {
    currentQueue: Queue;
    altQueue: Queue;
    constructor() {
        this.currentQueue = new Queue();
        this.altQueue = new Queue();
    }

    push(val: number) {
        this.altQueue.enqueue(val);
        while ((val = this.currentQueue.dequeue())) {
            this.altQueue.enqueue(val);
        }

        this.currentQueue = this.altQueue;
        this.altQueue = new Queue();
        return this;
    }

    pop() {
        return this.currentQueue.dequeue();
    }

    top() {
        return this.currentQueue.peek();
    }

    empty() {
        return this.currentQueue.empty();
    }

    size() {
        return this.currentQueue.size();
    }
}

class QueueFromStack {
    currentStack: Stack;
    altStack: Stack;
    constructor() {
        this.currentStack = new Stack();
        this.altStack = new Stack();
    }

    enqueue(newVal) {
        let val;

        if (this.currentStack.empty()) {
            this.currentStack.push(newVal);
            return this;
        }

        while ((val = this.currentStack.pop())) {
            this.altStack.push(val);
            if (this.currentStack.empty()) {
                this.currentStack.push(newVal);
                break;
            }
        }

        while ((val = this.altStack.pop())) {
            this.currentStack.push(val);
        }

        return this;
    }

    dequeue() {
        return this.currentStack.pop();
    }
}

const stack = new Stack();
stack.push(1);
stack.push(2);
stack.push(3);
assert.equal(stack.pop(), 3);
assert.equal(stack.pop(), 2);

const queue = new QueueFromStack();
queue.enqueue(1);
queue.enqueue(2);
queue.enqueue(3);
assert.equal(queue.dequeue(), 1);
assert.equal(queue.dequeue(), 2);
