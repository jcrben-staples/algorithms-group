const assert = require("assert");

/*
given certain conditions, a year is a leap year

smaller problem: we need to know how to find out if a number is divisible; requires prior knowledge of modulo operator
plan: we know there are ultimately 2 circumstances for a leap year,
so return true for them and false otherwise
time and space complexity: O(1)
*/
export const isLeapYear = (year: number): boolean => {
    if (year % 4 === 0 && !(year % 100 === 0)) {
        return true;
    }

    if (year % 4 === 0 && (year % 100 === 0 && year % 400 === 0)) {
        return true;
    }

    return false;
};

assert.equal(isLeapYear(2015), false);
assert.equal(isLeapYear(2016), true);
assert.equal(isLeapYear(1900), false);
assert.equal(isLeapYear(2000), true);
assert.equal(isLeapYear(1978), false);
assert.equal(isLeapYear(1992), true);

export const reverseString = (str: string): string => {
    let result = "";
    for (let i = str.length - 1; i >= 0; i--) {
        result += str[i];
    }

    return result;
};

assert.equal(reverseString("ben"), "neb");

export const reverseStringInPlace = (str: string): string => {
    const arr = str.split("");
    for (let i = 0; i < arr.length - 1 / 2; i++) {
        arr[i] = str[str.length - i - 1];
    }

    return arr.join("");
};

assert.equal(reverseStringInPlace("ben"), "neb");
assert.equal(reverseStringInPlace("hollow"), "wolloh");
assert.equal(reverseStringInPlace("holxo"), "oxloh");

/*
known: RNA complementary characters
seeking conversion of original chars to complements
first thought: create a key-value mapping, iterate thru and replace original with mapped value

O(n) time and O(1) space
*/
export const rnaTranscription = (dnaSeq: string): string => {
    if (!dnaSeq.length) return;

    const dnaToRnaMap = {
        G: "C",
        C: "G",
        T: "A",
        A: "U"
    };

    let result = "";

    for (let [index, nucleotide] of Array.from(dnaSeq).entries()) {
        result += dnaToRnaMap[dnaSeq[index]];
        // nucleotide + rnaTranscription(nucleotide.slice(index))
    }
    return result;
};

assert.equal(rnaTranscription("GCTA"), "CGAU");

/*
initially skimmed and thought this was decimal to binary
    * noticed that this is covered in the algos book
thinking about the reverse (decimal to binary) and it seems more complicated
NOTE: struggled with off-by-one errors and initially did not have additional counter
O(n) time complexity
*/
export const binaryToDecimal = (binaryStr: string) => {
    // const base = 2 ** binaryStr.length;

    let total = 0;
    console.log("entering");
    let counter = 0;

    // TODO: fix per https://stackoverflow.com/questions/8348792/multiple-counters-in-javascript-for-loop
    for (let i = binaryStr.length - 1; i >= 0; i--) {
        total += parseInt(binaryStr[counter], 10) * 2 ** i;
        counter += 1;
    }

    return total;
};

assert.equal(binaryToDecimal("0"), 0);
assert.equal(binaryToDecimal("1"), 1);
assert.equal(binaryToDecimal("10"), 2);
assert.equal(binaryToDecimal("11"), 3);
