/* 
knowns:
1. list of (ASCII) characters forming English alphabet (a-Z)
unknowns:
1. list of ASCII characters forming a "phrase"

condition: after checking every character in the phrase and marking
           it as included in, no characters in alphabet are unmarked
*/

const assert = require("assert");

const alphabetKvFromCode = (startCode: number): Array<[string, boolean]> =>
    [...Array(26).keys()].map(
        (i): [string, boolean] => [String.fromCharCode(i + startCode), false]
    );

/* version1:
analogy: similar problems such as deduplication of chars
have been solved by recording pregress with hashmap

plan: 
build hashmap of alphabet, then iterate over phrase and mark chars in alphabet
when phrase iteration is complete, iterate over alphabet -> false if any unmarked, else true
O(n) time and O(n) space
*/
export const isPangram = (phrase: string = ""): boolean => {
    const lowercase = alphabetKvFromCode("a".charCodeAt(0));

    // add uppercase to make case sensitive
    const charMap = new Map([...lowercase]);

    for (let char of phrase) {
        const getVal = charMap.get(char);
        if (charMap.get(char) === false) {
            charMap.set(char, true);
        }
    }

    return Array.from(charMap.values()).every(val => val === true);
};

/* version2
"early return" is a common pattern to handle invalid or corner cases
plan: if phrase contains fewer characters than alphabet, return early
time complexity unchanged
*/
export const isPangram2 = (phrase: string = ""): boolean => {
    if (phrase.length < 27) return false;

    // COPY OF version1 from here
    const lowercase = alphabetKvFromCode("a".charCodeAt(0));

    // add uppercase to make case sensitive
    const charMap = new Map([...lowercase]);

    for (let char of phrase) {
        const getVal = charMap.get(char);
        if (charMap.get(char) === false) {
            charMap.set(char, true);
        }
    }

    // return Array.from(charMap.values()).every(val => val === true);
};

/* version3
the phrase could be very long, and we don't need to iterate thru it entirely
or iterate thru the alphabet if we keep track of our progress as we go
plan: we know how many letters in alphabet, and we know when we're about to mark
another letter as touched - so return when we hit the number of letters in alphabet

time complexity remains O(n) 
*/
export const isPangram3 = (phrase: string = ""): boolean => {
    let alphabetLength = 27;
    if (phrase.length < alphabetLength) return false;

    const lowercase = alphabetKvFromCode("a".charCodeAt(0));

    // add uppercase to make case sensitive
    const charMap = new Map([...lowercase]);

    // region version3
    let alphabetCharsMarked = 0;
    for (let char of phrase) {
        const getVal = charMap.get(char);
        if (charMap.get(char) === false) {
            charMap.set(char, true);
            alphabetCharsMarked += 1;
            if (alphabetCharsMarked === alphabetLength - 1) return true;
        }
    }
    // endregion

    return false;
};

assert.equal(isPangram("the quick brown fox jumps over the lazy dog"), true);
assert.equal(isPangram2("the quick brown"), false);
assert.equal(isPangram3("the quick brown fox jumps over the lazy dog"), true);
