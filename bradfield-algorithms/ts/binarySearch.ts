import * as assert from "assert";

const binarySearch = (sortedNums: number[], target): boolean => {
    if (sortedNums.length <= 1) {
        return target === sortedNums[0];
    }

    const midpoint = Math.floor(sortedNums.length / 2);

    const initialSearch =
        target >= sortedNums[midpoint]
            ? sortedNums.slice(midpoint)
            : sortedNums.slice(0, midpoint);

    return binarySearch(initialSearch, target);
};

assert.equal(binarySearch([1, 2, 3, 5], 4), false);
assert.equal(binarySearch([1, 2, 3, 5, 7], 7), true);
assert.equal(binarySearch([1, 2, 3, 5, 7], 1), true);
assert.equal(binarySearch([1, 2, 3, 5, 7], 2), true);

const TwoDMatrixSearch = (sortedMatrix: Array<number[]>, target) => {
    for (let arr of sortedMatrix) {
        if (target >= arr[0] && target <= arr[arr.length - 1]) {
            return binarySearch(arr, target);
        }
    }

    return false;
};

assert.equal(
    TwoDMatrixSearch([[1, 3, 5, 7], [10, 11, 16, 20], [23, 30, 34, 50]], 3),
    true
);

assert.equal(
    TwoDMatrixSearch([[1, 3, 5, 7], [10, 11, 16, 20], [23, 30, 34, 50]], 13),
    false
);
