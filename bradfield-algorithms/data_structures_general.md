[Data Structures Easy to Advanced Course - Full Tutorial from a Google Engineer](https://www.youtube.com/watch?v=RBSGKlAvoiM)

## tips from Bradfield
https://my.bradfieldcs.com/algorithms/2018-06/

https://github.com/whatrocks/bradfield-algos

### mutually exclusive, collectively exhaustive
* See this in the perfect squares but also https://www.geeksforgeeks.org/count-ways-reach-nth-stair/
* stack will shrink and grow

### queue
See ./python/curriculum/queue

### Divide and Conquer
See ./notes/2018-06-07_divide_and_conquer.md

* https://www.youtube.com/watch?v=gSC8O-5qMwg&feature=youtu.be
* https://www.youtube.com/watch?v=2e9RsNQuTzI

## Time complexity analysis
https://g1thubhub.github.io/data-structure-zoo.html

As you increase the size of your input, by how much is time changed (linear, exponential, order of magnitude, etc)
* Constant time - 0(1), note that this is related to arrays located in contiguous block of memory ((base+index)*bucket size)
* Quadratic - 
* Linear - O(n) (drop the coefficient)
* Quadratic - O(n^2) but could also get into n^3 and n^4
* Exponential - O(c^N) (any sort of permutation)
* https://en.wikipedia.org/wiki/Pseudo-polynomial_time
    * https://stackoverflow.com/questions/19647658/what-is-pseudopolynomial-time-how-does-it-differ-from-polynomial-time

https://bradfieldcs.com/algos/analysis/big-o-notation/

https://stackoverflow.com/questions/2307283/what-does-olog-n-mean-exactly

