class Solution:
    def toLowerCase(self, str):
        """
        :type str: str
        :rtype: str
        """
        for c in str:
            return "".join(chr(ord(c) + 32) if 65 <= ord(c) <= 90 else c for c in str)


s = Solution()
r = s.toLowerCase("AAKSDLJFLSKDFJ")

print("r", r)

