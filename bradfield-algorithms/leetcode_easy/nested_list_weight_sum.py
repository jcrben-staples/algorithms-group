"""
https://leetcode.com/problems/nested-list-weight-sum/description/
Given a nested list of integers, return the sum of all integers in the list weighted by their depth.

Each element is either an integer, or a list -- whose elements may also be integers or other lists.

Example 1:
Given the list [[1,1],2,[1,1]], return 10. (four 1's at depth 2, one 2 at depth 1)

Example 2:
Given the list [1,[4,[6]]], return 27. (one 1 at depth 1, one 4 at depth 2, and one 6 at depth 3; 1 + 4*2 + 6*3 = 27)
"""


class Solution:
    def depthSum(self, nestedList):
        """
        :type nestedList: List[NestedInteger]
        :rtype: int
        """
        # sum = 1
        # stack = []
        initial_depth = 0

        def recurse_in(list_or_int, depth):
            # nonlocal sum
            # region adjusted for leetcode
            # still errored for some reason
            sum = 0
            # if list_or_int.isInteger():
            #     sum += depth * list_or_int.getInteger()
            # else:
            #     for item in list_or_int.getList():
            #         sum += recurse_in(item, depth + 1)
            # endregion
            # region plain python
            if isinstance(list_or_int, list):
                for item in list_or_int:
                    sum += recurse_in(item, depth + 1)
            else:
                print("depth", depth, "list_or_int", list_or_int)
                sum += depth * list_or_int
            # endregion

            return sum

        result = recurse_in(nestedList, initial_depth)
        return result

        # for item in nestedList:
        #     recurseIn(item, initial_depth + 1)
        # if item isinstance([0, 10, 20, 30], list):


solution = Solution()
# print("result", solution.depthSum([[1, 1], 2, [1, 1]]))
print("result", solution.depthSum([1, [4, [6]]]))

# """
# This is the interface that allows for creating nested lists.
# You should not implement it, or speculate about its implementation
# """
# class NestedInteger:
#    def __init__(self, value=None):
#        """
#        If value is not specified, initializes an empty list.
#        Otherwise initializes a single integer equal to value.
#        """
#
#    def isInteger(self):
#        """
#        @return True if this NestedInteger holds a single integer, rather than a nested list.
#        :rtype bool
#        """
#
#    def add(self, elem):
#        """
#        Set this NestedInteger to hold a nested list and adds a nested integer elem to it.
#        :rtype void
#        """
#
#    def setInteger(self, value):
#        """
#        Set this NestedInteger to hold a single integer equal to value.
#        :rtype void
#        """
#
#    def getInteger(self):
#        """
#        @return the single integer that this NestedInteger holds, if it holds a single integer
#        Return None if this NestedInteger holds a nested list
#        :rtype int
#        """
#
#    def getList(self):
#        """
#        @return the nested list that this NestedInteger holds, if it holds a nested list
#        Return None if this NestedInteger holds a single integer
#        :rtype List[NestedInteger]
#        """
