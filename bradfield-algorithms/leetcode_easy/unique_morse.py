
morse_list = [
    ".-",
    "-...",
    "-.-.",
    "-..",
    ".",
    "..-.",
    "--.",
    "....",
    "..",
    ".---",
    "-.-",
    ".-..",
    "--",
    "-.",
    "---",
    ".--.",
    "--.-",
    ".-.",
    "...",
    "-",
    "..-",
    "...-",
    ".--",
    "-..-",
    "-.--",
    "--..",
]

ASCII_START = 97

chars_to_morse = {chr(ASCII_START + i): morse for i, morse in enumerate(morse_list)}
print("words", chars_to_morse)


def unique_morse(input_words):
    morse_map = {}
    for word in input_words:
        tokens = [*word]
        morse_item = "".join([chars_to_morse[token] for token in tokens])
        morse_map[morse_item] = True

    return len(morse_map.keys())


unique_morse(["hi", "hello", "yo", "yo"])

