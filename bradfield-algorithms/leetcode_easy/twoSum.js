/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
//  var twoSum = function(nums, target) {
//     const hash = {}
    
//     for (let [index, num] of nums.entries()) {
//         hash[num] = index;
//         // let complement = target - num;
//         // if (hash[complement]) {
//         //     return [hash[complement], index];
//         // }
//     }
    
//     for (let [index, num] of nums.entries()) {
//         // if (!hash[num]) {
//         //     hash[num] = index;
//         // }
//         let complement = target - num;
//         if (hash[complement]) {
//             console.log('complement', complement)
//             return [hash[complement], index];
//         }
//     }
    
//     return false;
    
// };

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
 var twoSum = function(nums, target) {
    const hash = {}
    
    for (let [index, num] of nums.entries()) {
        let complement = target - num;
        if (complement in hash) {
            return [hash[complement], index];
        }
        hash[num] = index;
    }
    
    // for (let [index, num] of nums.entries()) {
    //     // if (!hash[num]) {
    //     //     hash[num] = index;
    //     // }
    //     let complement = target - num;
    //     if (hash[complement]) {
    //         //console.log('complement', complement, index)
    //         return [hash[complement], index];
    //     }
    // }
    
    // return false;
    
};

console.log(twoSum([2,7,11,15], 9))
