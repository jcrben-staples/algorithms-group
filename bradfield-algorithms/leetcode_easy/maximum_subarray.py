"""
https://leetcode.com/problems/maximum-subarray/description/
Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

Example:

Input: [-2,1,-3,4,-1,2,1,-5,4],
Output: 6
Explanation: [4,-1,2,1] has the largest sum = 6.
Follow up:

If you have figured out the O(n) solution, try coding another solution using the divide and conquer approach, which is more subtle
"""


def innerSubArray(nums, l=None, r=None, r_or_left=None):
    if len(nums) == 1:
        # TODO: return if left or right?
        return nums[0]

    midpoint = len(nums) // 2
    # TODO: if I pass in i, I can check to see if it's on the right side
    suml = innerSubArray(nums[:midpoint], 0, midpoint, "left")
    sumr = innerSubArray(nums[midpoint:], midpoint, len(nums), "right")

    result_left = max(suml, suml + sumr)
    result_right = max(sumr, suml + sumr)
    return max(result_left, result_right)


class Solution:
    # DP method copied from https://leetcode.com/problems/maximum-subarray/discuss/20396/Easy-Python-Way
    # for i in range(1, len(nums)):
    #     if nums[i-1] > 0:
    #         nums[i] += nums[i-1]
    # return max(nums)

    def maxSubArray(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """

        return innerSubArray(nums)


def assertEqual(calc, expected):
    if calc != expected:
        print("expected calc:", calc, "to equal:", expected)
        # raise Exception()


solution = Solution()
# print("solution:", solution.maxSubArray([0, 1, -1, 1, 2, 5, -2]))

# assertEqual(solution.maxSubArray([0, 1, -1, 1, 2, 5, -2]), 8)
assertEqual(solution.maxSubArray([0, 1, -1, 1, 2, -1]), 3)
