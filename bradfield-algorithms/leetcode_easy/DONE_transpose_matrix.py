"""
## retro
* went with a too easy solution
* learned more about zip mostly from https://www.journaldev.com/15891/python-zip-function
* learned about unpacking operator from https://stackoverflow.com/questions/5938786/how-would-you-zip-an-unknown-number-of-lists-in-python

## desc
https://leetcode.com/problems/transpose-matrix/description/
867. Transpose Matrix
Given a matrix A, return the transpose of A.

The transpose of a matrix is the matrix flipped over it's main diagonal, switching the row and column indices of the matrix.

Example 1:
Input: [[1,2,3],[4,5,6],[7,8,9]]
Output: [[1,4,7],[2,5,8],[3,6,9]]

Example 2:
Input: [[1,2,3],[4,5,6]]
Output: [[1,4],[2,5],[3,6]]
"""


class Solution:
    def transpose(self, A):
        """
        :type A: List[List[int]]
        :rtype: List[List[int]]
        """

        lists = []

        for x in A:
            lists.append(x)

        result = list(zip(*lists))
        return result


input = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
s = Solution()
r = s.transpose(input)
print("r", r)
for x in r:
    print("x", x)
