# smallest at top to start


class BinaryTree:
    def __init__(self, val=None):
        self.val = val
        # self.__val = val
        self.left = None
        self.right = None

    @property
    def val(self):
        # print("accessing val")
        return self.__val

    @val.setter
    def val(self, val):
        # print("setting val")
        self.__val = val

    # region these don't do anything - need more setup
    # def __get__(self, obj, objtype):
    #     print("objtype", objtype)
    #     return self.val

    # def __set__(self, obj, val):
    #     self.val = val
    # endregion

    def traverseDirection(self, dir="left"):
        print("traverse self.value", self.val)
        if dir == "left" and self.left != None:
            self.left.traverseDirection()

        if dir == "right" and self.right != None:
            self.right.traverseDirection()

    def addNode(self, val=None):
        if self.val == val:
            print("duplicate:", val)
            return

        if self.val == None:
            self.val = val
            return

        if val < self.val:
            if self.left == None:
                self.left = BinaryTree(val)
            else:
                self.left.addNode(val)

        if val > self.val:
            if self.right == None:
                self.right = BinaryTree(val)
            else:
                self.right.addNode(val)

        # if val > self.left.val:
        #     self.right.addNode(self.val)
        # elif val > self.right.val:
        #     self.left.addNode(self.val)


tree = BinaryTree(10)
tree.addNode(3)
tree.addNode(2)
# tree.addNode(4)
# tree.addNode(3)
# print("tree.val", tree.val)
tree.traverseDirection("left")
