def anagram_mappings(a, b):
    result = []

    b_map = {x: i for i, x in enumerate(b)}

    print(b_map)

    for i in range(len(a)):
        result.append(b_map[a[i]])

    return result


print(anagram_mappings([12, 28, 46, 32, 50], [50, 12, 32, 46, 28]))
