"""
https://leetcode.com/problems/judge-route-circle/description/

Initially, there is a Robot at position (0, 0). Given a sequence of its moves, judge if this robot makes a circle, which means it moves back to the original place.

The move sequence is represented by a string. And each move is represent by a character. The valid robot moves are R (Right), L (Left), U (Up) and D (down). The output should be true or false representing whether the robot makes a circle.

Example 1:
Input: "UD"
Output: true
Example 2:
Input: "LL"
Output: false
"""

def judgeCircle(inputStr):
    print('running')
    grid = [0,0]
    mapMoves = {
        "L": [0, -1],
        "R": [0, 1],
        "U": [-1, 0],
        "D": [1, 0]
    }

    xs = list(inputStr)

    for d in xs:
        # print('mapMoves', mapMoves)
        moves = mapMoves[d]
        # print('moves', moves)
        grid = [grid[0]+moves[0], grid[1]+moves[1]]

    return grid == [0,0]

# judge_route_circle("LL")
def assertEqual(calc, expected):
    if calc != expected:
        print("expected calc:", calc, "to equal:", expected)
    else:
        print('assertion passed')

assertEqual(judgeCircle("LL"), False)
assertEqual(judgeCircle("UD"), True)