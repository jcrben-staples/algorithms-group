// timed out
// Are Similar? | CodeFights Intro Algorithm JavaScript Solution and Breakdown https://www.youtube.com/watch?v=MDVYZpUKFWI
// is clever
function areSimilar(a, b) {
    let hash = {};
    hash[a] = JSON.stringify(a);
    hash[b] = JSON.stringify(b);
    if (hash[a] == hash[b]) return true;

    // functionCheckFor

    for (let i = 0; i < a.length; i++) {
        for (let j = 1; j < a.length; j++) {
            let newarr = a.slice();
            let tmp = a[i];
            // let tmp2 = a[j];
            newarr[i] = a[j];
            newarr[j] = tmp;
            if (JSON.stringify(newarr) == hash[b]) {
                // good
                return true;
            }
        }

        // if (a[i] == b[i]) {
        //     continue;
        // }

        // if (a[i] != b[i]) {
        // }
    }

    for (let i = 0; i < a.length; i++) {
        for (let j = 1; j < a.length; j++) {
            let newarr = b.slice();
            let tmp = b[i];
            // let tmp2 = a[j];
            newarr[i] = b[j];
            newarr[j] = tmp;
            if (JSON.stringify(newarr) == hash[a]) {
                // good
                return true;
            }
        }

        // if (a[i] == b[i]) {
        //     continue;
        // }

        // if (a[i] != b[i]) {
        // }
    }

    return false;
}

console.log("r", areSimilar([1, 2, 2], [2, 1, 1]));
