function digitDegree(n) {
    let a = Array.from(String(n), (x) => Number(x));
    let count = 0;
    while (a.length > 1) {
        let newSum = 0;
        for (let i = 0; i < a.length; i++) {
            newSum += a[i];
        }
        a = Array.from(String(newSum), (x) => Number(x));
        count++;
    }

    return count;
}

console.log(digitDegree(100));
