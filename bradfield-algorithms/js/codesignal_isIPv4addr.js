// https://www.regular-expressions.info/numericranges.html
function isIPv4Address(inputString) {
    // const re = /\d*\\.\d*\\.\d*\\.\d/
    const digits = "([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])";
    // const digits = '(1?[0-9]{1,2}|2[0-4][0-9]|25[0-5])'
    const re = new RegExp(`^${digits}\\.${digits}\\.${digits}\\.${digits}$`);
    console.log("re", re);
    const r = re.test(inputString);
    return r;
}
