const assert = require('assert');

/**
 * Should replace pieces of content with another value.
 * 
 * @param  {String} content     The source string to convert
 * @param  {String} matchString The string to match
 * @param  {String} translation The string to translate into
 * @return {String}             The output string
 */
function translate1(content, matchString, translation) {
  if (content === matchString) {
    return translation;
  }
}

const interpolate = (template, val) => {
    return template.replace(new RegExp(`\\{\\{0\\}\\}`, 'g'), val);
}

function translate2(content, matchString, translation) {
    const split = matchString.split('{{0}}');
    const preserve = content.split(new RegExp(`(?:${split[0]})(.*)(?:${split[1]})`)).join(' ').trim();

    return interpolate(translation, preserve);
}
  
(()=> {
    const content = 'hello world mister.';
    const matchString = 'hello world mister.';
    const translation = 'hola señor';
    const expected = 'hola señor';
    const result = translate1(content, matchString, translation);
    assert.equal(result, expected)
})();


(() => {
    const content = 'hello world TEXT TO PRESERVE mister.';
    const matchString = 'hello world {{0}} mister.';
    const translation = 'hola {{0}} señor';
    const expected = 'hola TEXT TO PRESERVE señor';
    const result = translate2(content, matchString, translation);
    assert.equal(result, expected);
})();