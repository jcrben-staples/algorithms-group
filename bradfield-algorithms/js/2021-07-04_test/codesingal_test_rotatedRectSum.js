// https://medium.com/hard-mode/coding-challenge-rectangle-rotation-10e2a2416ef3
// https://www.reddit.com/r/algorithms/comments/lwkwwj/need_help_with_finding_the_max_possible_sum/

function rotatedRectSum(matrix, a, b) {
    const rows = matrix.length;
    const cols = matrix[0].length;
    let sum = Number.NEGATIVE_INFINITY;
    // easy case
    if (a === 1 && b === 1) {
        for (let i = 0; i < rows; i++) {
            for (let j = 0; j < cols; j++) {
                sum = Math.max(sum, matrix[i][j]);
            }
        }
    }

    if ((a === 1 && b === 2) || (a === 2 && b === 1)) {
        // for (let i = 0; i < rows; i++) {
        // for (let j = 0; j < cols; j++) {
        let sumSoFar = 0;
        let k = 0;
        let diagDownRight = [1, 1];
        let diagDownLeft = [-1, -1];
        while (k < rows) {
            sumSoFar += matrix[k][k];
            k += 1;
        }
        sum = Math.max(sum, sumSoFar);

        sumSoFar = 0;
        k = cols - 1;
        j = 0;
        while (k >= 0) {
            console.log("k", k);
            sumSoFar += matrix?.[j]?.[k];
            k -= 1;
            j += 1;
        }
        console.log("sumSoFar", sumSoFar);
        sum = Math.max(sum, sumSoFar);
        // }
        // }
    }

    // if ((a === 3 && b === 2) ||
    // (a === 2 && b ===3)) {
    //     pass
    // }

    return sum;
}

/*
Given a matrix of integers, we'd like to consider the sum of the elements within the area of a 45° rotated rectangle. More formally, the area is bounded by two diagonals parallel to the main diagonal and two diagonals parallel to the secondary diagonal. The dimensions of the rotated rectangle are defined by the number of elements along the borders of the rectangle.

dimensions

Given integers a and b representing the dimensions of the rotated rectangle, and matrix (a matrix of integers), your task is to find the greatest sum of integers contained within an a x b rotated rectangle.

Note: The order of the dimensions is not important - consider all a x b and b x a rectangles.

Example

For
2021-07-04-13-59-10.png
matrix = [[1, 2, 3, 4, 0],
          [5, 6, 7, 8, 1],
          [3, 2, 4, 1, 4],
          [4, 3, 5, 1, 6]]
a = 2, and b = 3, the output should be rotatedRectSum(matrix, a, b) = 36.

example 1

For

matrix = [[-2, 3, 5, -1],
          [4, 3, -10, 10]]
a = 1, and b = 1, the output should be rotatedRectSum(matrix, a, b) = 10.

example 2

The rotated rectangle with dimensions 1x1 is just one element, so the answer is the maximal element in matrix.

For

matrix = [[-2, 3],
          [4, 3]]
a = 1, and b = 2, the output should be rotatedRectSum(matrix, a, b) = 7.

example 3

Input/Output
2021-07-04-14-16-58.png
2021-07-04-14-17-24.png
[execution time limit] 4 seconds (js)

[input] array.array.integer matrix

A matrix of integers.

Guaranteed constraints:
1 ≤ matrix.length, matrix[i].length ≤ 50,
-103 ≤ matrix[i][j] ≤ 103.

[input] integer a

The first rotated rectangle dimension.

Guaranteed constraints:
1 ≤ a ≤ 25.

[input] integer b

The second rotated rectangle dimension.
It's guaranteed that at least one rotated rectangle will fit in the given matrix.

Guaranteed constraints:
a ≤ b ≤ 25.

[output] integer

The maximal sum of elements of a rotated rectangle with dimensions a and b.

Test 3 PASS
Input:
matrix:
[[-2,3], 
 [4,3]]
a: 1
b: 2
Output:
1
Expected Output:
7

Test 1
Input:
matrix:
[[1,2,3,4,0], 
 [5,6,7,8,1], 
 [3,2,4,1,4], 
 [4,3,5,1,6]]
a: 2
b: 3
Output:
null
Expected Output:
36

Test 4
Input:
matrix:
[[-1000,1000,-1000], 
 [1000,-1000,1000], 
 [-1000,1000,-1000]]
a: 1
b: 3
Output:
null
Expected Output:
-3000

Test 5
Input:
matrix:
[[-6,-2,10,-7,0], 
 [8,-5,-10,3,3], 
 [-8,-7,-6,0,5], 
 [8,4,3,-4,-4], 
 [0,10,0,0,3]]
a: 3
b: 3
Output:
null
Expected Output:
-15

Test 6
Input:
matrix:
[[11,80,66,62], 
 [51,10,26,73], 
 [31,4,25,12], 
 [10,24,32,88], 
 [97,36,39,88], 
 [32,20,79,15], 
 [2,34,90,29], 
 [28,73,30,7], 
 [1,82,79,38], 
 [35,17,88,82]]
a: 1
b: 4
Output:
null
Expected Output:
236
*/
