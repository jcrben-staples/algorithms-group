const assert = require("assert");
function isPalindrome(s) {
    return s.join("") === s.reverse().join("");
}

// optimal
function palindromeCutting(s) {
    let strStart = 0;
    let palindromesByIndex = {};
    for (let i = 0; i < s.length; i++) {
        let l = i;
        let r = i;
        palindromesByIndex[i] = palindromesByIndex?.[i]
            ? palindromesByIndex[i]
            : [];
        while (l >= 0 && r < s.length && s[l] === s[r]) {
            palindromesByIndex[l].push(s.slice(l, r + 1));
            l -= 1;
            r += 1;
        }
        l = i;
        r = i + 1;
        while (l >= 0 && r < s.length && s[l] === s[r]) {
            palindromesByIndex[l].push(s.slice(l, r + 1));
            l -= 1;
            r += 1;
        }
    }

    let longestStr = "";
    if (palindromesByIndex[strStart]) {
        longestStr = palindromesByIndex[strStart].reduce((curr, next) => {
            if (next.length > curr.length) return next;
        }, "");
    }
    let longestLen = longestStr.length;
    while (longestLen > 1) {
        longestLen = longestStr.length;
        if (longestLen > 1) {
            strStart += longestLen;
        }
        if (palindromesByIndex[strStart]) {
            longestStr = palindromesByIndex[strStart].reduce((curr, next) => {
                if (next.length > curr.length) return next;
            }, "");
        } else {
            break;
        }
    }

    return s.slice(strStart);

    // console.log("palindromesByIndex", palindromesByIndex);
}

// console.log(palindromeCutting("aaacodedoc"));

// brute force
// https://leetcode.com/discuss/interview-question/1049739/uber-quora-oa

// time complexity = O(n^4*n) ??
/*
function palindromeCutting(s) {
    let allPrefixes = [];
    let allPalindromes = [];
    let sA = s.split("");
    let longestLen = Number.NEGATIVE_INFINITY;
    let longest;
    let newStr;

    for (let i = 0; i <= s.length; i++) {
        allPrefixes.push(sA.slice(0, i));
    }

    for (let pre of allPrefixes) {
        if (isPalindrome(pre)) {
            allPalindromes.push(pre);
            if (pre.length > longestLen) {
                longest = pre;
                longestLen = pre.length;
            }
        }
    }

    if (longestLen > 1) {
        newStr = palindromeCutting(sA.slice(longestLen).join(""));
    } else {
        newStr = s;
    }

    return newStr;
}
*/

// test 1
assert.strictEqual(palindromeCutting("aaacodedoc"), "");
// test 2
assert.strictEqual(palindromeCutting("codesignal"), "codesignal");
// test 3
assert.strictEqual(palindromeCutting(""), "");
// test 4
assert.strictEqual(palindromeCutting("a"), "a");

/*
You are given a string s. Consider the following algorithm applied to this string:

Take all the prefixes of the string, and choose the longest palindrome between them.
If this chosen prefix contains at least two characters, cut this prefix from s and go back to the first step with the updated string. Otherwise, end the algorithm with the current string s as a result.
Your task is to implement the above algorithm and return its result when applied to string s.

Note: you can click on the prefixes and palindrome words to see the definition of the terms if you're not familiar with them.

Example

For s = "aaacodedoc", the output should be palindromeCutting(s) = "".

The initial string s = "aaacodedoc" contains only three prefixes which are also palindromes - "a", "aa", "aaa". The longest one between them is "aaa", so we cut if from s.
Now we have string "codedoc". It contains two prefixes which are also palindromes - "c" and "codedoc". The longest one between them is "codedoc", so we cut if from the current string and obtain the empty string.
Finally the algorithm ends on the empty string, so the answer is "".
For s = "codesignal", the output should be palindromeCutting(s) = "codesignal".
The initial string s = "codesignal" contains the only prefix, which is also palindrome - "c". This prefix is the longest, but doesn't contain two characters, so the algorithm ends with string "codesignal" as a result.

For s = "", the output should be palindromeCutting(s) = "".

Input/Output

[execution time limit] 4 seconds (js)

[input] string s

A string consisting of English lowercase letters.

Guaranteed constraints:
0 ≤ s.length ≤ 1000.

[output] string

The result of the described algorithm.

Input:
s: "abbab"
Output:
null
Expected Output:
"b"

Input:
s: "abababaaab"
Output:
null
Expected Output:
"b"

Test 5
Input:
s: "abbab"
Output:
"abbab"
Expected Output:
"b"

Test 6
Input:
s: "aaabba"
Output:
"aaabba"
Expected Output:
"a"

Test 7
Input:
s: "aaaaaaab"
Output:
"aaaaaaab"
Expected Output:
"b"

Test 8
Input:
s: "abbaabbaabba"
Output:
"abbaabbaabba"
Expected Output:
""

Test 9
Input:
s: "abababaaab"
Output:
"abababaaab"
Expected Output:
"b"

Test 10
Input:
s: "bbabbaabaabbbbb"
Output:
"bbabbaabaabbbbb"
Expected Output:
""
*/
