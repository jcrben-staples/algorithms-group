import assert from "assert";

// TODO: giving up for now
// Approach idea:
// imolement longest arithmetic progress DP table from below
// check if adding elements from b add to that table... if they do, they should be incremented
//

// https://www.youtube.com/watch?v=-NIlLdVKBFs  by HappyCoding
// code in longest_arithmetic_s
// https://www.geeksforgeeks.org/longest-arithmetic-progression-dp-35/
// ***  set[i] + set[k] = 2*set[j]
// more code at https://github.com/wey068/Facebook-Interview-Coding/blob/master/Longest%20Arithmetic%20Subsequence.java

// https://leetcode.com/discuss/interview-question/917047/amazon-online-assessment-question
// https://leetcode.com/discuss/interview-question/1067789/maximum-arithmetic-sequence-of-two-different-arrays

function isArithmeticProgression(arr) {
    let firstDiff = a[1] - a[0];
    for (let i = 1; i < a.length - 1; i++) {
        if (a[i] - a[i - 1] != firstDiff) {
            return false;
        }
    }
    return true;
}

// good hint at https://leetcode.com/discuss/interview-question/1067789/Maximum-Arithmetic-sequence-of-two-different-arrays/851838
function maxArithmeticLength(a, b) {
    // function helper(arr) {}
    // let last = a[a.length - 1];
    // let bStep = b.findIndex((val) => {});
    // let sumSoFar = a.slice();
    let A = a.slice();
    let dp = {};

    let maxLen = isArithmeticProgression(a)
        ? a.length
        : Number.NEGATIVE_INFINITY;

    let currDiff;
    // function dfs() {
    let l1 = 0;
    let r1 = 1;

    let l2 = 0;
    let r2 = 1;
    for (let i = 0; i < a.length; i++) {
        let Astart = a[l1];
        let Aend = a[r1];

        let Bstart = b[l2];
        // let Bend = b[r2];

        while (Astart < Bstart) {
            currDiff = Bstart - Astart;
            A = A.unshift(Bstart);
        }
        // find where to put b
        // for (let j = 0; j < )
    }
    // }
}

/* 
// from https://replit.com/@RanadeepReddyRe/MelodicAnotherBinarytree#Main.java
// posted at https://leetcode.com/discuss/interview-question/1067789/maximum-arithmetic-sequence-of-two-different-arrays
// but seems to be incorrect
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        int a[] = {0, 4, 8, 16};
        int b[] = {0, 2, 6, 10,12, 14,18, 20};
        int l;
        l =  maxArithmeticLength(a, b);
        System.out.println("a = "+Arrays.toString(a)+", b = "+Arrays.toString(b)+", count "+l);

        a = new int[]{100, 180, 200};
        b = new int[]{102, 105, 110, 115, 120, 125, 130, 131, 135, 140, 145, 150, 155, 160, 165, 170, 173, 175, 185, 190, 195, 205, 210, 215, 220, 225, 230, 235, 240, 241, 245, 446, 471};
        l =  maxArithmeticLength(a, b);
        System.out.println("a = "+Arrays.toString(a)+", b = "+Arrays.toString(b)+", count "+l);

        a = new int[]{2, 4, 8};
        b = new int[]{1, 6, 10, 12};
        l =  maxArithmeticLength(a, b);
        System.out.println("a = "+Arrays.toString(a)+", b = "+Arrays.toString(b)+", count "+l);

        a = new int[]{1,5};
        b = new int[]{3,7,9};
        l =  maxArithmeticLength(a, b);
        System.out.println("a = "+Arrays.toString(a)+", b = "+Arrays.toString(b)+", count "+l);

    }

    private static int maxArithmeticLength(int[] a, int[] b) {
        int minDiff = findMinDiff(a);
        int i=1,j = 1,M = a.length, N = b.length, count = 1, previous = a[0];
        Set<Integer> listA = Arrays.stream(a).boxed().collect(Collectors.toSet());
        Set<Integer> listB = Arrays.stream(b).boxed().collect(Collectors.toSet());
        System.out.println("listA.contains(previous+minDiff) "+listA.contains(4));

        while (i<M || j < N){
            if (listA.contains(previous+minDiff)) {
                previous = previous+minDiff;
                count++;
                i++;
            }
            else if(listB.contains(previous+minDiff)) {
                previous =previous+minDiff;
                count++;
                j++;

            }
            else break;

        }
        return i< a.length ? -1 : count;
    }

    private static int findMinDiff(int[] a) {
        int min = Integer.MAX_VALUE;
        for(int i=0; i<a.length-1;i++){
                min = Math.min(min,a[i+1]-a[i]);
        }
        return min;
    }

}
*/

/*
You are given two arrays of integers a and b, which are both sorted in an ascending order and contain unique elements (i.e. no duplicates).

You can take several (possibly zero) numbers from the array b and add them to a at any positions in any order. You want the array a to be an arithmetic progression after this.

Your task is to find the maximal length of the resulting arithmetic progression represented by array a that can be achieved. If it is impossible to obtain an array forming an arithmetic progression, return -1.

Example

For a = [0, 4, 8, 16] and b = [0, 2, 6, 12, 14, 20], the output should be maxArithmeticLength(a, b) = 6.

You can add b[3] = 12 and b[5] = 20 to a and obtain array [0, 4, 8, 12, 16, 20], which is an arithmetic progression of length 6 (the sequence increases by 4 from each element to the next). It is impossible to obtain a longer arithmetic progression, so the answer is 6.

For a = [5, 7, 13, 14] and b = [9, 11, 15], the output should be maxArithmeticLength(a, b) = -1.

It is impossible to obtain an arithmetic progression with these elements, so the answer is -1.

Input/Output

[execution time limit] 4 seconds (js)

[input] array.integer a

An array of unique integers sorted in ascending order.

Guaranteed constraints:
2 ≤ a.length ≤ 1000,
0 ≤ a[i] ≤ 105.

[input] array.integer b

An array of unique integers sorted in ascending order.

Guaranteed constraints:
2 ≤ b.length ≤ 1000,
0 ≤ b[i] ≤ 105.

[output] integer

The maximal length of the arithmetic progression constructed by adding a certain number of elements from b to the array a.

Test 1
Input:
a: [0, 4, 8, 16]
b: [0, 2, 6, 12, 14, 20]
Output:
null
Expected Output:
6

Test 2
Input:
a: [5, 7, 13, 14]
b: [9, 11, 15]
Output:
null
Expected Output:
-1

Test 3
Input:
a: [7, 13]
b: [1, 10, 16]
Output:
null
Expected Output:
4

Test 4
Input:
a: [20, 22]
b: [19, 21, 23, 24, 26, 28]
Output:
null
Expected Output:
6

Test 5
Input:
a: [31, 39, 43, 45]
b: [0, 1, 2, 30, 31, 33, 35, 37, 38, 39, 40, 41, 45, 47]
Output:
null
Expected Output:
9

Test 6
Input:
a: [0, 36, 60, 78]
b: [12, 24, 48, 72, 90]
Output:
null
Expected Output:
-1

Test 7
Input:
a: [96000, 97000]
b: [99000, 100000]
Output:
null
Expected Output:
2
*/
