function minesweeper(matrix) {
    const rows = matrix.length;
    const cols = matrix[0].length;
    const output = [];

    for (let i = 0; i < rows; i++) {
        output.push([]);
        for (let j = 0; j < cols; j++) {
            const up = matrix?.[i - 1]?.[j];
            const left = matrix?.[i]?.[j - 1];
            const right = matrix?.[i]?.[j + 1];
            const down = matrix?.[i + 1]?.[j];
            const upperleft = matrix?.[i - 1]?.[j - 1];
            const upperright = matrix?.[i - 1]?.[j + 1];
            const lowerleft = matrix?.[i + 1]?.[j - 1];
            const lowerright = matrix?.[i + 1]?.[j + 1];

            let count = 0;
            for (let val of [
                up,
                left,
                right,
                down,
                upperleft,
                upperright,
                lowerleft,
                lowerright,
            ]) {
                if (val === true) {
                    count += 1;
                }
            }
            output[i].push(count);
        }
    }
    return output;
}

console.log(
    "minesweeper",
    minesweeper([
        [true, false, false],
        [false, true, false],
        [false, false, false],
    ])
);
