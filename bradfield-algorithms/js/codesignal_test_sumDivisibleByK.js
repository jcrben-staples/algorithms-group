// function sumsDivisibleByK(a, k) {
//     let result = 0;

//     for (let i = 0; i < a.length; i++) {
//         for (let j = 1; j < a.length; j++) {
//             if (a[i] + (a[j] % k) === 0) {
//                 result += 1;
//             }
//         }
//     }
//     return result;
// }

function sumsDivisibleByK(a, k) {
    let result = 0;
    let i = 0;
    let j = a.length - 1;

    while (i < a.length) {
        while (j > i) {
            if ((a[i] + a[j]) % k === 0) {
                result += 1;
            }
            j -= 1;
        }
        j = a.length - 1;
        i += 1;
    }

    return result;
}

console.log("[1, 2, 3, 4, 5]", sumsDivisibleByK([1, 2, 3, 4, 5]));

// You are given an array of integers a and an integer k. Your task is to calculate the number of ways to pick two different indices i < j, such that a[i] + a[j] is divisible by k.

// Example

// For a = [1, 2, 3, 4, 5] and k = 3, the output should be sumsDivisibleByK(a, k) = 4.

// There are 4 pairs of numbers that sum to a multiple of k = 3:

// a[0] + a[1] = 1 + 2 = 3
// a[0] + a[4] = 1 + 5 = 6
// a[1] + a[3] = 2 + 4 = 6
// a[3] + a[4] = 4 + 5 = 9
