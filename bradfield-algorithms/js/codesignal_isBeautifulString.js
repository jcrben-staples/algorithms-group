function isBeautifulString(inputString) {
    let hash = {};
    let ordered = [];

    for (const x of inputString) {
        hash[x] = hash[x] != null ? hash[x] + 1 : 1;
    }

    for (const [key, val] of Object.entries(hash)) {
        ordered.push([key, val]);
    }

    ordered.sort((a, b) => {
        if (a < b) return -1;
        else return 1;
    });

    console.log("ordered", ordered);

    for (let i = 0; i < ordered.length; i++) {
        if (ordered?.[i + 1]?.[1] != null) {
            const num1 = ordered[i][1];
            const num2 = ordered[i + 1][1];
            if (num1 > num2) {
                return false;
            }
        }
    }

    return true;
}

console("isBeautifulString", isBeautifulString("bbbaacdafe"));
