const Node = function(val) {
    this.val = val || null;
    this.left = null;
    this.right = null;

    this.insert = () => {};
};

const root = new Node(1);

root.left = new Node(2);
root.right = new Node(3);

root.left.left = new Node(4);
root.left.left.left = new Node(5);

const traverseBinary = tree => {
    console.log("traversing with val:", tree.val);
    if (left) {
        traverseBinary(left);
    }

    if (right) {
        traverseBinary(right);
    }
};
