function boxBlur(image) {
    const rows = image.length;
    const columns = image[0].length;
    if (rows.length === 0 && columns.length === 0) return image;
    const output = [];

    for (let i = 1; i < rows - 1; i++) {
        output.push([]);
        for (let j = 1; j < columns - 1; j++) {
            const middle = image[i][j];
            const up = image[i - 1][j];
            const left = image[i][j - 1];
            const down = image[i + 1][j];
            const right = image[i][j + 1];
            const upperleft = image[i - 1][j - 1];
            const upperright = image[i - 1][j + 1];
            const lowerleft = image[i + 1][j - 1];
            const lowerright = image[i + 1][j + 1];
            let sum = Math.floor(
                (middle +
                    up +
                    upperleft +
                    upperright +
                    lowerleft +
                    lowerright +
                    left +
                    down +
                    right) /
                    9
            );
            //    if (!output[i][j])
            output[i - 1].push(sum);
        }
    }

    return output;
}

console.log(
    "boxBlur",
    boxBlur([
        [36, 0, 18, 9],
        [27, 54, 9, 0],
        [81, 63, 72, 45],
    ])
);
