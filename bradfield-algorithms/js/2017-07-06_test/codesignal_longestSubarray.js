const assert = require("assert");
const test9 = require("./test-9.json").input;

function longestSubarrayCheck(a, b, c) {
    // check that all b in c using set
    // https://www.geeksforgeeks.org/find-whether-an-array-is-subset-of-another-array-set-1/
    let cSet = c.reduce((prev, next) => {
        prev.add(next);
        return prev;
    }, new Set());

    let bSet = b.reduce((prev, next) => {
        prev.add(next);
        return prev;
    }, new Set());
    // console.log("cSet", cSet, "bSet", bSet);
    // console.log("cSet.size !== bSet.size", cSet.size !== bSet.size);
    for (let el of bSet) {
        if (!cSet.has(el)) {
            return false;
        }
    }
    // if (cSet.size !== bSet.size) {
    //     return false;
    // }

    let longest = Number.NEGATIVE_INFINITY;
    let foundMatch = false;

    // get all subarrays which are least the length of b
    // let possibleSubArrays = [];
    let l = 0;
    while (l <= a.length - b.length) {
        let j = l;
        let r = j + b.length;
        while (r <= a.length) {
            let subarr = a.slice(j, r);
            let matchC = true;
            let matchB = true;
            for (let i = 0; i < subarr.length; i++) {
                if (!cSet.has(subarr[i])) {
                    matchC = false;
                    // break;
                }

                if (b[i] !== subarr[i]) {
                    matchB = false;
                    // break;
                }
            }
            if (matchB) {
                foundMatch = true;
            }
            longest = matchC ? Math.max(longest, subarr.length) : longest;
            r += 1;
        }
        l += 1;
    }

    if (b.length === longest && foundMatch) {
        return true;
    } else {
        return false;
    }
    // let bSet = new Set();

    // let longest =

    // for (let i = 0; i < b.length; i++) {

    // }
}

// Test 1 - FAILS
// console.log(longestSubarrayCheck([1, 1, 5, 1, 2], [1, 2], [2, 1]));
// assert.strictEqual(longestSubarrayCheck([1, 1, 5, 1, 2], [1, 2], [2, 1]), true);
// Input:
// a: [1, 1, 5, 1, 2]
// b: [1, 2]
// c: [2, 1]
// Output:
// false
// Expected Output:
// true

// Test 3 - FAILS
// assert.strictEqual(
//     longestSubarrayCheck([1, 2, 2, 3, 2, 1, 3], [(3, 2, 1, 3)], [2, 1, 3]),
//     false
// );
// console.log(
//     longestSubarrayCheck([1, 2, 2, 3, 2, 1, 3], [(3, 2, 1, 3)], [2, 1, 3])
// );
// Input:
// a: [1, 2, 2, 3, 2, 1, 3]
// b: [3, 2, 1, 3]
// c: [2, 1, 3]
// Output:
// true
// Expected Output:
// false

// Test 9
assert.strictEqual(longestSubarrayCheck(test9.a, test9.b, test9.c), true);
// The test case is too large and is shown truncated
// Input:
// a: [549218566, 187604091, 601655556, 549114877, 788445302, 818062433, 588751548, 48972608, 968050454, 218449522, 788668032, 761416085, 32601858, 153382406, 663261738, 923429767, 857606675, 842942732, 125497764, 877388693]
// b: [48972608, 968050454, 218449522, 788668032, 761416085, 32601858, 153382406, 663261738, 923429767, 857606675]
// c: [126466067, 460597114, 752507175, 382912186, 563515761, 421490317, 49238607, 493706413, 789847152, 546448832, 181772651, 517602529, 855345530, 240105186, 179540801, 587497182, 763748624, 860275874, 769172014, 527925000, 26848919, 481825172, 242007582, 663261738, 273136951, 756330317, 129993430, 114696813, 788668032, 248960964, 12145488, 970293987, 852175774, 963348057, 924472673, 500623660, 514649566, 853853348, 153382406, 48972608, 586035137, 415855055, 833878733, 470949137, 725181063, 56313301, 987695286, 560841372, 668692564, 556846562, 203711687, 857606675, 285130230, 728773765, 466634502, 675233830, 113423049, 479843724, 988363811, 614837101, 681966121, 376829935, 302596761, 895027275, 523514012, 915713277, 673472697, 925905382, 261745523, 880769654, 628070142, 22537488, 930284877, 241921093, 553224850, 387692019, 132422884, 742862044, 108385773, 984716753, 49805919, 768244789, 73684954, 154208081, 255006562, 2074518, 17422677, 32601858, 42164930, 887466982, 913017912, 106191350, 968050454, 139729559, 622875100, 696124965, 411409362, 552607428, 349778025, 156409863, ...]
// Output:
// false
// Expected Output:
// true

/*
You are given two integer arrays a, b and one array of distinct integers c. Your task is to check whether b is the longest contiguous subarray of a consisting only of elements from c, i.e.

Each of the elements of b belong to c,
a contains b as a contiguous subarray,
b is the longest of the contiguous subarrays of a which satisfy the first two conditions.
Return true if all the above conditions are met, and false otherwise.

NOTE: If there is a tie for the longest contiguous subarrays of a consisting of elements from c, the answer is still considered true if b is one of these longest contiguous subarrays.

Example

For a = [1, 1, 5, 1, 2], b = [1, 2], and c = [2, 1], the output should be longestSubarrayCheck(a, b, c) = true.

All three conditions are met:

All of the elements of b belong to c,
a contains b as a contiguous subarray (a[3..4] = b),
b is the longest of these contiguous subarrays. To prove this, let's look at all the contiguous subarrays of length greater than 2:
a[0..2] = [1, 1, 5] contains 5, which doesn't belong to c.
a[0..3] = [1, 1, 5, 1] contains 5, which doesn't belong to c.
a[0..4] = [1, 1, 5, 1, 2] contains 5, which doesn't belong to c.
a[1..3] = [1, 5, 1] contains 5, which doesn't belong to c.
a[1..4] = [1, 5, 1, 2] contains 5, which doesn't belong to c.
a[2..4] = [5, 1, 2] contains 5, which doesn't belong to c.
Therefore b is the longest contiguous subarray of a consisting only of elements from c, so the answer is true.

For a = [1, 2, 3, 6, 1, 1, 1], b = [1, 2, 3], and c = [2, 1], the output should be longestSubarrayCheck(a, b, c) = false.

Although b is a contiguous subarray of a, it contains the element b[2] = 3 which does not appear in c, therefore it does not meet the conditions. So the answer is false.

For a = [1, 2, 2, 3, 2, 1, 3], b = [3, 2, 1, 3], and c = [2, 1, 3], the output should be longestSubarrayCheck(a, b, c) = false.

All of the elements of a belong to c, and b.length < a.length, so b couldn't possibly be the longest contiguous subarray consisting of elements from c. So, the answer is false.

Input/Output

[execution time limit] 4 seconds (js)

[input] array.integer a

An array of integers.

Guaranteed constraints:
1 ≤ a.length ≤ 105,
0 ≤ a[i] ≤ 109.

[input] array.integer b

An array of integers.

Guaranteed constraints:
1 ≤ b.length ≤ 105,
0 ≤ b[i] ≤ 109.

[input] array.integer c

An array of distinct integers.

Guaranteed constraints:
1 ≤ c.length ≤ 105,
0 ≤ c[i] ≤ 109.

[output] boolean

Return true if b is the longest contiguous subarray of a consisting of elements from c, otherwise return false.
*/
