function absoluteValuesSumMinimization(a) {
    let closestNum;
    let smallestDiff = Number.POSITIVE_INFINITY;
    for (let i = 0; i < a.length; i++) {
        let sum = 0;
        let j = a.length - 1;
        while (j > i) {
            sum += Math.abs(a[j] - a[i]);
            j -= 1;
        }

        let k = 0;
        while (k < i) {
            sum += Math.abs(a[k] - a[i]);
            k += 1;
        }

        if (sum < smallestDiff) {
            smallestDiff = sum;
            closestNum = a[i];
        }
    }

    return closestNum;
}

console.log("[2, 4, 7]", absoluteValuesSumMinimization([2, 4, 7]));
