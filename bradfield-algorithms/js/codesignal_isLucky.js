function isLucky(n) {
    const n2 = Array.from(String(n), (x) => Number(x));
    const half = n2.length / 2;
    let firstsum = 0;
    let secondsum = 0;

    for (let i = 0; i < half; i++) {
        firstsum += n2[i];
    }

    for (let j = half; j < n2.length; j++) {
        secondsum += n2[j];
    }

    console.log("f", firstsum, "s", secondsum);
    if (secondsum == firstsum) return true;
    else return false;
}

isLucky(1230);
