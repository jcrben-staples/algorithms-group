function variableName(name) {
    if (/^[0-9]/.test(name[0])) return false;
    // regex: [a-Z][0-9]_
    // const allowedChars = ''
    if (!/^[A-z]|_/.test(name[0])) return false;
    const regex = /[a-zA-Z]|[0-9]|_/;
    for (let i = 1; i < name.length; i++) {
        const t = !regex.test(name[i]);
        if (t) {
            return false;
        }
    }
    return true;
}

console.log("variableName", variableName("va[riable0"));
