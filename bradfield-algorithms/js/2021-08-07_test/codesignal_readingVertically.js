// all tests pass
// https://math.stackexchange.com/questions/1941224/number-of-occurrences-in-contiguous-subarrays - mathy way?
function readingVertically(arr) {
    let rv = "";
    let lengthLongestWord = arr.reduce((prev, curr) => {
        if (curr.length > prev) {
            return curr.length;
        }
        return prev;
    }, 0);

    console.log("lengthLongestWord", lengthLongestWord);

    // for (let [i, k] of Object.entries())
    for (let i = 0; i < lengthLongestWord; i++) {
        for (let word of arr) {
            if (word.length - 1 >= i) {
                rv += word[i];
            }
        }
    }
    return rv;
}
