// slightly more efficient version still fails
// needs a dynamic programming version...?
// use left and right pointer and keep the chars hash updated
// as we encounter new chars
function occurrencesInSubarrays(arr, m) {
    // split into subarrays...
    // let subasLen = Math.floor(arr.length / m)
    let subas = [];
    let result = [];

    let prev = 0;
    let i = m;
    while (i <= arr.length) {
        console.log("entering");
        let sub = arr.slice(prev, i);
        let chars = {};
        for (let el of sub) {
            chars[el] = chars[el] != null ? chars[el] + 1 : 1;
        }
        let freq = 1;
        for (let [k, v] of Object.entries(chars)) {
            if (v > freq) {
                freq = v;
            }
        }
        subas.push(freq);

        prev += 1;
        i += 1;
    }
    console.log("subas", subas);

    // let chars = Array.from(subas, () => { return {}})
    // for (let [i, sub] of subas.entries()) {
    //     for (let el of sub) {
    //         chars[i][el] = chars[i][el] != null ? chars[i][el] + 1 : 1
    //     }
    // }

    // console.log('chars', chars)

    // result = chars.map((val) => {
    //     let freq = 1
    //     for (let [k, v] of Object.entries(val)) {
    //         if (v > freq) {
    //             freq = v
    //         }
    //     }
    //     return freq
    // })

    return subas;
}

// 8/14 -> hidden tests did not pass due to execution limit
function occurrencesInSubarrays(arr, m) {
    // split into subarrays...
    // let subasLen = Math.floor(arr.length / m)
    let subas = [];
    let result = [];

    let prev = 0;
    let i = m;
    while (i <= arr.length) {
        console.log("entering");
        subas.push(arr.slice(prev, i));
        prev += 1;
        i += 1;
    }
    console.log("subas", subas);

    let chars = Array.from(subas, () => {
        return {};
    });
    for (let [i, sub] of subas.entries()) {
        for (let el of sub) {
            chars[i][el] = chars[i][el] != null ? chars[i][el] + 1 : 1;
        }
    }

    console.log("chars", chars);

    result = chars.map((val) => {
        let freq = 1;
        for (let [k, v] of Object.entries(val)) {
            if (v > freq) {
                freq = v;
            }
        }
        return freq;
    });

    return result;
}

/*
Given an array of integers arr and a positive integer m, your task is to find the frequency of the most common element within each contiguous subarray of length m in arr.

Return an array of these highest frequencies among subarray elements, ordered by their corresponding subarray's starting index. You can look at the examples section for a better understanding.

Example

For arr = [1, 2] and m = 2, the output should be occurrencesInSubarrays(arr, m) = [1].

example 1

arr contains only one contiguous subarray of length m = 2 - arr[0..1] = [1, 2]. This subarray contains 2 most frequent elements - 1 and 2, both having a frequency of 1. So, the answer is [1].

For arr = [1, 3, 2, 2, 3] and m = 4, the output should be occurrencesInSubarrays(arr, m) = [2, 2].

example 2

arr contains two contiguous subarrays of length m = 4:

arr[0..3] = [1, 3, 2, 2] contains only one most frequent element - 2, and its frequency is 2.
arr[1..4] = [3, 2, 2, 3] contains two most frequent elements - 2 and 3, both of them have a frequency of 2.
Putting the answers for both subarrays together, we obtain the array [2, 2]
For arr = [2, 1, 2, 3, 3, 2, 2, 2, 2, 1] and m = 3, the output should be occurrencesInSubarrays(arr, m) = [2, 1, 2, 2, 2, 3, 3, 2].

example 3

arr contains 8 contiguous subarrays of length m = 3:

arr[0..2] = [2, 1, 2] contains only one most frequent element - 2, and its frequency is 2.
arr[1..3] = [1, 2, 3] contains three most frequent elements - 1, 2, and 3. All of them have frequency 1.
arr[2..4] = [2, 3, 3] contains only one most frequent element - 3, and its frequency is 2.
arr[3..5] = [3, 3, 2] contains only one most frequent element - 3, and its frequency is 2.
arr[4..6] = [3, 2, 2] contains only one most frequent element - 2, and its frequency is 2.
arr[5..7] = [2, 2, 2] contains only one most frequent element - 2, and its frequency is 3.
arr[6..8] = [2, 2, 2] contains only one most frequent element - 2, and its frequency is 3.
arr[7..9] = [2, 2, 1] contains only one most frequent element - 1, and its frequency is 2.
Putting the answers for both subarrays together, we obtain the array [2, 1, 2, 2, 2, 3, 3, 2].
*/
