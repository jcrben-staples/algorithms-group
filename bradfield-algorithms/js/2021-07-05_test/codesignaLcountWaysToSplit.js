// https://stackoverflow.com/questions/65715189/spilt-string-in-3-non-empty-parts-such-that-a-b-b-c-and-c-a-are-all-diffe
// https://leetcode.com/discuss/interview-question/922241/quora-oa-2020-ways-to-split-string
function countWaysToSplit(s) {}

/*
You are given a string s. Your task is to count the number of ways of splitting s into three non-empty parts a, b and c (s = a + b + c) in such a way that a + b, b + c and c + a are all different strings.

NOTE: + refers to string concatenation.

Example

For s = "xzxzx", the output should be countWaysToSplit(s) = 5.

Consider all the ways to split s into three non-empty parts:

If a = "x", b = "z" and c = "xzx", then all a + b = "xz", b + c = "zxzx" and c + a = xzxx are different.
If a = "x", b = "zx" and c = "zx", then all a + b = "xzx", b + c = "zxzx" and c + a = zxx are different.
If a = "x", b = "zxz" and c = "x", then all a + b = "xzxz", b + c = "zxzx" and c + a = xx are different.
If a = "xz", b = "x" and c = "zx", then a + b = b + c = "xzx". Hence, this split is not counted.
If a = "xz", b = "xz" and c = "x", then all a + b = "xzxz", b + c = "xzx" and c + a = xxz are different.
If a = "xzx", b = "z" and c = "x", then all a + b = "xzxz", b + c = "zx" and c + a = xxzx are different.
Since there are five valid ways to split s, the answer is 5.

Input/Output

[execution time limit] 4 seconds (js)

[input] string s

A string to split.

Guaranteed constraints:
3 ≤ s.length ≤ 100.

[output] integer

The number of ways to split the given string.

Test 1
Input:
s: "xzxzx"
Output:
null
Expected Output:
5

Test 2
Input:
s: "xzy"
Output:
null
Expected Output:
1

Test 3
Input:
s: "xxx"
Output:
null
Expected Output:
0

Test 4
Input:
s: "xzxzxzxzxz"
Output:
null
Expected Output:
30

Test 5
Input:
s: "xxxxxxxxxx"
Output:
null
Expected Output:
24

Test 6
Input:
s: "xyzxyzxyzx"
Output:
null
Expected Output:
35

Test 7
Input:
s: "xzxzxxzzxx"
Output:
null
Expected Output:
36

Test 8
Input:
s: "gggggggggggggggggggggggggggggg"
Output:
null
Expected Output:
366

Test 9
Input:
s: "gfgfgfgfgfgfgfgfgfgfgfgfgfgfgf"
Output:
null
Expected Output:
387
*/
