// copied from their solutions; pops out evenly except for once
function palindromeRearranging(s) {
    var ss = s.split("").sort(),
        r = 0;
    while (ss.length) {
        var t = ss.shift();
        if (t === ss[0]) ss.shift();
        else r++;
    }
    return r < 2;
}

// solution was not fast enough
function palindromeRearranging(inputString) {
    let a2 = Array.from(inputString);
    let single = false;
    a2.sort((a, b) => {
        if (a < b) return -1;
    });

    let numPerChar = {};

    for (let char of a2) {
        numPerChar[char] = numPerChar[char] != null ? numPerChar[char] + 1 : 1;
    }

    for (const [key, val] of Object.entries(numPerChar)) {
        let val2 = parseInt(val, 10);
        if (val == 1 && single == false) {
            single = true;
            continue;
        }

        if (val == 1 && single == true) {
            return false;
        }

        if (val > 1 && val % 2 != 0) return false;
    }

    return true;
}

console.log(palindromeRearranging("abbcabb"));
