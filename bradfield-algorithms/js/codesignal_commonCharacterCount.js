function commonCharacterCount(s1, s2) {
    let shortest = s1.length > s2.length ? s1 : s2;
    let hash1 = {};
    let hash2 = {};
    let count = 0;

    for (let i = 0; i < shortest.length; i++) {
        console.log("hash1[s1[i]]", hash1[s1[i]]);
        hash1[s1[i]] = hash1[s1[i]] != null ? hash1[s1[i]] + 1 : 1;
        hash2[s2[i]] = hash2[s2[i]] != null ? hash2[s2[i]] + 1 : 1;
        // if (s1[i] == s2[i]) {
        //     hash1[s1[i]] += 1
        //     hash2[s2[i]] += 1
        //     // count += 1;
        // }

        // Math.min
    }

    console.log("hash1", hash1);

    for (const [key, value] of Object.entries(hash1)) {
        console.log("hash1[key]", hash1[key], hash2[key]);
        count += Math.min(hash1[key] || 0, hash2[key] || 0);
    }

    console.log("count", count);

    return count;
}

commonCharacterCount("aabcc", "adcaa");
