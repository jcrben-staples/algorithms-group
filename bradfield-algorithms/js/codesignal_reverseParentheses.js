function reverseInParentheses(inputString) {
    // function reverseStr() {

    // }
    let stack = [];

    for (let i = 0; i < inputString.length; i++) {
        if (inputString[i] === ")") {
            let j = i;
            let tmp = "";
            i = stack.length - 1;
            while (stack[i] != "(") {
                i -= 1;
                let top = stack.pop();
                tmp += top;
                // stack.push(inputString[i])
            }

            let openingParen = stack.pop();
            // i -= 1;
            i = j;
            stack = stack.concat(tmp.split(""));
        }
        if (i < inputString.length && inputString[i] != ")") {
            stack.push(inputString[i]);
        }
    }
    return stack.reduce((prev, curr) => {
        return prev + curr;
    }, "");
}

let r = reverseInParentheses("foo(bar(baz))blim");
console.log("r", r);
