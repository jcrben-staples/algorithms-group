/*
Use to compose multiple higher-order components into a single higher-order component. This works exactly like the function of the same name in Redux, or lodash's flowRight().
-per recompose docs

"The exception is the right-most argument which can accept multiple parameters, as it will provide the signature for the resulting composed function."
-https://redux.js.org/api/compose
*/

const compose = (...funcs) => {
    return funcs.reduce((a, b) => {
        // console.log('a', a)
        // console.log('b', b)
        return (...args) => {
            console.log("args", args);
            return a(b(...args));
        };
    }, arg => arg);
};

const myfuncs = {
    f1: p1 => {
        console.log("p1", p1);
        return p1 + 1;
        // return (p6) => {
        //     console.log('p6', p6)
        //     return p1;
        // }
    },
    f2: p2 => {
        console.log("p2", p2);
        return p2 + 2;
        // return (p5) => {
        //     console.log('p5')
        //     return p2;
        // }
    }
};

const f3 = (arg1, arg2 = 1, arg3 = 1) => {
    return arg1 + arg2 + arg3;
};

// myfuncs.f1('param1')

const vals = compose(
    myfuncs.f1,
    myfuncs.f2,
    f3
);
console.log("vals", vals(1, 3, 5));

// const vals = compose(myfuncs, () => {
//     console.log('p3');
//     return () => {
//         console.log('p4')
//     }
// });

// console.log('vals', vals(1,2,3))

// const res = vals(1,2,3);
// console.log('res', res)
// console.log('res', res(1)(1))
