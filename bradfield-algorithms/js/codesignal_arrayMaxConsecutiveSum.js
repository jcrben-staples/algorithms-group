function arrayMaxConsecutiveSum(inputArray, k) {
    let result = Number.NEGATIVE_INFINITY;
    for (let i = 0; i < inputArray.length; i++) {
        let sum = inputArray[i];
        let j = i + 1;
        while (j < i+k) {
            if (inputArray[j] != null) {
            sum += inputArray[j]
            }
            j += 1;
        }
        result = Math.max(result, sum)
    }
    
    return result;

}


console.log(
    "arrayMaxConsecutiveSum",
    arrayMaxConsecutiveSum([2, 3, 5, 1, 6], 2)
);
