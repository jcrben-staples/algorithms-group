function adjacentElementsProduct(inputArray) {
    let result = Number.NEGATIVE_INFINITY;
    let product;

    for (let i = 0; i < inputArray.length - 1; i++) {
        product = inputArray[i] * inputArray[i + 1];
        console.log("product", product);
        result = Math.max(product, result);
    }

    return result;
}

r = adjacentElementsProduct([-23, 4, -3, 8, -12]);
