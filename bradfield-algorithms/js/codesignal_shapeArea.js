// function shapeArea(n) {
//     let sum = 0;
//     const helper = (nMinus) => {
//         let r;
//         if (nMinus == 1) {
//             // sum += 1;
//             return 1;
//         }

//         for (let i = 0; i < nMinus; i++) {
//             sum += helper(nMinus - 1);
//         }

//         return sum;

//         // return r;
//     };

//     r = helper(n);

//     return sum;
// }

// from https://github.com/RonCanCode/CodeFights_Solutions/blob/master/shapeArea.cpp
// function shapeArea(n) {
//     let area = 0;

//     if (n == 1) {
//         area = 1;
//         return area;
//     } else {
//         area = shapeArea(n - 1) + n * 2 + (n - 2) * 2;
//         return area;
//     }
// }

// from https://codeworldtechnology.wordpress.com/2020/08/25/shapearea/
function shapeArea(n) {
    if (n == 1) {
        return 1;
    } else {
        return shapeArea(n - 1) + 4 * (n - 1);
    }
}

console.log("shapeArea(3)", shapeArea(3));
