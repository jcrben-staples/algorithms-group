// requires perms as described at [codefights arcade intro task 33](https://www.youtube.com/watch?v=PFOE2CAEA5E)
// ultimately Hamiltonian path probem!
// solution at https://app.codesignal.com/arcade/intro/level-7/PTWhv2oWqd6p4AHB9/solutions?solutionId=jmXr4F8X8M3RczKPA
// from https://app.codesignal.com/profile/js_the_game/overview
function stringsRearrangement(inputArray) {
    function differsByOneChar(arr) {
        for (let i = 0; i < arr.length - 1; i++) {
            let mismatches = 0;
            let str = arr[i];
            let strNext = arr[i + 1];
            if (str === strNext) return false;
            // let singleDifference = false;
            for (let k = 0; k < str.length; k++) {
                if (str?.[k] !== strNext?.[k]) {
                    if (mismatches > 1) {
                        return false;
                    } else {
                        mismatches++;
                    }
                }
                // if (str?.[j] !== strNext?.[j] && !singleDifference) {
                //     singleDifference = true;
                // } else if (str?.[j] !== strNext?.[j] && singleDifference) {
                //     return false;
                // }
            }
            if (mismatches != 1) return false;
        }
        return true;
    }

    let perms = [];
    let soFar = [];

    function recurse(subarr) {
        if (soFar.length === inputArray.length) {
            perms.push(soFar.slice());
            return;
        }

        for (let i = 0; i < subarr.length; i++) {
            soFar.push(subarr[i]);
            recurse([...subarr.slice(0, i), ...subarr.slice(i + 1)]);
            soFar.pop();
        }
    }

    recurse(inputArray);

    for (let x of perms) {
        let differsProperly = differsByOneChar(x);
        if (differsProperly) return true;
        // for (let i = 0; i < inputArray.length; i++) {
        //     let differsProperly = differsByOneChar(perms[i]);
        //     if (differsProperly) return true;
        // }
    }

    return false;
    // let differs = differsByOneChar(inputArray);
    // if (differs) return true;
    // for (let i = 0; i < inputArray.length; i++) {
    //     for (let j = 0; j < inputArray.length; j++) {
    //         let rearranged = inputArray.slice();
    //         let tmp = inputArray[i];
    //         rearranged[i] = inputArray[j];
    //         rearranged[j] = tmp;
    //         let differsProperly = differsByOneChar(rearranged);
    //         if (differsProperly) return true;
    //     }
    // }

    // return false;
}

console.log(
    "stringsRearrangement",
    stringsRearrangement(["zzzzab", "zzzzbb", "zzzzaa"])
);
