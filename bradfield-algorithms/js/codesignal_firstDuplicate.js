function firstDuplicate(a) {
    const dupsToIndexes = {};
    let smallest = Number.POSITIVE_INFINITY;
    let result;

    for (let i = 0; i < a.length; i++) {
        const char = a[i];
        if (dupsToIndexes[char] == null) {
            dupsToIndexes[char] = [i];
        } else {
            dupsToIndexes[char].push(i);
        }
    }

    for (const [key, val] of Object.entries(dupsToIndexes)) {
        const secondIndex = val[1] ? val[1] : Number.POSITIVE_INFINITY;
        if (secondIndex < smallest) {
            smallest = Math.min(smallest, secondIndex);
            result = key;
        }
    }

    return result ? parseInt(result, 10) : -1;
}

// console.log("[2, 1, 3, 5, 3, 2]", firstDuplicate([2, 1, 3, 5, 3, 2]));
console.log(
    "[8, 4, 6, 2, 6, 4, 7, 9, 5, 8]",
    firstDuplicate([8, 4, 6, 2, 6, 4, 7, 9, 5, 8])
);
