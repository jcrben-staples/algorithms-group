from typing import List, Any
import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from python.utils import utils  # noqa


# https://leetcode.com/problems/combination-sum-iv/


class Solution:
    def combinationSum4(self, nums: List[int], target: int) -> int:
        dp = [0] * (target + 1)
        dp[0] = 1
        combos: List[Any] = [[] * (target + 1)]
        combos[0] = [1] * target
        # nums = sorted(nums)

        # from https://leetcode.com/problems/combination-sum-iv/discuss/85036/1ms-Java-DP-Solution-with-Detailed-Explanation
        for i in range(1, target + 1):
            for j in nums:
                if i - j >= 0:
                    # if i - j == 0:
                    # print("inside")
                    # combos.append([i, j])
                    # for k in combos:
                    #     times = 1
                    #     if target % j == 0:
                    #         times = target // j
                    #     k.append(j * times)
                    dp[i] += dp[i - j]

        # print("dp", dp)
        # print("combos", combos)
        return dp[target]


if __name__ == "__main__":
    solution = Solution()
    r = solution.combinationSum4([1, 2, 3], 4)
    utils.assertEqual(r, 7)

