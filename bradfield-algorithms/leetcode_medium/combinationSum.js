/**
 * @param {number[]} candidates
 * @param {number} target
 * @return {number[][]}
 */
var combinationSum2 = function(candidates, target, combo, result, memo) {
    memo = memo || new Map();
    combo = combo || [];
    result = result || [];

    if (combo.reduce((a, b) => a + b, 0) > target) {
        combo = [];
        return;
    }

    for (let i = 0; i < candidates.length; i++) {
        // combo = combo.concat(candidates.slice(i, i + 1));
        combinationSum2(
            candidates.slice(i + 1, candidates.length),
            target,
            combo.concat(candidates.slice(i, i + 1)),
            result,
            memo
        );
    }

    if (combo.reduce((a, b) => a + b, 0) === target) {
        combo.sort((a, b) => a - b);
        if (memo.get(combo.toString()) == combo.toString()) return result;
        memo.set(combo.toString(), combo);
        result.push(combo);
        combo = [];
        return;
    }

    if (candidates.length === 0) {
        return;
    }

    return result;
};

console.log(combinationSum2([10, 1, 2, 7, 6, 1, 5], 8));
