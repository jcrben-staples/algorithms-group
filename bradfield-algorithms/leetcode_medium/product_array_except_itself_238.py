# https://leetcode.com/problems/product-of-array-except-self/
class Solution:
    def productExceptSelf(self, nums):
        res = [None] * (len(nums))
        # leftPrecalc =  [None] * (len(nums) - 1)
        # leftPrecalc = list(map(reduce(lambda a, b: a * b, nums), nums))
        leftPrecalc = [nums[0]]

        for i in range(1, len(nums)):
            leftPrecalc.append(nums[i] * leftPrecalc[i - 1])

        rightPrecalc = [None] * (len(nums))
        rightPrecalc[len(nums) - 1] = nums[len(nums) - 1]
        for i in range(len(nums) - 2, -1, -1):
            rightPrecalc[i] = nums[i] * rightPrecalc[i + 1]

        print("leftPrecalc", leftPrecalc)
        print("rightPrecalc", rightPrecalc)
        # rightPrecalc = [None] * (len(nums) - 1)
        # rightPrecalc = list(map(reduce(lambda b, a: a * b, nums), nums))

        for i in range(0, len(nums)):
            # if i == 0:
            #     left = 1
            # else:
            left = leftPrecalc[i - 1] if i > 0 else 1

            # if i == (len(nums) - 1):
            #     right = 1
            # else:
            #  if i + 1 < (len(nums) - 1) else 1
            right = rightPrecalc[i + 1] if i < (len(nums) - 1) else 1
            res[i] = left * right

        # # res = [None] * len(nums)
        # cache = {}
        # partial = None
        # for i, n in enumerate(nums):

        #     left = leftPrecalc[i - 1] if i > 0 else False
        #     right = rightPrecalc[i + 1] if i + 1 <= len(rightPrecalc) else False

        #     if left:
        #         partial += left

        #     if right:
        #         partial += right

        #     res[i] = partial
        #     for j, x in enumerate(nums):
        #         if i == j:
        #             continue

        #         # partial of
        #         partial = x if partial == None else partial * x

        #     res[i] = partial

        return res
