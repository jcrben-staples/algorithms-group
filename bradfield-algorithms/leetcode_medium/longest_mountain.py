"""
Initial attempt: 2018-06-26
    * looked at answer quick
Second attempt: 2018-06-27
    * did not do well, came up with for loop rather than while loop
https://leetcode.com/problems/longest-mountain-in-array/description/
Let's call any (contiguous) subarray B (of A) a mountain if the following properties hold:

B.length >= 3
There exists some 0 < i < B.length - 1 such that B[0] < B[1] < ... B[i-1] < B[i] > B[i+1] > ... > B[B.length - 1]
(Note that B could be any subarray of A, including the entire array A.)

Given an array A of integers, return the length of the longest mountain. 

Return 0 if there is no mountain.

Example 1:

Input: [2,1,4,7,3,2,5]
Output: 5
Explanation: The largest mountain is [1,4,7,3,2] which has length 5.
Example 2:

Input: [2,2,2]
Output: 0
Explanation: There is no mountain.
Note:

0 <= A.length <= 10000
0 <= A[i] <= 10000
Follow up:

Can you solve it using only one pass?
Can you solve it in O(1) space?
"""


class Solution:
    # def longestMountain(self, A):
    #     """
    #     :type A: List[int]
    #     :rtype: int
    #     """

    def longestMountain(self, xs):
        existing_mountain = 0
        mountain_peaked = False
        mountain_length = 0

        for i, x in enumerate(xs):
            print("i:", i, "x", x)
            if i == 0:
                mountain_length += 1
            elif xs[i] > xs[i - 1] and mountain_peaked == False:
                print("mountain ascent")
                mountain_length += 1
            elif xs[i] < xs[i - 1] and mountain_peaked == True:
                print("mountain descent")
                mountain_length += 1
            elif xs[i] < xs[i - 1] and mountain_peaked == False:
                print("mountain peak")
                if i <= len(xs) - 1:
                    if xs[i + 1] < xs[i]:
                        mountain_peaked = True
                    else:
                        # end of moutain here
                        existing_mountain = (
                            mountain_length
                            if mountain_length > existing_mountain
                            else existing_mountain
                        )
                        mountain_peaked = False
                        mountain_length = 1

            elif xs[i] > xs[i - 1] and mountain_peaked == True:
                print("mountain end case")
                # new mountain case
                existing_mountain = (
                    mountain_length + 1
                    if mountain_length + 1 > existing_mountain
                    else existing_mountain
                )

                mountain_peaked = False
                mountain_length = 1
            else:
                print("no mountain")
                # xs[i] == xs[i-1] - no mountain
                mountain_peaked = False
                mountain_length = 0

        return max(mountain_length, existing_mountain)


solution = Solution()
print("solution:", solution.longestMountain([2, 1, 4, 7, 3, 2, 5]))

"""
Solution from leetcode:
class Solution(object):
    def longestMountain(self, A):
        N = len(A)
        ans = base = 0

        while base < N:
            end = base
            if end + 1 < N and A[end] < A[end + 1]: #if base is a left-boundary
                #set end to the peak of this potential mountain
                while end+1 < N and A[end] < A[end+1]:
                    end += 1

                if end + 1 < N and A[end] > A[end + 1]: #if end is really a peak..
                    #set 'end' to right-boundary of mountain
                    while end+1 < N and A[end] > A[end+1]:
                        end += 1
                    #record candidate answer
                    ans = max(ans, end - base + 1)

            base = max(end, base + 1)

        return ans
"""
