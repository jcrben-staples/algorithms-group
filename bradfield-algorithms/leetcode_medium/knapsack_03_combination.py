from typing import List, Any
import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from python.utils import utils  # noqa

# https://leetcode.com/problems/combination-sum/discuss/16502/A-general-approach-to-backtracking-questions-in-Java-(Subsets-Permutations-Combination-Sum-Palindrome-Partitioning)
# https://leetcode.com/problems/combination-sum/discuss/16506/8-line-Python-solution-dynamic-programming-beats-86.77
class Solution:
    def combinationSum(self, candidates, target):
        candidates.sort()
        dp = [[[]]] + [[] for i in range(target)]
        for i in range(1, target + 1):
            for number in candidates:
                if number > i:
                    break
                for L in dp[i - number]:
                    if not L or number >= L[-1]:
                        dp[i] += (L + [number],)
        return dp[target]


if __name__ == "__main__":
    solution = Solution()
    r = solution.combinationSum([1, 2, 3], 4)
    utils.assertEqual(r, [[1, 1, 1, 1], [1, 1, 2], [2, 2], [1, 3]])
