from typing import List, Optional, Any, Dict, Union
import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from python.utils import utils  # noqa

# https://leetcode.com/problems/course-schedule-ii/
# this is an inorder traversal: https://www.geeksforgeeks.org/tree-traversals-inorder-preorder-and-postorder/
class Solution:
    def dfs(self, i, adj_list, visited, result, visiting):
        visiting = visiting or {}
        visiting[i] = True

        children = adj_list.get(i, [])

        for j in children:
            j = str(j)
            if visited.get(j, False):
                continue

            if visiting.get(j, False):
                return True  # cycle

            if self.dfs(j, adj_list, visited, result, visiting):
                return True

        # if at the end...
        result.append(int(i))
        visited[i] = True
        visiting.pop(i, None)

    def findOrder(self, numCourses: int, prerequisites: List[List[int]]) -> List[int]:
        adj_list: Dict[str, List[int]] = {str(i): [] for i in range(numCourses)}

        for i in prerequisites:
            adj_list[str(i[0])].append(i[1])

        visited: Dict[str, bool] = {}
        result: List[Any] = []

        for v in adj_list:
            if v not in visited:
                if self.dfs(str(v), adj_list, visited, result, None):
                    return []  # type: ignore

        return result


if __name__ == "__main__":
    solution = Solution()
    # r = solution.findOrder(2, [[1, 0]])
    # utils.assertEqual(r, [0, 1])

    r = solution.findOrder(2, [[0, 1], [1, 0]])
    utils.assertEqual(r, [])

