"""
* 2020-02-08: reviewed this https://leetcode.com/problems/generate-parentheses/
return array of combinations of parentheses
"""


def generateParenthesis(n):
    result = []

    def inner_gen(combo, leftNum, rightNum):

        if len(combo) == n * 2:
            print("combo")
            result.append(combo)
            return

        if leftNum < n:
            inner_gen(combo + "(", leftNum + 1, rightNum)

        if rightNum < leftNum:
            inner_gen(combo + ")", leftNum, rightNum + 1)

    inner_gen("", 0, 0)
    return result


if __name__ == "__main__":
    parens = gen_parentheses(2)
    print("parens", parens)

