const assert = require("assert");

const Node = ({ children = [], depth = null, sum = null }) => ({
    children: children,
    depth: depth,
    sum: sum
});

const numSquares = target => {
    if (target === 1 || Math.sqrt(target) % 1 === 0) {
        return 1;
    }

    const possibilities = [];

    for (let i = 1; i * i < target; i++) {
        possibilities.unshift(i * i);
    }
    let minSquares;
    const visited = {};
    const node = Node({ depth: 0, sum: 0 });
    const nodeQ = [node];

    let curr;
    while ((curr = nodeQ.pop())) {
        for (let poss of possibilities) {
            if (curr.sum + poss === target) {
                const depth = curr.depth + 1;
                if (!minSquares) {
                    minSquares = depth;
                } else {
                    minSquares = depth < minSquares ? depth : minSquares;
                }
            } else if (
                curr.sum + poss <= target &&
                !visited[curr.sum + poss] &&
                !(curr.sum + poss > target)
            ) {
                visited[curr.sum + poss] = true;
                nodeQ.unshift(
                    Node({
                        depth: curr.depth + 1,
                        sum: curr.sum + poss
                    })
                );
            }
        }
    }

    return minSquares;
};

assert.equal(numSquares(1), 1);
assert.equal(numSquares(4), 1);
assert.equal(numSquares(12), 3);
