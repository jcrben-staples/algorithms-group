/**
 * from https://leetcode.com/problems/daily-temperatures/
 * copied from https://leetcode.com/problems/daily-temperatures/discuss/136017/Elegant-Python-Solution-with-Stack and did it from memory
 * @param {number[]} T
 * @return {number[]}
 */
var dailyTemperatures = function(T) {
    let stack = [];
    let result = [];
    let current, val;
    for (let i = T.length - 1; i >= 0; i--) {
        current = T[i];

        while (stack.length > 0 && current >= T[stack[stack.length - 1]]) {
            stack.pop();
        }

        val = stack.length === 0 ? 0 : stack[stack.length - 1] - i;
        result[i] = val;
        stack.push(i);
    }
};
