from typing import List, Any
import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from python.utils import utils  # noqa

# add weights later
# base this on meeting optimization in 8 hours
def knapsack(meetings: List[int]):
    # rows of meetings, columns of hours
    hours = 8
    dp = [[0 for y in range(hours + 1)] for x in range(len(meetings) + 1)]
    result: List[Any] = []

    # fill in the dp table - https://www.sanfoundry.com/java-program-solve-knapsack-problem-using-dp/
    for i, x in enumerate(meetings):
        for j in range(1, hours - 1):
            # valIfTake =
            # can we fit the meeting in?
            if dp[i - 1][j - meetings[i - 1]] + meetings[i] <= 8:
                print("still within")

            if meetings[i] <= j:
                if dp[i - 1][j - meetings[i - 1]] + meetings[i] > dp[i][j - 1]:
                    result.append(meetings[i])

                dp[i][j] = max(
                    dp[i - 1][j - meetings[i - 1]] + meetings[i], dp[i - 1][j]
                )
            else:
                dp[i][j] = dp[i - 1][j]

            if dp[i][j] == 8:
                return result

            # print("j")
    return result


# test cases
meetings = [4, 6, 1, 3, 2]
solution = [4, 1, 3] or [6, 2]
hours = 8


r = knapsack(meetings)

utils.assertEqual(r, [4, 1, 3])
"""'
Resources:

best:
[The 0/1 Knapsack Problem (Demystifying Dynamic Programming)](https://www.youtube.com/watch?v=xCbYmUPvc2Q)
* code at https://www.sanfoundry.com/java-program-solve-knapsack-problem-using-dp/

If w=val it's https://en.wikipedia.org/wiki/Subset_sum_problem

old: 
http://techieme.in/solving-01-knapsack-problem-using-recursion/ | Solving 0/1 Knapsack problem using Recursion - Techie Me
https://www.youtube.com/watch?v=8LusJS5-AGo | Tushar Roy 0/1 Knapsack Problem Dynamic Programming - YouTube
https://leetcode.com/problems/coin-change-2/discuss/99212/Knapsack-problem-Java-solution-with-thinking-process-O(nm)-Time-and-O(m)-Space | Knapsack problem - Java solution with thinking process O(nm) Time and O(m) Space - LeetCode Discuss
https://en.wikipedia.org/wiki/Knapsack_problem | Knapsack problem - Wikipedia
https://en.wikipedia.org/wiki/List_of_knapsack_problems | List of knapsack problems - Wikipedia
https://www.geeksforgeeks.org/0-1-knapsack-problem-dp-10/ | 0-1 Knapsack Problem | DP-10 - GeeksforGeeks
"""

