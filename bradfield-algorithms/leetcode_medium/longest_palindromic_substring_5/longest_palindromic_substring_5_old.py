# fails on leetcode; have to do bottom-up DP as mentioned at https://leetcode.com/problems/longest-palindromic-substring/discuss/1160264/Top-down-DP-(python-memoized)-Time-Limit-Exceeded

def isPalindrome(str):
    for i in range(len(str) // 2):
        if str[i] == str[len(str) - i - 1]:
            continue
        else:
            return False

    return True


def allSubstrings(str, memo):
    result = ""
    for i in range(len(str) + 1):
        for j in range(1, len(str) + 1):
            if memo[i][j] != -1:
                continue
            sub = str[i:j]
            memo[i][j] = sub
            if isPalindrome(sub):
                if len(sub) > len(result):
                    result = sub
            # substrings.add(str[i:j])

    return result


def longestPalindrome(s):
    result = ""
    memo = [[-1] * (len(s) + 1) for x in range((len(s) + 1))]
    return allSubstrings(s, memo)
    # for x in substrings:
    #     if isPalindrome(x):
    #         if len(x) > len(result):
    #             result = x
    # return result


result = longestPalindrome("cbbd")
# result = longestPalindrome(
#     "rgczcpratwyqxaszbuwwcadruayhasynuxnakpmsyhxzlnxmdtsqqlmwnbxvmgvllafrpmlfuqpbhjddmhmbcgmlyeypkfpreddyencsdmgxysctpubvgeedhurvizgqxclhpfrvxggrowaynrtuwvvvwnqlowdihtrdzjffrgoeqivnprdnpvfjuhycpfydjcpfcnkpyujljiesmuxhtizzvwhvpqylvcirwqsmpptyhcqybstsfgjadicwzycswwmpluvzqdvnhkcofptqrzgjqtbvbdxylrylinspncrkxclykccbwridpqckstxdjawvziucrswpsfmisqiozworibeycuarcidbljslwbalcemgymnsxfziattdylrulwrybzztoxhevsdnvvljfzzrgcmagshucoalfiuapgzpqgjjgqsmcvtdsvehewrvtkeqwgmatqdpwlayjcxcavjmgpdyklrjcqvxjqbjucfubgmgpkfdxznkhcejscymuildfnuxwmuklntnyycdcscioimenaeohgpbcpogyifcsatfxeslstkjclauqmywacizyapxlgtcchlxkvygzeucwalhvhbwkvbceqajstxzzppcxoanhyfkgwaelsfdeeviqogjpresnoacegfeejyychabkhszcokdxpaqrprwfdahjqkfptwpeykgumyemgkccynxuvbdpjlrbgqtcqulxodurugofuwzudnhgxdrbbxtrvdnlodyhsifvyspejenpdckevzqrexplpcqtwtxlimfrsjumiygqeemhihcxyngsemcolrnlyhqlbqbcestadoxtrdvcgucntjnfavylip"
# )
print(f"{result}")

# substrings = allSubstrings("helvo")
# print(f"{len(substrings)}")
assert isPalindrome("level") is True

