# 2021-09-29: go outward is what I remember...
# brute force - TLE
# class Solution:
#     def longestPalindrome(self, s: str) -> str:
#         if len(s) == 0:
#             return 0

#         longestLen = 1
#         longest = s[0]

#         def isPalindrome(s):
#             i, j = 0, len(s) - 1

#             while i <= j:
#                 if s[i] == s[j]:
#                     i += 1
#                     j -= 1
#                 else:
#                     return False

#             return True

#         for i in range(len(s)):
#             for j in range(i, len(s)):
#                 r = isPalindrome(s[i : j + 1])
#                 if r is True:
#                     tmp = longestLen
#                     longestLen = longestLen if j - i < tmp else j - i
#                     longest = longest if j - i < tmp else s[i : j + 1]
#                     # TODO: longest

#         return longest


# r = Solution().longestPalindrome("babad")
# print("r", r)


# uncertain...
class Solution:
    def longestPalindrome(self, s: str) -> str:
        def isPalindrome(s):
            i, j = 0, len(s) - 1

            while i <= j:
                if s[i] == s[j]:
                    i += 1
                    j -= 1
                else:
                    return False

            return True

        # sliding window
        i, j = 0, len(s) - 1

        while i <= j:
            newstr = s[i : j + 1]
            r = isPalindrome(newstr)

            return True
