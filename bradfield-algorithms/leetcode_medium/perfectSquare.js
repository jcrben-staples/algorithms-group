const assert = require("assert");

const Node = (val = 0, children = []) => ({
    val: val,
    children: children,
    bfsTraverse: () => {}
});

const perfectSquare = num => {
    if (Math.sqrt(num) % 2 === 0) {
        return 1;
    }

    // let possibleMinPerfectSquares = [];
    let numPerfectSquares = 0;
    let minNumPerfectSquares = 0;
    let currentState = 0;

    const depthMarker = -1;

    let queue = [];
    const visited = {};

    // possible state changes
    for (let i = 1; i * i < num; i++) {
        queue.unshift(i * i);
    }
    console.log("queue1", queue);

    const possibilities = queue.slice();
    const reversedPoss = possibilities.slice().reverse();
    let currentDepth = 1;

    // to handle iniital case
    queue.unshift(0);
    const node = Node(0, possibilities);
    const nodeQ = [node];

    // let nodes = [Node(queue)];

    // traverse(nodes);

    // let searchQueue = queue.slice();
    // for (let i of possibilities) {
    //     numOfSquares = 0;
    //     curr = searchQueue.pop();
    //     while (searchQueue.length) {}
    // }

    // for (let i of reversedPoss) {
    //     console.log("queue", queue);
    while (queue.length) {
        // currentState = i;
        // currentDepth = 1;
        // queue = possibilities.slice();

        // curr = currentState + queue.pop();
        node = queue.pop();

        for (let child of node.children) {
            child.children.unshift();
            child.bfsTraverse();
        }

        traverse(node.children);
        console.log("curr", curr);
        // currentState = currentState || curr;
        console.log("currentState", currentState);

        if (curr === depthMarker) {
            currentDepth += 1;
            console.log("currentDepth += 1", currentDepth);
            curr = queue.pop();
        } else {
            // currentState += curr;
        }

        for (let possible of reversedPoss) {
            currentState = currentState + curr;
            // for (while  queue.pop()) {
            console.log("possible", possible);
            console.log("newPossible", currentState + possible);
            if (possible + currentState === num) {
                console.log(
                    "newPossible",
                    possible + curr,
                    "num",
                    currentDepth
                );
                // numPerfectSquares += 1;
                // possibleMinPerfectSquares.push(numPerfectSquares);
                minNumPerfectSquares =
                    currentDepth < minNumPerfectSquares
                        ? currentDepth
                        : minNumPerfectSquares;

                // numPerfectSquares = 0;

                // return numPerfectSquares;
            } else if (
                !visited[curr + currentState] &&
                possible + currentState < num
            ) {
                // numPerfectSquares += 1;
                visited[possible + currentState] = true;
                console.log("adding");
                queue.unshift(depthMarker);
                queue.unshift(...possibilities);
            } else if (currentState + curr > num) {
                // currentState = currentState - curr;
            }
            // else skip
        }
    }

    return minNumPerfectSquares;
};

assert.equal(perfectSquare(12), 3);
